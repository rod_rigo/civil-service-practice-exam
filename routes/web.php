<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','users/login');

Route::group(['controller' => 'UsersController', 'prefix' => 'users', 'as' => 'users.'], function(){
    Route::redirect('/','users/login');
    Route::match(['get', 'post'],'register', 'register')->name('register');
    Route::match(['get', 'post'],'login', 'login')->name('login');
    Route::match(['get', 'post'],'logout', 'logout')->name('logout');
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'admin']], function (){

    Route::redirect('/','admin/dashboards/index');

    Route::group(['controller' => 'DashboardsController', 'prefix' => 'dashboards', 'as' => 'dashboards.'], function (){
        Route::redirect('/','dashboards/index');
        Route::get('index','index')->name('index');
        Route::get('counts','counts')->name('counts');
        Route::get('getExaminations','getExaminations')->name('getExaminations');
        Route::get('getExaminationsCount', 'getExaminationsCount')->name('getExaminationsCount');
        Route::get('getExaminationsPassingRate','getExaminationsPassingRate')->name('getExaminationsPassingRate');
        Route::get('getExaminationsMostTake','getExaminationsMostTake')->name('getExaminationsMostTake');
        Route::get('getLeaderboardsWeek','getLeaderboardsWeek')->name('getLeaderboardsWeek');
        Route::get('getLeaderboardsMonth','getLeaderboardsMonth')->name('getLeaderboardsMonth');
        Route::get('getLeaderboardsYear','getLeaderboardsYear')->name('getLeaderboardsYear');
    });

    Route::group(['controller' => 'UsersController', 'prefix' => 'users', 'as' => 'users.'], function (){
        Route::redirect('/','users/index');
        Route::get('index','index')->name('index');
        Route::get('getUsers','getUsers')->name('getUsers');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['get', 'post'],'account/{id}', 'account')->name('account');
        Route::match(['get', 'post'],'changepassword/{id}', 'changepassword')->name('changepassword');
    });

    Route::group(['controller' => 'SectionsController', 'prefix' => 'sections', 'as' => 'sections.'], function (){
        Route::redirect('/','sections/index');
        Route::get('index','index')->name('index');
        Route::get('getSections','getSections')->name('getSections');
        Route::get('bin','bin')->name('bin');
        Route::get('getSectionsDeleted','getSectionsDeleted')->name('getSectionsDeleted');
        Route::get('getSectionsList/{examId}','getSectionsList')->name('getSectionsList');
        Route::get('getSectionsExamList/{examId}','getSectionsExamList')->name('getSectionsExamList');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['get', 'post'],'orderPosition', 'orderPosition')->name('orderPosition');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'ReviewersController', 'prefix' => 'reviewers', 'as' => 'reviewers.'], function (){
        Route::redirect('/','reviewers/index');
        Route::get('index','index')->name('index');
        Route::get('getReviewers','getReviewers')->name('getReviewers');
        Route::get('bin','bin')->name('bin');
        Route::get('getReviewersDeleted','getReviewersDeleted')->name('getReviewersDeleted');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'QuestionsController', 'prefix' => 'questions', 'as' => 'questions.'], function (){
        Route::redirect('/','questions/index');
        Route::get('index','index')->name('index');
        Route::get('getQuestions','getQuestions')->name('getQuestions');
        Route::get('bin','bin')->name('bin');
        Route::get('getQuestionsDeleted','getQuestionsDeleted')->name('getQuestionsDeleted');
        Route::get('getQuestionsSectionList/{sectionId}','getQuestionsSectionList')->name('getQuestionsSectionList');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['get', 'post'],'account/{id}', 'account')->name('account');
        Route::match(['get', 'post'],'changepassword/{id}', 'changepassword')->name('changepassword');
        Route::match(['get', 'post'],'orderPosition', 'orderPosition')->name('orderPosition');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'LessonsController', 'prefix' => 'lessons', 'as' => 'lessons.'], function (){
        Route::redirect('/','lessons/index');
        Route::get('index','index')->name('index');
        Route::get('getLessons','getLessons')->name('getLessons');
        Route::get('bin','bin')->name('bin');
        Route::get('getLessonsDeleted','getLessonsDeleted')->name('getLessonsDeleted');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['get', 'post'],'download/{id}', 'download')->name('download');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'ExamsController', 'prefix' => 'exams', 'as' => 'exams.'], function (){
        Route::redirect('/','exams/index');
        Route::get('index','index')->name('index');
        Route::get('getExams','getExams')->name('getExams');
        Route::get('bin','bin')->name('bin');
        Route::get('getExamsDeleted','getExamsDeleted')->name('getExamsDeleted');
        Route::get('getExam/{id}','getExam')->name('getExam');
        Route::get('practice','practice')->name('practice');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'ExaminationsController', 'prefix' => 'examinations', 'as' => 'examinations.'], function (){
        Route::redirect('/','examinations/index');
        Route::get('index','index')->name('index');
        Route::get('getExaminations','getExaminations')->name('getExaminations');
        Route::get('today','today')->name('today');
        Route::get('getExaminationsToday','getExaminationsToday')->name('getExaminationsToday');
        Route::get('week','week')->name('week');
        Route::get('getExaminationsWeek','getExaminationsWeek')->name('getExaminationsWeek');
        Route::get('month','month')->name('month');
        Route::get('getExaminationsMonth','getExaminationsMonth')->name('getExaminationsMonth');
        Route::get('year','year')->name('year');
        Route::get('getExaminationsYear','getExaminationsYear')->name('getExaminationsYear');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['get', 'post'],'exam/{id}/{examId}', 'exam')->name('exam');
        Route::match(['get', 'post'],'release/{id}', 'release')->name('release');
        Route::get('counts/{userId}', 'counts')->name('counts');
        Route::get('getAnswersStatistics/{userId}', 'getAnswersStatistics')->name('getAnswersStatistics');
        Route::get('getAverage/{userId}', 'getAverage')->name('getAverage');
    });

    Route::group(['controller' => 'DocumentsController', 'prefix' => 'documents', 'as' => 'documents.'], function (){
        Route::redirect('/','documents/index');
        Route::get('index','index')->name('index');
        Route::get('getDocuments','getDocuments')->name('getDocuments');
        Route::get('bin','bin')->name('bin');
        Route::get('getDocumentsDeleted','getDocumentsDeleted')->name('getDocumentsDeleted');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'edit/{id}', 'edit')->name('edit');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['get', 'post'],'download/{id}', 'download')->name('download');
        Route::match(['delete', 'post'],'delete/{id}','delete')->name('delete');
        Route::match(['delete', 'post'],'restore/{id}','restore')->name('restore');
        Route::match(['delete', 'post'],'forceDelete/{id}','forceDelete')->name('forceDelete');
    });

    Route::group(['controller' => 'AnswersController', 'prefix' => 'answers', 'as' => 'answers.'], function (){
        Route::redirect('/','answers/index');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'isCorrect/{id}/{isCorrect}', 'isCorrect')->name('isCorrect');
    });

});

Route::group(['namespace' => 'Examinee', 'prefix' => 'examinee', 'as' => 'examinee.', 'middleware' => ['auth', 'examinee']], function (){

    Route::redirect('/','examinee/dashboards/index');

    Route::group(['controller' => 'DashboardsController', 'prefix' => 'dashboards', 'as' => 'dashboards.'], function (){
        Route::redirect('/','dashboards/index');
        Route::get('index','index')->name('index');
        Route::get('counts', 'counts')->name('counts');
        Route::get('getAnswersStatistics', 'getAnswersStatistics')->name('getAnswersStatistics');
        Route::get('getAverage', 'getAverage')->name('getAverage');
    });

    Route::group(['controller' => 'UsersController', 'prefix' => 'users', 'as' => 'users.'], function (){
        Route::redirect('/','users/account');
        Route::match(['get', 'post'],'account/{id}', 'account')->name('account');
        Route::match(['get', 'post'],'changepassword/{id}', 'changepassword')->name('changepassword');
    });

    Route::group(['controller' => 'ReviewersController', 'prefix' => 'reviewers', 'as' => 'reviewers.'], function (){
        Route::redirect('/','reviewers/index');
        Route::get('index','index')->name('index');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
    });

    Route::group(['controller' => 'LessonsController', 'prefix' => 'lessons', 'as' => 'lessons.'], function (){
        Route::redirect('/','lessons/index');
        Route::get('index','index')->name('index');
        Route::match(['get', 'post'],'view/{id}', 'view')->name('view');
        Route::match(['get', 'post'],'download/{id}', 'download')->name('download');
    });

    Route::group(['controller' => 'ExamsController', 'prefix' => 'exams', 'as' => 'exams.'], function (){
        Route::redirect('/','exams/index');
        Route::get('index','index')->name('index');
        Route::get('getExam/{id}','getExam')->name('getExam');
    });

    Route::group(['controller' => 'ExaminationsController', 'prefix' => 'examinations', 'as' => 'examinations.'], function (){
        Route::redirect('/','examinations/index');
        Route::get('index','index')->name('index');
        Route::get('getExaminations','getExaminations')->name('getExaminations');
        Route::get('today','today')->name('today');
        Route::get('getExaminationsToday','getExaminationsToday')->name('getExaminationsToday');
        Route::get('week','week')->name('week');
        Route::get('getExaminationsWeek','getExaminationsWeek')->name('getExaminationsWeek');
        Route::get('month','month')->name('month');
        Route::get('getExaminationsMonth','getExaminationsMonth')->name('getExaminationsMonth');
        Route::get('year','year')->name('year');
        Route::get('getExaminationsYear','getExaminationsYear')->name('getExaminationsYear');
        Route::match(['get', 'post'],'add', 'add')->name('add');
        Route::match(['get', 'post'],'exam/{id}/{examId}', 'exam')->name('exam');
    });

    Route::group(['controller' => 'DocumentsController', 'prefix' => 'documents', 'as' => 'documents.'], function (){
        Route::redirect('/','documents/index');
        Route::get('index','index')->name('index');
        Route::match(['get', 'post'],'download/{id}', 'download')->name('download');
    });

    Route::group(['controller' => 'AnswersController', 'prefix' => 'answers', 'as' => 'answers.'], function (){
        Route::redirect('/','answers/index');
        Route::match(['get', 'post'],'add', 'add')->name('add');
    });

});