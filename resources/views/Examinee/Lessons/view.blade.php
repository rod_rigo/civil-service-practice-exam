@extends('layout.examinee')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <?php
        $content = '';
        $path = public_path($lesson->content);
        $folder = \Illuminate\Support\Facades\File::isFile($path);
        if($folder){
            $content = file_get_contents($path);
        }
    ?>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <?php if(\Illuminate\Support\Facades\File::isFile(public_path($lesson->lesson_file))):?>
                <a href="{{route('examinee.lessons.download',['id' => intval($lesson->id)])}}" target="_blank" class="btn btn-info rounded-0 mx-2" title="{{ucwords('Download')}}">Download</a>
            <?php endif;?>
            <a href="{{route('examinee.lessons.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">
                        {{strtoupper($lesson->lesson)}}
                    </strong>
                </div>
                <div class="card-body">
                    <div class="row">

                        <div class="form-group col-sm-12 col-md-8 col-lg-8 my-2">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"><?=$content?></textarea>
                            <small></small>
                        </div>

                        <?php if(\Illuminate\Support\Facades\File::isFile(public_path($lesson->lesson_file))):?>
                        <div class="form-group col-sm-12 col-md-4 col-lg-4 my-2">
                            <embed src="{{url('public/'.($lesson->lesson_file))}}" id="embed" height="100%" width="100%">
                        </div>
                        <?php else:?>
                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <embed src="" id="embed" height="0" width="0">
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/examinee/lessons/view.js')}}"></script>
@endsection