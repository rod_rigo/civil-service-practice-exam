@extends('layout.examinee')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <?php
       $content = '';
       $path = public_path($reviewer->content);
       $folder = \Illuminate\Support\Facades\File::isFile($path);
       if($folder){
           $content = file_get_contents($path);
       }
    ?>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('examinee.reviewers.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">
                        {{strtoupper($reviewer->reviewer)}}
                    </strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"><?=$content?></textarea>
                            <small></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/examinee/reviewers/view.js')}}"></script>
@endsection