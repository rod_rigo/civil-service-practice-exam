@extends('layout.examinee')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <script>
        var total = parseInt({{doubleval($exam['total'])}});
        var average =  parseFloat({{doubleval($examination->average)}}).toFixed(2);
    </script>

    <div class="toast fade show floating-timer" id="timer-container" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body d-flex justify-content-center align-items-center flex-column" id="timer-body">
            <div id="percircle" class="percircle blue">
            </div>
            <p class="h4" id="scores">
                {{doubleval($examination->total)}}/{{doubleval($exam['total'])}}
            </p>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.exams.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 my-2">
            <div class="alert alert-light" role="alert">
                <h4 class="alert-heading text-uppercase">{{strtoupper($exam['exam'])}}</h4>
                <hr>
                <?php
                    $content = '';
                    $path = public_path($exam['content']);
                    $folder = \Illuminate\Support\Facades\File::isFile($path);
                    if($folder){
                        $content = file_get_contents($path);
                    }
                ?>
                <?=$content?>
            </div>
        </div>

        @foreach($exam['sections'] as $key => $section)

            <?php
                $content = '';
                $path = public_path($section['content']);
                $folder = \Illuminate\Support\Facades\File::isFile($path);
                if($folder){
                    $content = file_get_contents($path);
                }
            ?>

            <p class="h4 my-3 col-sm-12 col-md-12 col-lg-12 text-left">
                <a href="{{route('admin.reviewers.view',['id' => intval($section['reviewers']['id'])])}}" class="text-dark" target="_blank">{{strtoupper($section['reviewers']['reviewer'])}}</a>
            </p>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-warning" role="alert">
                    <?=$content?>
                </div>
            </div>
            <?php
            $i = intval(1);
            ?>
            @foreach($section['questions'] as $key => $question)
                <?php
                    $content = '';
                    $path = public_path($question['content']);
                    $folder = \Illuminate\Support\Facades\File::isFile($path);
                    if($folder){
                        $content = file_get_contents($path);
                }
                ?>
                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <div class="card shadow">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <?php
                                $letter = $answers->where('section_id', '=', intval($section['id']))
                                    ->where('question_id', '=', intval($question['id']))
                                    ->first();
                            ?>
                            <strong class="card-title m-0" title="<?=strval($i);?>" style="cursor:pointer !important;">
                                <?=intval($i)?>
                            </strong>
                            <div class="input-group w-25">
                                <input type="text" class="form-control form-control-md rounded-0 answer" name="answer" placeholder="Answer" value="{{strtoupper(strval(@$letter['answer']))}}" disabled>
                                @if(!empty($letter))
                                    @if(boolval($letter['is_correct']))
                                        <div class="input-group-append rounded-0">
                                            <button class="btn btn-success rounded-0 px-2 py-0 d-flex justify-content-center align-items-center text-white" type="button">
                                                <i class="fe fe-check" style="font-size: 20px !important;"></i>
                                            </button>
                                        </div>
                                    @else
                                        <div class="input-group-append rounded-0">
                                            <button class="btn btn-danger rounded-0 px-2 py-0 d-flex justify-content-center align-items-center text-white" type="button">
                                                <i class="fe fe-x" style="font-size: 20px !important;"></i>
                                            </button>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <?=$content?>
                        </div>
                    </div>
                </div>
                <?php $i++;?>
            @endforeach

        @endforeach

    </div>

    <script src="{{url('public/js/examinee/examinations/view.js')}}"></script>
@endsection