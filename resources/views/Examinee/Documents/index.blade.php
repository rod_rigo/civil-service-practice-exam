@extends('layout.examinee')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">

        </div>

        @foreach($documents as $key => $document)
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card shadow text-center mb-4">
                    <div class="card-body file">
                        <div class="file-action">
                            <button type="button" class="btn btn-link dropdown-toggle more-vertical p-0 text-muted mx-auto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="text-muted sr-only">Action</span>
                            </button>
                            <div class="dropdown-menu m-2">
                                <a class="dropdown-item" href="{{route('examinee.documents.download',['id' => intval($document->id)])}}" target="_blank">
                                    <i class="fe fe-download fe-12 mr-4"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                        <div class="circle circle-lg bg-light my-4">
                            <span class="fe fe-file-text fe-24 text-primary"></span>
                        </div>
                    </div> <!-- .card-body -->
                    <div class="card-footer bg-transparent border-0 fname">
                        <strong>{{strtoupper($document->title)}}</strong>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-end mt-2">
            {{ $documents->links() }}
        </div>

    </div>

@endsection