@extends('layout.examinee')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Examinations (Total)</h3>
                            <p class="small text-muted mb-0" id="total-examinations">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-success">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Examinations (Released)</h3>
                            <p class="small text-muted mb-0" id="total-released-examinations">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-danger">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Examinations (Not Released)</h3>
                            <p class="small text-muted mb-0" id="total-not-released-examinations">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-7 col-lg-8 my-2">
            <div class="card shadow eq-card mb-4">
                <div class="card-header">
                    <strong>Statistics</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12" style="height: 55vh !important;">
                            <canvas id="examination-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-4 my-2">
            <div class="card shadow eq-card mb-4">
                <div class="card-header">
                    <strong>Passing Rate</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center" style="height: 55vh !important;">
                            <div id="percircle" class="percircle green big">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/examinee/dashboards/index.js')}}"></script>

@endsection