<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{csrf_token()}}">

    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">

    <link  rel="shortcut icon" type="image/x-icon" href="{{url('public/img/logo.jpg')}}">

    <!-- Simple bar CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/simplebar.css')}}">
    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/feather.css')}}">
    <!-- Date Range Picker CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/daterangepicker.css')}}">
    <!-- App CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/app-light.css')}}" id="lightTheme">

    <link rel="stylesheet" href="{{url('public/i-check/css/icheck-bootstrap.css')}}">

    <link rel="stylesheet" href="{{url('public/percircle/css/percircle.css')}}">

    <link rel="stylesheet" href="{{url('public/datatables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{url('public/datatables/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{url('public/datatables/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{url('public/datatables/css/buttons/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{url('public/datatables/css/buttons/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{url('public/css/admin.css')}}">

    <link rel="stylesheet" href="{{url('public/jquery/css/jquery-ui.css')}}">
    <script src="{{url('public/jquery/js/jquery-3.6.0.js')}}"></script>
    <script src="{{url('public/jquery/js/jquery-ui.js')}}"></script>

    <script src="{{url('public/ck-editor/js/ckeditor.js')}}"></script>

    <script src="{{url('public/chartjs/js/chart.js')}}"></script>
    <script src="{{url('public/chartjs/js/chartjs-plugin-autocolors.js')}}"></script>

    <script src="{{url('public/moment/js/moment.js')}}"></script>

    <script src="{{url('public/sweet-alert/js/sweetalert2.js')}}"></script>
    <script src="{{url('public/sweet-alert/js/sweetalert2.all.js')}}"></script>

    <script src="{{url('public/percircle/js/percircle.js')}}"></script>

    <script src="{{url('public/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('public/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('public/datatables/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/buttons.print.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/html2pdf.bundle.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/jszip.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/pdfmake.min.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/vfs_fonts.js')}}"></script>
    <script src="{{url('public/datatables/js/buttons/buttons.html5.min.js')}}"></script>
    <script src="{{url('public/datatables/js/col-reorder-resize/ColReorderWithResize.js')}}"></script>

    <script src="{{url('public/face-detector/js/FaceDetector.js')}}"></script>
    <script src="{{url('public/face-detector/js/WebcamManager.js')}}"></script>

    <script src="{{url('public/turbo-links/js/turbolinks.js')}}"></script>

    <script>
        var mainurl = window.location.origin+'/civil-service-practice-exam/admin/';
    </script>

</head>
<body class="vertical light collapsed">

    @if(session('success'))
        @include('element.flash.success')
    @elseif(session('error'))
        @include('element.flash.error')
    @else

    @endif

    <div class="wrapper">

        @include('element.admin.navbar')

        @include('element.admin.aside')

        <main role="main" class="main-content">
            <div class="container-fluid">
                @yield('content')
            </div> <!-- .container-fluid -->
        </main> <!-- main -->

    </div> <!-- .wrapper -->

<script src="{{url('public/tiny-dash/light/js/popper.min.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/simplebar.min.js')}}"></script>
<script src='{{url('public/tiny-dash/light/js/daterangepicker.js')}}'></script>
<script src='{{url('public/tiny-dash/light/js/jquery.stickOnScroll.js')}}'></script>
<script src="{{url('public/tiny-dash/light/js/tinycolor-min.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/config.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/apps.js')}}"></script>
<script src="{{url('public/js/admin.js')}}"></script>

</body>
</html>