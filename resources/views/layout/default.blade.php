<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{csrf_token()}}">

    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">

    <link  rel="shortcut icon" type="image/x-icon" href="{{url('public/img/logo.jpg')}}">

    <!-- Simple bar CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/simplebar.css')}}">
    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/feather.css')}}">
    <!-- Date Range Picker CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/daterangepicker.css')}}">
    <!-- App CSS -->
    <link rel="stylesheet" href="{{url('public/tiny-dash/light/css/app-light.css')}}" id="lightTheme">

    <link rel="stylesheet" href="{{url('public/i-check/css/icheck-bootstrap.css')}}">

    <link rel="stylesheet" href="{{url('public/css/default.css')}}">

    <link rel="stylesheet" href="{{url('public/jquery/css/jquery-ui.css')}}">
    <script src="{{url('public/jquery/js/jquery-3.6.0.js')}}"></script>
    <script src="{{url('public/jquery/js/jquery-ui.js')}}"></script>

    <script src="{{url('public/moment/js/moment.js')}}"></script>

    <script src="{{url('public/sweet-alert/js/sweetalert2.js')}}"></script>
    <script src="{{url('public/sweet-alert/js/sweetalert2.all.js')}}"></script>

    <script src="{{url('public/turbo-links/js/turbolinks.js')}}"></script>

    <script>
        var mainurl = window.location.origin+'/civil-service-practice-exam/';
    </script>
</head>
<body class="light">

@if(session('success'))
    @include('element.flash.success')
@elseif(session('error'))
    @include('element.flash.error')
@else

@endif

<div class="wrapper vh-100">
    <div class="row align-items-center h-100">
        @yield('content')
    </div>
</div>

<script src="{{url('public/tiny-dash/light/js/popper.min.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/simplebar.min.js')}}"></script>
<script src='{{url('public/tiny-dash/light/js/daterangepicker.js')}}'></script>
<script src='{{url('public/tiny-dash/light/js/jquery.stickOnScroll.js')}}'></script>
<script src="{{url('public/tiny-dash/light/js/tinycolor-min.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/config.js')}}"></script>
<script src="{{url('public/tiny-dash/light/js/apps.js')}}"></script>
<script src="{{url('public/js/default.js')}}"></script>

</body>
</html>