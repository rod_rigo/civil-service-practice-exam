@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.users.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Form</strong>
                </div>
                <div class="card-body">
                    <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control rounded-0 required" id="name" placeholder="Name" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control rounded-0 required" id="username" placeholder="Username" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d).{8,24}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control rounded-0 required" id="email" placeholder="Email" title="{{ucwords('please fill out this field')}}" pattern="\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control rounded-0 required" id="password" placeholder="Password" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':&quot;\\|,.<>\/?]).{8,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="confirm-password">Password (Confirm)</label>
                            <input type="password" name="confirm_password" class="form-control rounded-0 required" id="confirm-password" placeholder="Password (Confirm)" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':&quot;\\|,.<>\/?]).{8,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-1">
                            <div class="icheck-primary">
                                <input type="checkbox" id="show-password">
                                <label for="show-password">Show Password</label>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-8 col-lg-7 my-2">
                            <label for="contact-number">Contact Number</label>
                            <input type="text" name="contact_number" class="form-control rounded-0 required" id="contact-number" placeholder="Contact Number" title="{{ucwords('please fill out this field')}}" pattern="(09|\+639)([0-9]{9})" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="admin">
                                <label for="admin">Admin</label>
                            </div>

                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="examinee">
                                <label for="examinee">Examinee</label>
                            </div>

                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="active">
                                <label for="active">Active</label>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <input type="hidden" name="is_active" id="is-active" value="0" required>
                            <input type="hidden" name="is_admin" id="is-admin" value="1" required>
                            <input type="hidden" name="is_examinee" id="is-examinee" value="0" required>
                            <input type="hidden" name="token" id="token" value="{{uniqid()}}" required>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/users/add.js')}}"></script>
@endsection