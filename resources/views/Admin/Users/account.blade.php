@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <script>
        var id = parseInt(<?=intval($user->id)?>);
    </script>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">

        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-body">
                    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="true">
                                Account <i class="fe fe-user-check ml-1"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="change-password-tab" data-toggle="tab" href="#change-password" role="tab" aria-controls="change-password" aria-selected="false">
                                Password <i class="fe fe-key ml-1"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="user-content">
                        <div class="tab-pane fade active show" id="account" role="tabpanel" aria-labelledby="account-tab">
                            <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control rounded-0 required" id="name" placeholder="Name" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{$user->name}}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" class="form-control rounded-0 required" id="username" placeholder="Username" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d).{8,24}" value="{{$user->username}}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control rounded-0 required" id="email" placeholder="Email" title="{{ucwords('please fill out this field')}}" pattern="\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+" value="{{$user->email}}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-8 col-lg-7 my-2">
                                    <label for="contact-number">Contact Number</label>
                                    <input type="text" name="contact_number" class="form-control rounded-0 required" id="contact-number" placeholder="Contact Number" title="{{ucwords('please fill out this field')}}" pattern="(09|\+639)([0-9]{9})" value="{{$user->contact_number}}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                    <input type="hidden" name="is_active" id="is-active" value="<?=intval($user->is_active)?>" required>
                                    <input type="hidden" name="is_admin" id="is-admin" value="<?=intval($user->is_admin)?>" required>
                                    <input type="hidden" name="is_examinee" id="is-examinee" value="<?=intval($user->is_examinee)?>" required>
                                    <input type="hidden" name="token" id="token" value="{{uniqid()}}" required>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>

                            </form>
                        </div>
                        <div class="tab-pane fade" id="change-password" role="tabpanel" aria-labelledby="change-password-tab">
                            <form action="" class="row d-flex justify-content-center align-items-center" id="password-form" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group col-sm-12 col-md-7 col-lg-8 my-2">
                                    <label for="current-password">Password (Current)</label>
                                    <input type="password" name="current_password" class="form-control rounded-0 required" id="current-password" placeholder="Password (Current)" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':&quot;\\|,.<>\/?]).{8,}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-7 col-lg-8 my-2">
                                    <label for="password">Password (New)</label>
                                    <input type="password" name="password" class="form-control rounded-0 required" id="password" placeholder="Password (New)" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':&quot;\\|,.<>\/?]).{8,}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-7 col-lg-8 my-2">
                                    <label for="confirm-password">Password (Confirm)</label>
                                    <input type="password" name="confirm_password" class="form-control rounded-0 required" id="confirm-password" placeholder="Password (Confirm)" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':&quot;\\|,.<>\/?]).{8,}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-7 col-lg-8 my-1">
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="show-password">
                                        <label for="show-password">Show Password</label>
                                    </div>
                                </div>

                                <div class="form-group col-sm-12 col-md-7 col-lg-8 my-2">
                                    <input type="hidden" name="token" id="token" value="{{uniqid()}}" required>
                                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="{{url('public/js/admin/users/account.js')}}"></script>
@endsection