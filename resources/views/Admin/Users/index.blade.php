@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.users.add')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('New User')}}" turbo-links>New User</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-body">
                    <div class="table-responsive p-1">
                        <table id="datatable" class="table table table-hover dt-responsive nowrap w-100">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Contact Number</th>
                                <th>Updated At</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/users/index.js')}}"></script>
@endsection