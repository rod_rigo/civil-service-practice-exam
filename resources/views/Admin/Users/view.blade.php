@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <script>
        var id = parseInt(<?=intval($user->id)?>);
    </script>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.users.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Form</strong>
                </div>
                <div class="card-body">
                    <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control rounded-0 required" id="name" placeholder="Name" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{$user->name}}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control rounded-0 required" id="username" placeholder="Username" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d).{8,24}" value="{{$user->username}}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control rounded-0 required" id="email" placeholder="Email" title="{{ucwords('please fill out this field')}}" pattern="\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+" value="{{$user->email}}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-8 col-lg-7 my-2">
                            <label for="contact-number">Contact Number</label>
                            <input type="text" name="contact_number" class="form-control rounded-0 required" id="contact-number" placeholder="Contact Number" title="{{ucwords('please fill out this field')}}" pattern="(09|\+639)([0-9]{9})" value="{{$user->contact_number}}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="admin" <?=(boolval($user->is_admin))? 'checked': null?>>
                                <label for="admin">Admin</label>
                            </div>

                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="examinee" <?=(boolval($user->is_examinee))? 'checked': null?>>
                                <label for="examinee">Examinee</label>
                            </div>

                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="active" <?=(boolval($user->is_active))? 'checked': null?>>
                                <label for="active">Active</label>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <input type="hidden" name="is_active" id="is-active" value="<?=intval($user->is_active)?>" required>
                            <input type="hidden" name="is_admin" id="is-admin" value="<?=intval($user->is_admin)?>" required>
                            <input type="hidden" name="is_examinee" id="is-examinee" value="<?=intval($user->is_examinee)?>" required>
                            <input type="hidden" name="token" id="token" value="{{uniqid()}}" required>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Examinations (Total)</h3>
                            <p class="small text-muted mb-0" id="total-examinations">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-success">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Examinations (Released)</h3>
                            <p class="small text-muted mb-0" id="total-released-examinations">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-danger">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Examinations (Not Released)</h3>
                            <p class="small text-muted mb-0" id="total-not-released-examinations">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-7 col-lg-8 my-2">
            <div class="card shadow eq-card mb-4">
                <div class="card-header">
                    <strong>Statistics</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12" style="height: 55vh !important;">
                            <canvas id="examination-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-4 my-2">
            <div class="card shadow eq-card mb-4">
                <div class="card-header">
                    <strong>Passing Rate</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center" style="height: 55vh !important;">
                            <div id="percircle" class="percircle green big">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/users/view.js')}}"></script>
@endsection