@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <?php
        $content = '';
        $path = public_path($section->content);
        $folder = \Illuminate\Support\Facades\File::isFile($path);
        if($folder){
            $content = file_get_contents($path);
        }
    ?>

    <script>
        var id = parseInt({{intval($section->id)}});
    </script>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <form class="modal-content" id="question-form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Question</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control rounded-0 required" id="question-content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"></textarea>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="answer">Answer</label>
                            <input type="text" name="answer" class="form-control rounded-0 required" id="answer" placeholder="Answer" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="points">Points</label>
                            <input type="number" name="points" class="form-control rounded-0 required" id="points" placeholder="Points" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="1" step="any" min="1" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="question-active" checked>
                                <label for="question-active">Active</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="section_id" id="section-id" value="<?=intval($section->id)?>" required>
                    <input type="hidden" name="exam_id" id="exam-id" value="<?=intval($section->exam_id)?>" required>
                    <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                    <input type="hidden" name="order_position" id="order-position" value="0" required>
                    <input type="hidden" name="is_active" id="question-is-active" value="1" required>
                    <button type="reset" class="btn mb-2 btn-danger">Reset</button>
                    <button type="submit" class="btn mb-2 btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-5">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-7 d-flex justify-content-end align-items-end">
            <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="{{ucwords('New Question')}}">New Question</button>
            <a href="{{route('admin.exams.view', ['id' => intval($section->exam_id)])}}" class="btn btn-info rounded-0 mx-2" title="{{ucwords('Return To Exam')}}" turbo-links>Return To Exam</a>
            <a href="{{route('admin.sections.index')}}" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-7 col-lg-8 mt-3">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="card shadow">
                        <div class="card-header">
                            <strong class="card-title">Form</strong>
                        </div>
                        <div class="card-body">
                            <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                                    <label for="exam-id">Exam</label>
                                    <select name="exam_id" class="form-control rounded-0 required" id="exam-id" title="{{ucwords('please fill out this field')}}" required>
                                        <option value="">Select Exam</option>
                                        @foreach($exams as $key => $value)
                                            <option value="{{intval($key)}}" {{(intval($key) == intval($section->exam_id))? 'selected': null;}}>{{strtoupper($value)}}</option>
                                        @endforeach
                                    </select>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                                    <label for="reviewer-id">Reviewer</label>
                                    <select name="reviewer_id" class="form-control rounded-0 required" id="reviewer-id" title="{{ucwords('please fill out this field')}}" required>
                                        <option value="">Select Reviewer</option>
                                        @foreach($reviewers as $key => $value)
                                            <option value="{{intval($key)}}" {{(intval($key) == intval($section->reviewer_id))? 'selected': null;}}>{{strtoupper($value)}}</option>
                                        @endforeach
                                    </select>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                    <label for="content">Content</label>
                                    <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"><?=$content?></textarea>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                                    <div class="icheck-primary m-2">
                                        <input type="checkbox" id="active" {{(boolval($section->is_active))? 'checked': null;}}>
                                        <label for="active">Active</label>
                                    </div>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                    <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                                    <input type="hidden" name="order_position" id="order-position" value="{{intval($section->order_position)}}" required>
                                    <input type="hidden" name="is_active" id="is-active" value="{{intval($section->is_active)}}" required>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-4">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="accordion w-100" id="question-accordion">
                        @foreach($questions as $key => $question)
                            <?php
                                $content = '';
                                $path = public_path($question['content']);
                                $folder = \Illuminate\Support\Facades\File::isFile($path);
                                if($folder){
                                    $content = file_get_contents($path);
                                }
                            ?>
                            <div class="card shadow order-position" data-id="{{intval($question['id'])}}">
                                <div class="card-header" id="heading-{{$question['id']}}">
                                    <a role="button" href="#collapse-{{$question['id']}}" data-toggle="collapse" data-target="#collapse-{{$question['id']}}" aria-expanded="false" aria-controls="collapse-{{$question['id']}}" class="collapsed float-left">
                                        <strong class="text-uppercase">
                                            <span class="question-no">{{$question['order_position']}}) </span>
                                            <?=(substr(strip_tags($content), intval(0), intval(50)))?>
                                        </strong>
                                    </a>
                                    <a href="{{route('admin.questions.view',['id' => intval($question['id'])])}}" class="float-right" target="_blank">
                                        <strong>
                                            <i class="fe fe-edit fe-16"></i>
                                        </strong>
                                    </a>
                                </div>
                                <div id="collapse-{{$question['id']}}" class="collapse" aria-labelledby="heading-{{$question['id']}}" data-parent="#question-accordion">
                                    <div class="card-body">
                                        <?=$content?> - <?=strtoupper($question['answer'])?>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="{{url('public/js/admin/sections/view.js')}}"></script>
@endsection