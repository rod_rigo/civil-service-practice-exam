@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <?php
        $content = '';
        $path = public_path($question->content);
        $folder = \Illuminate\Support\Facades\File::isFile($path);
        if($folder){
            $content = file_get_contents($path);
        }
    ?>

    <script>
        var id = parseInt({{intval($question->id)}});
    </script>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-6 col-lg-5">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-7 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.exams.view', ['id' => intval($question->exam_id)])}}" class="btn btn-info rounded-0" title="{{ucwords('Return To Exam')}}" turbo-links>Return To Exam</a>
            <a href="{{route('admin.sections.view', ['id' => intval($question->section_id)])}}" class="btn btn-dark rounded-0 mx-2" title="{{ucwords('Return To Section')}}" turbo-links>Return To Section</a>
            <a href="{{route('admin.questions.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Form</strong>
                </div>
                <div class="card-body">
                    <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="exam-id">Exam</label>
                            <select name="exam_id" class="form-control rounded-0 required" id="exam-id" title="{{ucwords('please fill out this field')}}" required>
                                <option value="">Select Exam</option>
                                @foreach($exams as $key => $value)
                                    <option value="{{intval($key)}}" {{(intval($key) == intval($question->exam_id))? 'selected': null;}}>{{strtoupper($value)}}</option>
                                @endforeach
                            </select>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="section-id">Section</label>
                            <select name="section_id" class="form-control rounded-0 required" id="section-id" title="{{ucwords('please fill out this field')}}" required>
                                <option value="">Select Section</option>
                                @foreach($sections as $key => $value)
                                    <option value="{{intval($key)}}" {{(intval($key) == intval($question->section_id))? 'selected': null;}}>{{strtoupper($value)}}</option>
                                @endforeach
                            </select>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"><?=$content?></textarea>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-3 col-lg-5 my-2">
                            <label for="answer">Answer</label>
                            <input type="text" name="answer" class="form-control rounded-0 required" id="answer" placeholder="Answer" title="{{ucwords('please fill out this field')}}" value="{{strtoupper($question->answer)}}" pattern="(.){1,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-9 col-lg-7 my-2">
                            <label for="points">Points</label>
                            <input type="number" name="points" class="form-control rounded-0 required" id="points" placeholder="Points" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{doubleval($question->points)}}" step="any" min="1" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="active" {{(boolval($question->is_active))? 'checked': null;}}>
                                <label for="active">Active</label>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                            <input type="hidden" name="order_position" id="order-position" value="{{intval($question->order_position)}}" required>
                            <input type="hidden" name="is_active" id="is-active" value="{{intval($question->is_active)}}" required>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/questions/view.js')}}"></script>
@endsection