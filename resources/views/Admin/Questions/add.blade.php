@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')


    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.questions.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Form</strong>
                </div>
                <div class="card-body">
                    <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="exam-id">Exam</label>
                            <select name="exam_id" class="form-control rounded-0 required" id="exam-id" title="{{ucwords('please fill out this field')}}" required>
                                <option value="">Select Exam</option>
                                @foreach($exams as $key => $value)
                                    <option value="{{intval($key)}}">{{strtoupper($value)}}</option>
                                @endforeach
                            </select>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                            <label for="section-id">Section</label>
                            <select name="section_id" class="form-control rounded-0 required" id="section-id" title="{{ucwords('please fill out this field')}}" required>
                                <option value="">Choose Exam</option>
                            </select>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-3 col-lg-5 my-2">
                            <label for="answer">Answer</label>
                            <input type="text" name="answer" class="form-control rounded-0 required" id="answer" placeholder="Answer" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-9 col-lg-7 my-2">
                            <label for="points">Points</label>
                            <input type="number" name="points" class="form-control rounded-0 required" id="points" placeholder="Points" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="1" step="any" min="1" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"></textarea>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="active" checked>
                                <label for="active">Active</label>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                            <input type="hidden" name="order_position" id="order-position" value="0" required>
                            <input type="hidden" name="is_active" id="is-active" value="1" required>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/questions/add.js')}}"></script>
@endsection