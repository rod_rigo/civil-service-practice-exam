@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-user fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Users</h3>
                            <p class="small text-muted mb-0" id="total-users">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-file-plus fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Exams</h3>
                            <p class="small text-muted mb-0" id="total-exams">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-bookmark fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Reviewer</h3>
                            <p class="small text-muted mb-0" id="total-reviewers">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Today</h3>
                            <p class="small text-muted mb-0" id="total-examinations-today">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Week</h3>
                            <p class="small text-muted mb-0" id="total-examinations-week">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Month</h3>
                            <p class="small text-muted mb-0" id="total-examinations-month">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3 col-lg-3 my-2">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-3 text-center">
                          <span class="circle circle-md bg-primary">
                            <span class="fe fe-file-text fe-24 text-white"></span>
                          </span>
                        </div>
                        <div class="col">
                            <h3 class="h6 mb-0 text-uppercase">Year</h3>
                            <p class="small text-muted mb-0" id="total-examinations-year">0</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form class="row" id="form" method="post" enctype="multipart/form-data">
        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
            <label for="start-date">Start Date</label>
            <input type="date" name="start_date" class="form-control rounded-0" id="start-date" value="{{(new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')}}" title="{{ucwords('please fill out this field')}}" required>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
            <label for="end-date">End Date</label>
            <input type="date" name="end_date" class="form-control rounded-0" id="end-date" value="{{(new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')}}" title="{{ucwords('please fill out this field')}}" required>
        </div>
        <div class="col-sm-12 col-md-2 col-lg-2 mt-3 d-flex justify-content-start align-items-end">
            <button type="submit" class="btn btn-primary rounded-0">Submit</button>
        </div>
    </form>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9 my-2">
            <div class="card shadow eq-card mb-4">
                <div class="card-header">
                    <strong>Examinations</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12" style="height: 55vh !important;">
                            <canvas id="examination-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 my-2">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Passing Rates</strong>
                </div>
                <div class="card-body">
                    <div class="list-group list-group-flush my-n3" id="examination-most-take">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-7 my-2">
            <div class="card shadow eq-card mb-4">
                <div class="card-header">
                    <strong>Most Take Exam</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12" style="height: 55vh !important;">
                            <canvas id="examination-most-take-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-5 my-2">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Leaderboards</strong>
                </div>
                <div class="card-body py-2">
                    <ul class="nav nav-tabs mb-3" id="leaderboards-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="week-tab" data-toggle="tab" href="#week" role="tab" aria-controls="week" aria-selected="true">
                                Week
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="month-tab" data-toggle="tab" href="#month" role="tab" aria-controls="month" aria-selected="false">
                                Month
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="year-tab" data-toggle="tab" href="#year" role="tab" aria-controls="year" aria-selected="false">
                                Year
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="leaderboards-tab-content">
                        <div class="tab-pane fade show active" id="week" role="tabpanel" aria-labelledby="week-tab">
                            <div class="list-group list-group-flush" id="week-leaderboards">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="month" role="tabpanel" aria-labelledby="month-tab">
                            <div class="list-group list-group-flush" id="month-leaderboards">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="year" role="tabpanel" aria-labelledby="year-tab">
                            <div class="list-group list-group-flush" id="year-leaderboards">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/dashboards/index.js')}}"></script>

@endsection