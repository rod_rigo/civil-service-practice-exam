@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')


    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.lessons.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Form</strong>
                </div>
                <div class="card-body">
                    <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="lesson">Lesson</label>
                            <input type="text" name="lesson" class="form-control rounded-0 required" id="lesson" placeholder="Lesson" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="reviewer-id">Reviewer</label>
                            <select name="reviewer_id" class="form-control rounded-0 required" id="reviewer-id" title="{{ucwords('please fill out this field')}}" required>
                                <option value="">Select Reviewer</option>
                                @foreach($reviewers as $key => $value)
                                    <option value="{{intval($key)}}">{{strtoupper($value)}}</option>
                                @endforeach
                            </select>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"></textarea>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="file">File</label>
                            <input type="file" name="file" class="form-control rounded-0 required" id="file" title="{{ucwords('please fill out this field')}}" accept="application/pdf" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <embed src="" id="embed" height="0" width="0">
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="active" checked>
                                <label for="active">Active</label>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                            <input type="hidden" name="is_active" id="is-active" value="1" required>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/lessons/add.js')}}"></script>
@endsection