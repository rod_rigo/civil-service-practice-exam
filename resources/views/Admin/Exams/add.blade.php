@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')


    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.exams.index')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-header">
                    <strong class="card-title">Form</strong>
                </div>
                <div class="card-body">
                    <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="exam">Exam</label>
                            <input type="text" name="exam" class="form-control rounded-0 required" id="exam" placeholder="Exam" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-2 col-lg-2 my-2">
                            <label for="month">Month</label>
                            <input type="text" name="month" class="form-control rounded-0 required" id="month" placeholder="Month" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{date('F')}}" readonly required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-2 col-lg-1 my-2">
                            <label for="year">Year</label>
                            <input type="text" name="year" class="form-control rounded-0 required" id="year" placeholder="Year" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{date('Y')}}" readonly required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-2 col-lg-1 my-2">
                            <label for="minutes">Minutes</label>
                            <input type="number" name="minutes" class="form-control rounded-0 required" id="minutes" placeholder="Minutes" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="15" step="any" min="15" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-3 col-lg-4 my-2">
                            <label for="total">Total</label>
                            <input type="number" name="total" class="form-control rounded-0 required" id="total" placeholder="Total" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="1" step="any" min="1" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-3 col-lg-4 my-2">
                            <label for="average">Average</label>
                            <input type="number" name="average" class="form-control rounded-0 required" id="average" placeholder="Average" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="1" step="any" min="1" max="100" required>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"></textarea>
                            <small></small>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                            <div class="icheck-primary m-2">
                                <input type="checkbox" id="active" checked>
                                <label for="active">Active</label>
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                            <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                            <input type="hidden" name="is_active" id="is-active" value="1" required>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/exams/add.js')}}"></script>
@endsection