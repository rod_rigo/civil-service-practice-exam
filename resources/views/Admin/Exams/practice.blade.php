@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <form class="modal-content" id="form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Examination</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="row">
                       <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                           <label for="minutes">Minutes</label>
                           <input type="number" name="minutes" class="form-control rounded-0 required" id="minutes" placeholder="Minutes" title="{{ucwords('please fill out this field')}}" min="15" value="15" pattern="(.){1,}">
                           <small></small>
                       </div>

                       <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                           <label for="password">Password (Confirm)</label>
                           <input type="password" name="password" class="form-control rounded-0 required" id="password" placeholder="Password" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}">
                           <small></small>
                       </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="exam_id" id="exam-id" value="" required>
                    <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                    <button type="reset" class="btn mb-2 btn-secondary" data-dismiss="modal" aria-label="Close">Close</button>
                    <button type="submit" class="btn mb-2 btn-primary">Proceed</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12 my-2">
            <div class="alert alert-primary" role="alert">
                <span class="fe fe-alert-circle fe-16 mr-2"></span>
                This Is Only Based In The Civil Service Exam Reviewer, The Questions Are Not Sure To Be Included In The Actual Examination
            </div>
        </div>

        @foreach($exams as $exam)
            <div class="col-sm-12 col-md-6 col-lg-4 my-2">
                <div class="card-deck my-4">
                    <div class="card mb-4 shadow">
                        <div class="card-body text-center my-4">
                            <a href="javascript:void(0);">
                                <h3 class="h5 mt-4 mb-0">
                                    {{strtoupper($exam['exam'])}}
                                </h3>
                            </a>
                            <p class="text-muted"></p>

                            <span class="h1 mb-0 text-primary">
                                {{number_format(doubleval($exam['average']))}}%
                            </span>
                            <p class="text-dark">Passing Average</p>

                            <ul class="list-unstyled">
                                <li class="text-uppercase">
                                    <h6>Sections</h6>
                                </li>
                                @foreach($exam['sections'] as $section)
                                    <li class="text-uppercase">
                                        {{strtoupper($section['reviewers']['reviewer'])}} ({{intval(count($section['questions']))}})
                                    </li>
                                @endforeach
                                <li class="text-uppercase">Total Questions {{intval(count($exam['questions']))}}/{{intval($exam['total'])}} Points</li>
                            </ul>

                            <button type="button" class="btn mb-2 btn-primary btn-block btn-lg exam" data-id="{{$exam['id']}}" data-url="{{intval($exam['examination_id'])}}">
                                {{(intval($exam['examination_id']))? 'Continue': 'Start';}}
                            </button>

                            <br>

                            <span class="text-danger ml-3">
                                <i class="fe fe-clock fe-16"></i>
                                {{intval($exam['minutes'])}}(Mins)
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-end mt-2">
            {{ $exams->links() }}
        </div>

    </div>

    <script src="{{url('public/js/admin/exams/practice.js')}}"></script>
@endsection