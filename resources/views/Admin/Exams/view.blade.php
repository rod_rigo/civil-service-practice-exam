@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <?php
       $content = '';
       $path = public_path($exam->content);
       $folder = \Illuminate\Support\Facades\File::isFile($path);
       if($folder){
           $content = file_get_contents($path);
       }
    ?>

    <script>
        var id = parseInt({{intval($exam->id)}});
    </script>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <form class="modal-content" id="section-form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Section</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                        <label for="reviewer-id">Reviewer</label>
                        <select name="reviewer_id" class="form-control rounded-0 required" id="reviewer-id" title="{{ucwords('please fill out this field')}}" required>
                            <option value="">Select Reviewer</option>
                            @foreach($reviewers as $key => $value)
                                <option value="{{intval($key)}}">{{strtoupper($value)}}</option>
                            @endforeach
                        </select>
                        <small></small>
                    </div>

                    <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                        <label for="content">Content</label>
                        <textarea name="content" class="form-control rounded-0 required" id="section-content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"></textarea>
                        <small></small>
                    </div>

                    <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                        <div class="icheck-primary m-2">
                            <input type="checkbox" id="section-active" checked>
                            <label for="section-active">Active</label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="exam_id" id="exam-id" value="{{intval($exam->id)}}" required>
                    <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                    <input type="hidden" name="order_position" id="order-position" value="0" required>
                    <input type="hidden" name="is_active" id="section-is-active" value="1" required>
                    <button type="reset" class="btn mb-2 btn-danger">Reset</button>
                    <button type="submit" class="btn mb-2 btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <button type="button" id="toggle-modal" class="btn btn-primary rounded-0 mr-2" title="{{ucwords('New Section')}}">New Section</button>
            <a href="{{route('admin.exams.index')}}" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-7 col-lg-8">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <strong class="card-title">Form</strong>
                        </div>
                        <div class="card-body">
                            <form action="" class="row" id="form" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                    <label for="exam">Exam</label>
                                    <input type="text" name="exam" class="form-control rounded-0 required" id="exam" placeholder="Exam" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{$exam->exam}}" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-4 col-lg-4 my-2">
                                    <label for="month">Month</label>
                                    <input type="text" name="month" class="form-control rounded-0 required" id="month" placeholder="Month" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{$exam->month}}" readonly required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-4 col-lg-4 my-2">
                                    <label for="year">Year</label>
                                    <input type="text" name="year" class="form-control rounded-0 required" id="year" placeholder="Year" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{$exam->year}}" readonly required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-4 col-lg-4 my-2">
                                    <label for="minutes">Minutes</label>
                                    <input type="number" name="minutes" class="form-control rounded-0 required" id="minutes" placeholder="Minutes" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{intval($exam->minutes)}}" step="any" min="15" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-6 col-lg-5 my-2">
                                    <label for="total">Total</label>
                                    <input type="number" name="total" class="form-control rounded-0 required" id="total" placeholder="Total" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{$exam->total}}" step="any" min="1" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-6 col-lg-5 my-2">
                                    <label for="average">Average</label>
                                    <input type="number" name="average" class="form-control rounded-0 required" id="average" placeholder="Average" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" value="{{$exam->average}}" step="any" min="1" max="100" required>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                    <label for="content">Content</label>
                                    <textarea name="content" class="form-control rounded-0 required" id="content" placeholder="Content" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}"><?=$content?></textarea>
                                    <small></small>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2 d-flex justify-content-start align-items-center">
                                    <div class="icheck-primary m-2">
                                        <input type="checkbox" id="active" {{(boolval($exam->is_active))? 'checked': null;}}>
                                        <label for="active">Active</label>
                                    </div>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                    <input type="hidden" name="user_id" id="user-id" value="<?=intval(auth()->user()->id)?>" required>
                                    <input type="hidden" name="is_active" id="is-active" value="{{intval($exam->is_active)}}" required>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-4">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                    <div class="accordion w-100" id="section-accordion">
                        @foreach($sections as $key => $section)
                            <div class="card shadow order-position" data-id="{{intval($section['id'])}}">
                                <div class="card-header" id="heading-{{$section['id']}}">
                                    <a role="button" href="#collapse-{{$section['id']}}" data-toggle="collapse" data-target="#collapse-{{$section['id']}}" aria-expanded="false" aria-controls="collapse-{{$section['id']}}" class="collapsed float-left">
                                        <strong class="text-uppercase">
                                            <span class="section-no">{{$section['order_position']}}) </span>
                                            {{strtoupper($section['reviewers']['reviewer'])}}
                                        </strong>
                                    </a>
                                    <a href="{{route('admin.sections.view',['id' => intval($section['id'])])}}" class="float-right" target="_blank">
                                        <strong>
                                            <i class="fe fe-edit fe-16"></i>
                                        </strong>
                                    </a>
                                </div>
                                <div id="collapse-{{$section['id']}}" class="collapse" aria-labelledby="heading-{{$section['id']}}" data-parent="#section-accordion">
                                    <div class="card-body">
                                        <ol>
                                            @foreach($section['questions'] as $key => $question)
                                                <?php
                                                    $path = public_path($question['content']);
                                                    $folder = \Illuminate\Support\Facades\File::isFile($path);
                                                ?>
                                                @if($folder)
                                                        <li>
                                                            <?=file_get_contents($path);?> - <?=strtoupper($question['answer'])?>
                                                        </li>
                                                @endif
                                            @endforeach
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{url('public/js/admin/exams/view.js')}}"></script>
@endsection