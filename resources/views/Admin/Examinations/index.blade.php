@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <form class="row justify-content-center" id="form" method="post" enctype="multipart/form-data">
        @csrf
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">

        </div>

        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
            <label for="start-date">Start Date</label>
            <input type="date" name="start_date" class="form-control rounded-0" id="start-date" value="{{(new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')}}" title="{{ucwords('please fill out this field')}}" required>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
            <label for="end-date">End Date</label>
            <input type="date" name="end_date" class="form-control rounded-0" id="end-date" value="{{(new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')}}" title="{{ucwords('please fill out this field')}}" required>
        </div>
        <div class="col-sm-12 col-md-2 col-lg-2 mt-3">
            <label for="records">Records</label>
            <input type="number" name="records" class="form-control rounded-0" id="records" value="10000" min="10000" title="{{ucwords('please fill out this field')}}" placeholder="Records" required>
        </div>
        <div class="col-sm-12 col-md-2 col-lg-2 mt-3 d-flex justify-content-start align-items-end">
            <button type="submit" class="btn btn-primary rounded-0">Submit</button>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card shadow">
                <div class="card-body">
                    <div class="table-responsive p-1">
                        <table id="datatable" class="table table table-hover dt-responsive nowrap w-100">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Examinee</th>
                                <th>Exam</th>
                                <th>Minutes</th>
                                <th>Month</th>
                                <th>Year</th>
                                <th>Total</th>
                                <th>Average</th>
                                <th>Is Active</th>
                                <th>Updated At</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="{{url('public/js/admin/examinations/index.js')}}"></script>
@endsection