@extends('layout.admin')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <?php
        $now = new \Moment\Moment('now', 'Asia/Manila');
        $interval = $now->from($examination->end_date);
        $endDate = new \Moment\Moment($examination->end_date, 'Asia/Manila');
        $disabled = ($endDate > $now)? null: 'disabled';
    ?>
    <script>
        var date = moment('{{strval($examination->end_date)}}');
        var examination_id = parseInt({{intval($examination->id)}});
        var user_id = parseInt({{intval(auth()->user()->id)}});
    </script>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                @csrf
                <div class="modal-header d-flex justify-content-center align-items-center">
                    <h6 class="text-center m-0">Please Place You Face In The Camera, Wait Until You See A Blue Box</h6>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center p-3">
                            <video id="video" width="400" height="400" style="position:relative;left:14.5em;top:0;width: 400px; height: 400px; border: 1px solid black;" autoplay></video>
                            <canvas id="canvas" width="400" height="400" style="position:relative;left:-14em;top:0;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center align-items-center">
                    <h6 class="text-center m-0">Please Hold Your Position</h6>
                </div>
            </div>
        </div>
    </div>

    <div class="toast fade show floating-timer" id="timer-container" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body" id="timer-body">
            <i class="fe fe-clock"></i>
            @if($endDate > $now)
                <?=(abs(intval($interval->getMinutes())).':'.(intval(60) - intval(date('s'))))?>
            @else
                --:--
            @endif
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-8 col-lg-9">
            <h3 class="page-title">{{ucwords(explode('.', Route::currentRouteName())[1])}}</h3>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-3 d-flex justify-content-end align-items-end">
            <a href="{{route('admin.exams.practice')}}" id="toggle-modal" class="btn btn-secondary rounded-0" title="{{ucwords('Return')}}" turbo-links>Return</a>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12 my-2">
            <div class="alert alert-light" role="alert">
                <h4 class="alert-heading text-uppercase">{{strtoupper($exam['exam'])}}</h4>
                <hr>
                <?php
                    $content = '';
                    $path = public_path($exam['content']);
                    $folder = \Illuminate\Support\Facades\File::isFile($path);
                    if($folder){
                        $content = file_get_contents($path);
                    }
                ?>
                <?=$content?>
            </div>
        </div>

        @foreach($exam['sections'] as $key => $section)

            <?php
                $content = '';
                $path = public_path($section['content']);
                $folder = \Illuminate\Support\Facades\File::isFile($path);
                if($folder){
                    $content = file_get_contents($path);
                }
            ?>

            <p class="h4 my-3 col-sm-12 col-md-12 col-lg-12 text-left">
                <a href="{{route('admin.reviewers.view',['id' => intval($section['reviewers']['id'])])}}" class="text-dark" target="_blank">{{strtoupper($section['reviewers']['reviewer'])}}</a>
            </p>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-warning" role="alert">
                    <?=$content?>
                </div>
            </div>
            <?php
                $i = intval(1);
            ?>
            @foreach($section['questions'] as $key => $question)
                    <?php
                        $content = '';
                        $path = public_path($question['content']);
                        $folder = \Illuminate\Support\Facades\File::isFile($path);
                        if($folder){
                            $content = file_get_contents($path);
                        }
                    ?>
                <div class="col-sm-12 col-md-12 col-lg-12 my-2">
                    <div class="card shadow">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <strong class="card-title m-0" title="<?=strval($i);?>">
                                <?=intval($i)?>
                            </strong>
                            <?php
                                $letter = $answers->where('section_id', '=', intval($section['id']))
                                    ->where('question_id', '=', intval($question['id']))
                                    ->first();
                            ?>
                            <input type="text" class="w-25 form-control form-control-md rounded-0 answer" name="answer" placeholder="Answer" value="{{strtoupper(strval(@$letter['answer']))}}" data-section-id="{{intval($section['id'])}}" data-question-id="{{intval($question['id'])}}" {{$disabled}}>
                        </div>
                        <div class="card-body">
                            <?=$content?>
                        </div>
                    </div>
                </div>
                <?php $i++;?>
            @endforeach

        @endforeach

    </div>

    <script src="{{url('public/js/admin/examinations/exam.js')}}"></script>

@endsection