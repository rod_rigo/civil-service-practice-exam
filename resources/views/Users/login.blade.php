@extends('layout.default')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')
    <form class="col-lg-3 col-md-4 col-10 mx-auto text-center" id="form" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card shadow mb-4">
            <div class="card-header">
                <strong class="card-title">
                    <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="javascript:void(0);">
                        <svg version="1.1" id="logo" class="navbar-brand-img brand-md" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
              <g>
                  <polygon class="st0" points="78,105 15,105 24,87 87,87 	"></polygon>
                  <polygon class="st0" points="96,69 33,69 42,51 105,51 	"></polygon>
                  <polygon class="st0" points="78,33 15,33 24,15 87,15 	"></polygon>
              </g>
            </svg>
                    </a>
                </strong>
            </div>
            <div class="card-body">
                <h4 class="mb-3">Sign in</h4>
                <div class="form-group">
                    <label for="username" class="sr-only">Username | Email</label>
                    <input type="text" name="username" id="username" class="form-control form-control-lg required" placeholder="Username | Email" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" required>
                    <small></small>
                </div>
                <div class="form-group">
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="password" class="form-control form-control-lg required" placeholder="Password" pattern="(.){1,}" title="{{ucwords('please fill out this field')}}" required>
                    <small></small>
                </div>
                <div class="form-group d-flex flex-nowrap justify-content-between align-items-center">
                    <div class="icheck-primary checkbox mb-3">
                        <input type="checkbox" name="remember_me" id="remember-me">
                        <label for="remember-me">Remember Me</label>
                    </div>
                    <div class="icheck-primary checkbox mb-3">
                        <input type="checkbox" name="show_password" id="show-password">
                        <label for="show-password">Show Password</label>
                    </div>
                </div>
                <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Sign In</button>
                <p class="mt-5 mb-3 text-muted">© 2024</p>
            </div>
            <div class="card-footer">
                <a href="{{route('users.register')}}" class="text-dark" turbo-links>Register</a>
            </div>
        </div>
    </form>

    <script src="{{url('public/js/users/login.js')}}"></script>
@endsection