@extends('layout.default')
@section('title', ucwords(explode('.', Route::currentRouteName())[0]))
@section('content')

    <form class="col-sm-12 col-md-8 col-lg-5 col-10 mx-auto" id="form" method="post" enctype="multipart/form-data" style="overflow-y: auto !important;">
        @csrf
        <div class="card shadow mb-4">
            <div class="card-header text-center">
                <strong class="card-title">
                    <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="javascript:void(0);">
                        <svg version="1.1" id="logo" class="navbar-brand-img brand-md" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
              <g>
                  <polygon class="st0" points="78,105 15,105 24,87 87,87 	"></polygon>
                  <polygon class="st0" points="96,69 33,69 42,51 105,51 	"></polygon>
                  <polygon class="st0" points="78,33 15,33 24,15 87,15 	"></polygon>
              </g>
            </svg>
                    </a>
                </strong>
            </div>
            <div class="card-body">
                <h4 class="mb-3 text-center">Register</h4>
                <div class="row">
                    <div class="form-group col-sm-12 col-md-8 col-lg-8 my-2">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control rounded-0 required" id="name" placeholder="Name" title="{{ucwords('please fill out this field')}}" pattern="(.){1,}" required>
                        <small></small>
                    </div>

                    <div class="form-group col-sm-12 col-md-4 col-lg-4 my-2">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control rounded-0 required" id="username" placeholder="Username" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d).{8,24}" required>
                        <small></small>
                    </div>

                    <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control rounded-0 required" id="email" placeholder="Email" title="{{ucwords('please fill out this field')}}" pattern="\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+" required>
                        <small></small>
                    </div>

                    <div class="form-group col-sm-12 col-md-6 col-lg-6 my-2">
                        <label for="contact-number">Contact Number</label>
                        <input type="text" name="contact_number" class="form-control rounded-0 required" id="contact-number" placeholder="Contact Number" title="{{ucwords('please fill out this field')}}" pattern="(09|\+639)([0-9]{9})" required>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-8">
                        <div class="row">
                            <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control rounded-0 required" id="password" placeholder="Password" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':&quot;\\|,.<>\/?]).{8,}" required>
                                <small></small>
                            </div>

                            <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                                <label for="confirm-password">Password (Confirm)</label>
                                <input type="password" name="confirm_password" class="form-control rounded-0 required" id="confirm-password" placeholder="Password (Confirm)" title="{{ucwords('please fill out this field')}}" pattern="(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':&quot;\\|,.<>\/?]).{8,}" required>
                                <small></small>
                            </div>

                            <div class="form-group col-sm-12 col-md-12 col-lg-12 my-1">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="show-password">
                                    <label for="show-password">Show Password</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-4 text-capitalize my-2">
                        <p class="mb-2">Password requirements</p>
                        <p class="small text-muted mb-2 text-dark"> To create a new password, you have to meet all of the following requirements: </p>
                        <ul class="small text-muted pl-4 mb-0 text-dark">
                            <li> Minimum 8 character </li>
                            <li>At least one special character</li>
                            <li>At least one number</li>
                            <li>Can’t be the same as a previous password </li>
                        </ul>
                    </div>

                    <div class="form-group col-sm-12 col-md-12 col-lg-12 my-2">
                        <input type="hidden" name="is_active" id="is-active" value="1" required>
                        <input type="hidden" name="is_admin" id="is-admin" value="0" required>
                        <input type="hidden" name="is_examinee" id="is-examinee" value="1" required>
                        <input type="hidden" name="token" id="token" value="{{uniqid()}}" required>
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">
                <a href="{{route('users.login')}}" class="text-dark" turbo-links>Return To Login</a>
            </div>
        </div>
    </form>

    <script src="{{url('public/js/users/register.js')}}"></script>
@endsection