<script>
    $(function () {
        Swal.fire({
            icon:'success',
            title:'Success',
            text:'{{session('success')}}',
            timer:5000,
            timerProgressBar:true,
        });
    })
</script>