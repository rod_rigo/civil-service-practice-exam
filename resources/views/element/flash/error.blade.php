<script>
    $(function () {
        Swal.fire({
            icon:'error',
            title:'Error',
            text:'{{session('error')}}',
            timer:5000,
            timerProgressBar:true,
        });
    })
</script>