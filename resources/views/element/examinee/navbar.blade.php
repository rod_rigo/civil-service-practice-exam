<nav class="topnav navbar navbar-light">
    <button type="button" class="navbar-toggler text-muted mt-2 p-0 mr-3 collapseSidebar">
        <i class="fe fe-menu navbar-toggler-icon"></i>
    </button>
    <ul class="nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-muted pr-0" href="javascript:void(0);" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="avatar avatar-sm mt-2">
                <img src="{{url('public/img/user-avatar/'.(strval(auth()->user()->id)).'.png')}}?timestamp={{uniqid()}}" alt="#" class="avatar-img rounded-circle" loading="lazy">
              </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="{{route('examinee.users.account',['id' => intval(auth()->user()->id)])}}" turbo-links>Account</a>
            </div>
        </li>
    </ul>
</nav>