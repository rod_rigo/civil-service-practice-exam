<aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
    <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
        <i class="fe fe-x"><span class="sr-only"></span></i>
    </a>
    <nav class="vertnav navbar navbar-light">
        <!-- nav bar -->
        <div class="w-100 mb-4 d-flex">
            <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="javascript:void(0);">
                <svg version="1.1" id="logo" class="navbar-brand-img brand-sm" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
                <g>
                    <polygon class="st0" points="78,105 15,105 24,87 87,87" />
                    <polygon class="st0" points="96,69 33,69 42,51 105,51" />
                    <polygon class="st0" points="78,33 15,33 24,15 87,15" />
                </g>
              </svg>
            </a>
        </div>
        <p class="text-muted nav-heading mt-4 mb-1">
            <span>Navigation</span>
        </p>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item w-100 {{(request()->routeIs('examinee.dashboards.index'))? 'active': null;}}">
                <a class="nav-link" href="{{route('examinee.dashboards.index')}}" turbo-links>
                    <i class="fe fe-layers fe-16"></i>
                    <span class="ml-3 item-text">Dashboard</span>
                </a>
            </li>
            <li class="nav-item w-100 {{(request()->routeIs('examinee.exams.index'))? 'active': null;}}">
                <a class="nav-link" href="{{route('examinee.exams.index')}}" turbo-links>
                    <i class="fe fe-book fe-16"></i>
                    <span class="ml-3 item-text">Exams</span>
                </a>
            </li>
            <li class="nav-item dropdown {{(request()->routeIs('examinee.examinations.*'))? 'active': null;}}">
                <a href="#examinations" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle nav-link">
                    <i class="fe fe-file-text fe-16"></i>
                    <span class="ml-3 item-text">Examinations</span><span class="sr-only">(current)</span>
                </a>
                <ul class="list-unstyled pl-4 w-100 collapse {{(request()->routeIs('examinee.examinations.*'))? 'show': null;}}" id="examinations" style="">
                    <li class="nav-item {{(request()->routeIs('examinee.examinations.index'))? 'active': null;}}">
                        <a class="nav-link pl-3" href="{{route('examinee.examinations.index')}}" turbo-links>
                            <span class="ml-1 item-text">All</span>
                        </a>
                    </li>
                    <li class="nav-item {{(request()->routeIs('examinee.examinations.today'))? 'active': null;}}">
                        <a class="nav-link pl-3" href="{{route('examinee.examinations.today')}}" turbo-links>
                            <span class="ml-1 item-text">Today</span>
                        </a>
                    </li>
                    <li class="nav-item {{(request()->routeIs('examinee.examinations.week'))? 'active': null;}}">
                        <a class="nav-link pl-3" href="{{route('examinee.examinations.week')}}" turbo-links>
                            <span class="ml-1 item-text">Week</span>
                        </a>
                    </li>
                    <li class="nav-item {{(request()->routeIs('examinee.examinations.month'))? 'active': null;}}">
                        <a class="nav-link pl-3" href="{{route('examinee.examinations.month')}}" turbo-links>
                            <span class="ml-1 item-text">Month</span>
                        </a>
                    </li>
                    <li class="nav-item {{(request()->routeIs('examinee.examinations.year'))? 'active': null;}}">
                        <a class="nav-link pl-3" href="{{route('examinee.examinations.year')}}" turbo-links>
                            <span class="ml-1 item-text">Year</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <p class="text-muted nav-heading mt-4 mb-1">
            <span>Forms</span>
        </p>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item w-100 {{(request()->routeIs('examinee.lessons.index'))? 'active': null;}}">
                <a class="nav-link" href="{{route('examinee.lessons.index')}}" turbo-links>
                    <i class="fe fe-book-open fe-16"></i>
                    <span class="ml-3 item-text">Lessons</span>
                </a>
            </li>
            <li class="nav-item w-100 {{(request()->routeIs('examinee.documents.index'))? 'active': null;}}">
                <a class="nav-link" href="{{route('examinee.documents.index')}}" turbo-links>
                    <i class="fe fe-folder fe-16"></i>
                    <span class="ml-3 item-text">Documents</span>
                </a>
            </li>
            <li class="nav-item w-100 {{(request()->routeIs('examinee.reviewers.index'))? 'active': null;}}">
                <a class="nav-link" href="{{route('examinee.reviewers.index')}}" turbo-links>
                    <i class="fe fe-bookmark fe-16"></i>
                    <span class="ml-3 item-text">Reviewers</span>
                </a>
            </li>
        </ul>
        <p class="text-muted nav-heading mt-4 mb-1">
            <span>Sign Out</span>
        </p>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <li class="nav-item w-100 {{(request()->routeIs('users.logout'))? 'active': null;}}">
                <a class="nav-link" href="{{route('users.logout')}}" turbo-links>
                    <i class="fe fe-log-out fe-16"></i>
                    <span class="ml-3 item-text">Logout</span>
                </a>
            </li>
        </ul>
    </nav>
</aside>