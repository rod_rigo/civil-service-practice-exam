/*
 Navicat Premium Data Transfer

 Source Server         : xampp
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : civil_service_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 05/06/2024 21:58:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `examination_id` bigint UNSIGNED NOT NULL,
  `question_id` bigint UNSIGNED NOT NULL,
  `section_id` bigint UNSIGNED NOT NULL,
  `answer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `points` double NOT NULL DEFAULT 0,
  `is_correct` tinyint NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `answers to users`(`user_id` ASC) USING BTREE,
  INDEX `answers to examinations`(`examination_id` ASC) USING BTREE,
  INDEX `answers to questions`(`question_id` ASC) USING BTREE,
  INDEX `answers to sections`(`section_id` ASC) USING BTREE,
  CONSTRAINT `answers to examinations` FOREIGN KEY (`examination_id`) REFERENCES `examinations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to questions` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to sections` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `answers to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES (2, 1, 2, 21, 2, 'D', 1, 1, '2024-06-04 04:39:57', '2024-06-05 06:55:57', NULL);
INSERT INTO `answers` VALUES (3, 1, 2, 22, 2, 'A', 1, 1, '2024-06-04 04:48:32', '2024-06-04 05:02:34', NULL);
INSERT INTO `answers` VALUES (4, 1, 2, 20, 1, 'D', 1, 1, '2024-06-04 04:49:54', '2024-06-05 07:06:42', NULL);
INSERT INTO `answers` VALUES (5, 1, 2, 23, 2, 'C', 1, 1, '2024-06-04 05:05:55', '2024-06-04 05:07:37', NULL);
INSERT INTO `answers` VALUES (6, 2, 3, 21, 2, 'A', 0, 0, '2024-06-05 09:22:35', '2024-06-05 09:22:35', NULL);
INSERT INTO `answers` VALUES (7, 2, 4, 21, 2, 'B', 0, 0, '2024-06-05 13:33:46', '2024-06-05 13:33:46', NULL);
INSERT INTO `answers` VALUES (8, 2, 4, 22, 2, 'A', 1, 1, '2024-06-05 13:33:56', '2024-06-05 13:33:56', NULL);
INSERT INTO `answers` VALUES (9, 2, 4, 23, 2, 'C', 1, 1, '2024-06-05 13:34:18', '2024-06-05 13:34:18', NULL);
INSERT INTO `answers` VALUES (10, 2, 4, 24, 2, 'D', 0, 0, '2024-06-05 13:34:32', '2024-06-05 13:34:32', NULL);
INSERT INTO `answers` VALUES (11, 2, 4, 25, 2, 'A', 1, 1, '2024-06-05 13:34:44', '2024-06-05 13:34:44', NULL);
INSERT INTO `answers` VALUES (12, 2, 4, 26, 2, 'C', 1, 1, '2024-06-05 13:34:51', '2024-06-05 13:34:51', NULL);
INSERT INTO `answers` VALUES (13, 2, 4, 27, 2, 'B', 1, 1, '2024-06-05 13:35:30', '2024-06-05 13:35:30', NULL);
INSERT INTO `answers` VALUES (14, 2, 4, 28, 2, 'B', 1, 1, '2024-06-05 13:35:43', '2024-06-05 13:35:43', NULL);
INSERT INTO `answers` VALUES (15, 2, 4, 29, 2, 'A', 1, 1, '2024-06-05 13:36:11', '2024-06-05 13:36:11', NULL);
INSERT INTO `answers` VALUES (16, 2, 4, 30, 2, 'C', 1, 1, '2024-06-05 13:36:23', '2024-06-05 13:36:23', NULL);
INSERT INTO `answers` VALUES (17, 2, 4, 1, 1, 'D', 0, 0, '2024-06-05 13:37:07', '2024-06-05 13:37:07', NULL);
INSERT INTO `answers` VALUES (18, 2, 4, 5, 1, 'E', 0, 0, '2024-06-05 13:37:30', '2024-06-05 13:37:30', NULL);
INSERT INTO `answers` VALUES (19, 2, 4, 2, 1, 'A', 0, 0, '2024-06-05 13:37:45', '2024-06-05 13:37:45', NULL);
INSERT INTO `answers` VALUES (20, 2, 4, 6, 1, 'A', 0, 0, '2024-06-05 13:38:26', '2024-06-05 13:38:26', NULL);
INSERT INTO `answers` VALUES (21, 2, 4, 7, 1, 'A', 0, 0, '2024-06-05 13:38:38', '2024-06-05 13:38:38', NULL);
INSERT INTO `answers` VALUES (22, 2, 4, 8, 1, 'A', 0, 0, '2024-06-05 13:38:43', '2024-06-05 13:38:43', NULL);
INSERT INTO `answers` VALUES (23, 2, 4, 9, 1, 'A', 1, 1, '2024-06-05 13:38:51', '2024-06-05 13:38:51', NULL);
INSERT INTO `answers` VALUES (24, 2, 4, 10, 1, 'A', 0, 0, '2024-06-05 13:38:57', '2024-06-05 13:38:57', NULL);
INSERT INTO `answers` VALUES (25, 2, 4, 11, 1, 'B', 0, 0, '2024-06-05 13:39:27', '2024-06-05 13:39:27', NULL);
INSERT INTO `answers` VALUES (26, 2, 4, 13, 1, 'C', 0, 0, '2024-06-05 13:40:45', '2024-06-05 13:40:45', NULL);
INSERT INTO `answers` VALUES (27, 2, 4, 14, 1, 'D', 1, 1, '2024-06-05 13:41:24', '2024-06-05 13:41:24', NULL);
INSERT INTO `answers` VALUES (28, 2, 4, 15, 1, 'A', 1, 1, '2024-06-05 13:44:01', '2024-06-05 13:44:01', NULL);
INSERT INTO `answers` VALUES (29, 2, 4, 16, 1, 'B', 0, 0, '2024-06-05 13:44:08', '2024-06-05 13:44:12', NULL);
INSERT INTO `answers` VALUES (30, 2, 4, 17, 1, 'A', 0, 0, '2024-06-05 13:44:21', '2024-06-05 13:44:21', NULL);
INSERT INTO `answers` VALUES (31, 2, 4, 20, 1, 'D', 0, 0, '2024-06-05 13:58:04', '2024-06-05 13:58:04', NULL);
INSERT INTO `answers` VALUES (32, 2, 4, 19, 1, 'A', 0, 0, '2024-06-05 13:58:05', '2024-06-05 13:58:05', NULL);
INSERT INTO `answers` VALUES (33, 2, 4, 18, 1, 'C', 0, 0, '2024-06-05 13:58:06', '2024-06-05 13:58:06', NULL);
INSERT INTO `answers` VALUES (34, 2, 4, 12, 1, 'A', 0, 0, '2024-06-05 13:58:12', '2024-06-05 13:58:12', NULL);
INSERT INTO `answers` VALUES (35, 2, 4, 4, 1, 'C', 0, 0, '2024-06-05 13:58:16', '2024-06-05 13:58:16', NULL);
INSERT INTO `answers` VALUES (36, 2, 4, 3, 1, 'C', 0, 0, '2024-06-05 13:58:17', '2024-06-05 13:58:17', NULL);

-- ----------------------------
-- Table structure for documents
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `documents to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `documents to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of documents
-- ----------------------------

-- ----------------------------
-- Table structure for examinations
-- ----------------------------
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE `examinations`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `exam_id` bigint UNSIGNED NOT NULL,
  `minutes` double NOT NULL,
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `year` year NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `total` double NOT NULL DEFAULT 0,
  `average` double NOT NULL,
  `is_released` tinyint UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `examinations to users`(`user_id` ASC) USING BTREE,
  INDEX `examinations to exams`(`exam_id` ASC) USING BTREE,
  CONSTRAINT `examinations to exams` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `examinations to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of examinations
-- ----------------------------
INSERT INTO `examinations` VALUES (2, 1, 1, 60, 'JUNE', 2024, '2024-06-04 09:45:47', '2024-06-04 13:45:00', 4, 13.333333333333, 1, '2024-06-04 01:45:47', '2024-06-05 07:06:42', NULL);
INSERT INTO `examinations` VALUES (3, 2, 1, 60, 'JUNE', 2024, '2024-06-05 17:21:56', '2024-06-05 18:21:56', 0, 0, 0, '2024-06-05 09:21:56', '2024-06-05 09:21:56', NULL);
INSERT INTO `examinations` VALUES (4, 2, 1, 60, 'JUNE', 2024, '2024-06-05 21:33:20', '2024-06-05 22:33:20', 11, 36.666666666667, 0, '2024-06-05 13:33:20', '2024-06-05 13:44:01', NULL);

-- ----------------------------
-- Table structure for exams
-- ----------------------------
DROP TABLE IF EXISTS `exams`;
CREATE TABLE `exams`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `exam` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `minutes` double NOT NULL,
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `year` year NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `total` double NOT NULL,
  `average` double NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exams to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `exams to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of exams
-- ----------------------------
INSERT INTO `exams` VALUES (1, 1, 'PRACTICE EXAM 1', 60, 'JUNE', 2024, 'exam/665d57520a1e5.txt', 30, 75, 1, '2024-06-03 05:40:34', '2024-06-03 07:58:33', NULL);
INSERT INTO `exams` VALUES (2, 1, 'PRACTICE EXAM 2', 60, 'JUNE', 2024, 'exam/665f1cfe8e75c.txt', 35, 75, 1, '2024-06-04 13:22:04', '2024-06-04 13:56:14', NULL);

-- ----------------------------
-- Table structure for lessons
-- ----------------------------
DROP TABLE IF EXISTS `lessons`;
CREATE TABLE `lessons`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `reviewer_id` bigint UNSIGNED NOT NULL,
  `lesson` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lesson_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `lessons to users`(`user_id` ASC) USING BTREE,
  INDEX `lessons to reviewers`(`reviewer_id` ASC) USING BTREE,
  CONSTRAINT `lessons to reviewers` FOREIGN KEY (`reviewer_id`) REFERENCES `reviewers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `lessons to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lessons
-- ----------------------------
INSERT INTO `lessons` VALUES (1, 1, 7, 'ARITHMETIC AND NUMBER SENSE', 'lesson/665d4ec66b25e.txt', 'lesson-file/665d4ec66b65callmath01.pdf', 1, '2024-06-03 05:04:06', '2024-06-03 05:04:06', NULL);
INSERT INTO `lessons` VALUES (2, 1, 7, 'BASIC ALGEBRA', 'lesson/665d4fb5e6bc1.txt', 'lesson-file/665d4fb5e7106knapp-basic.pdf', 1, '2024-06-03 05:08:05', '2024-06-03 05:08:05', NULL);
INSERT INTO `lessons` VALUES (3, 1, 7, 'BASIC GEOMETRY', 'lesson/665d50f29732a.txt', 'lesson-file/665d50f297e6aGeometryHandbook.pdf', 1, '2024-06-03 05:13:22', '2024-06-03 05:13:22', NULL);
INSERT INTO `lessons` VALUES (4, 1, 7, 'BASIC STATISTICS', 'lesson/665d514bd8231.txt', 'lesson-file/665d514bd862estat_-book_introduction_to_statistics.pdf', 1, '2024-06-03 05:14:51', '2024-06-03 05:14:51', NULL);
INSERT INTO `lessons` VALUES (5, 1, 5, 'GRAMMAR', 'lesson/665d527249afa.txt', 'lesson-file/665d527249edcFree-English-Grammar-eBook-Beginner.pdf', 1, '2024-06-03 05:19:46', '2024-06-03 05:19:46', NULL);
INSERT INTO `lessons` VALUES (6, 1, 2, 'VOCABULARY', 'lesson/665d531e7fdbf.txt', 'lesson-file/665d531e80556English_Vocabulary_Builder - learnenglishteam.com.pdf', 1, '2024-06-03 05:22:38', '2024-06-03 05:22:38', NULL);
INSERT INTO `lessons` VALUES (7, 1, 5, 'ANALYTICAL ABILITY MODULES AND READINGS', 'lesson/665d542f118ba.txt', 'lesson-file/665d542f11e97Handbook of Analytical Techniques - 2001 - Günzler - Frontmatter.pdf', 1, '2024-06-03 05:27:11', '2024-06-03 05:27:11', NULL);
INSERT INTO `lessons` VALUES (8, 1, 8, 'CLERICAL ABILITY MODULES AND READINGS', 'lesson/665d550b0d228.txt', 'lesson-file/665d550b0d776Clerical-v2.pdf', 1, '2024-06-03 05:30:51', '2024-06-03 05:30:51', NULL);
INSERT INTO `lessons` VALUES (9, 1, 9, 'GENERAL INFORMATION MODULES AND READINGS', 'lesson/665d563824aa3.txt', 'lesson-file/665d563824f72Peace_Education Training_Manual_for_GPPAC-SEA_2015.pdf', 1, '2024-06-03 05:35:52', '2024-06-03 05:35:52', NULL);

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `exam_id` bigint UNSIGNED NOT NULL,
  `section_id` bigint UNSIGNED NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `answer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `points` double NOT NULL DEFAULT 0,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `questions to users`(`user_id` ASC) USING BTREE,
  INDEX `questions to exams`(`exam_id` ASC) USING BTREE,
  CONSTRAINT `questions to exams` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `questions to sections` FOREIGN KEY (`id`) REFERENCES `sections` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `questions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES (1, 1, 1, 1, 'question/665d63c4b5877.txt', 'A', 1, 1, 1, '2024-06-03 06:33:40', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (2, 1, 1, 1, 'question/665d6bfd42eb1.txt', 'D', 1, 3, 1, '2024-06-03 07:08:45', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (3, 1, 1, 1, 'question/665d6cac08355.txt', 'A', 1, 4, 1, '2024-06-03 07:11:40', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (4, 1, 1, 1, 'question/665d6cfa46bf7.txt', 'B', 1, 5, 1, '2024-06-03 07:12:58', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (5, 1, 1, 1, 'question/665d6e03a3713.txt', 'D', 1, 2, 1, '2024-06-03 07:17:23', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (6, 1, 1, 1, 'question/665d71d8a1cfb.txt', 'C', 1, 6, 1, '2024-06-03 07:33:44', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (7, 1, 1, 1, 'question/665d724e006e9.txt', 'B', 1, 7, 1, '2024-06-03 07:35:42', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (8, 1, 1, 1, 'question/665d7290e5834.txt', 'B', 1, 8, 1, '2024-06-03 07:36:48', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (9, 1, 1, 1, 'question/665d72d726069.txt', 'A', 1, 9, 1, '2024-06-03 07:37:59', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (10, 1, 1, 1, 'question/665d7320aed8d.txt', 'D', 1, 10, 1, '2024-06-03 07:39:12', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (11, 1, 1, 1, 'question/665d7376d36ee.txt', 'A', 1, 11, 1, '2024-06-03 07:40:38', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (12, 1, 1, 1, 'question/665d73b43c7d3.txt', 'B', 1, 12, 1, '2024-06-03 07:41:40', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (13, 1, 1, 1, 'question/665d73dc33580.txt', 'D', 1, 13, 1, '2024-06-03 07:42:20', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (14, 1, 1, 1, 'question/665d73ff64dd9.txt', 'D', 1, 14, 1, '2024-06-03 07:42:55', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (15, 1, 1, 1, 'question/665d7452c9eae.txt', 'A', 1, 15, 1, '2024-06-03 07:44:18', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (16, 1, 1, 1, 'question/665d748003c70.txt', 'A', 1, 16, 1, '2024-06-03 07:45:04', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (17, 1, 1, 1, 'question/665d74a7c9fbe.txt', 'B', 1, 17, 1, '2024-06-03 07:45:43', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (18, 1, 1, 1, 'question/665d74d31f3ab.txt', 'B', 1, 18, 1, '2024-06-03 07:46:27', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (19, 1, 1, 1, 'question/665d750642ec8.txt', 'B', 1, 19, 1, '2024-06-03 07:47:18', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (20, 1, 1, 1, 'question/665d752d3d50e.txt', 'A', 1, 20, 1, '2024-06-03 07:47:57', '2024-06-03 07:49:34', NULL);
INSERT INTO `questions` VALUES (21, 1, 1, 2, 'question/665d760014361.txt', 'D', 1, 1, 1, '2024-06-03 07:51:28', '2024-06-03 07:51:28', NULL);
INSERT INTO `questions` VALUES (22, 1, 1, 2, 'question/665d762a71edf.txt', 'A', 1, 2, 1, '2024-06-03 07:52:10', '2024-06-03 07:52:10', NULL);
INSERT INTO `questions` VALUES (23, 1, 1, 2, 'question/665d7660e1753.txt', 'C', 1, 3, 1, '2024-06-03 07:53:04', '2024-06-03 07:53:04', NULL);
INSERT INTO `questions` VALUES (24, 1, 1, 2, 'question/665d767dc4a71.txt', 'B', 1, 4, 1, '2024-06-03 07:53:33', '2024-06-03 07:53:33', NULL);
INSERT INTO `questions` VALUES (25, 1, 1, 2, 'question/665d76b85f327.txt', 'A', 1, 5, 1, '2024-06-03 07:54:32', '2024-06-03 07:54:32', NULL);
INSERT INTO `questions` VALUES (26, 1, 1, 2, 'question/665d76d072a41.txt', 'C', 1, 6, 1, '2024-06-03 07:54:56', '2024-06-03 07:54:56', NULL);
INSERT INTO `questions` VALUES (27, 1, 1, 2, 'question/665d770b528b6.txt', 'B', 1, 7, 1, '2024-06-03 07:55:55', '2024-06-03 07:55:55', NULL);
INSERT INTO `questions` VALUES (28, 1, 1, 2, 'question/665d7734a7554.txt', 'B', 1, 8, 1, '2024-06-03 07:56:36', '2024-06-03 07:56:36', NULL);
INSERT INTO `questions` VALUES (29, 1, 1, 2, 'question/665d7774ddd34.txt', 'A', 1, 9, 1, '2024-06-03 07:57:40', '2024-06-03 07:57:40', NULL);
INSERT INTO `questions` VALUES (30, 1, 1, 2, 'question/665d77a9707bc.txt', 'C', 1, 10, 1, '2024-06-03 07:58:33', '2024-06-03 07:58:33', NULL);
INSERT INTO `questions` VALUES (31, 1, 2, 3, 'question/665f159e33f42.txt', 'C', 1, 1, 1, '2024-06-04 13:24:46', '2024-06-04 13:24:46', NULL);
INSERT INTO `questions` VALUES (32, 1, 2, 3, 'question/665f15c4119bd.txt', 'A', 1, 2, 1, '2024-06-04 13:25:24', '2024-06-04 13:25:24', NULL);
INSERT INTO `questions` VALUES (33, 1, 2, 3, 'question/665f15f65579f.txt', 'C', 1, 3, 1, '2024-06-04 13:26:14', '2024-06-04 13:26:14', NULL);
INSERT INTO `questions` VALUES (34, 1, 2, 3, 'question/665f1619ba42b.txt', 'A', 1, 4, 1, '2024-06-04 13:26:49', '2024-06-04 13:26:49', NULL);
INSERT INTO `questions` VALUES (35, 1, 2, 3, 'question/665f1643919b4.txt', 'D', 1, 5, 1, '2024-06-04 13:27:31', '2024-06-04 13:27:31', NULL);
INSERT INTO `questions` VALUES (36, 1, 2, 3, 'question/665f169490d77.txt', 'D', 1, 6, 1, '2024-06-04 13:28:52', '2024-06-04 13:28:52', NULL);
INSERT INTO `questions` VALUES (37, 1, 2, 3, 'question/665f16c550863.txt', 'A', 1, 7, 1, '2024-06-04 13:29:41', '2024-06-04 13:29:41', NULL);
INSERT INTO `questions` VALUES (38, 1, 2, 3, 'question/665f16ecc4736.txt', 'D', 1, 8, 1, '2024-06-04 13:30:20', '2024-06-04 13:30:20', NULL);
INSERT INTO `questions` VALUES (39, 1, 2, 3, 'question/665f1708a18d4.txt', 'A', 1, 9, 1, '2024-06-04 13:30:48', '2024-06-04 13:30:48', NULL);
INSERT INTO `questions` VALUES (40, 1, 2, 3, 'question/665f172da12e1.txt', 'C', 1, 10, 1, '2024-06-04 13:31:25', '2024-06-04 13:31:25', NULL);
INSERT INTO `questions` VALUES (41, 1, 2, 3, 'question/665f174f20a4c.txt', 'C', 1, 11, 1, '2024-06-04 13:31:59', '2024-06-04 13:31:59', NULL);
INSERT INTO `questions` VALUES (42, 1, 2, 3, 'question/665f178972b62.txt', 'A', 1, 12, 1, '2024-06-04 13:32:57', '2024-06-04 13:32:57', NULL);
INSERT INTO `questions` VALUES (43, 1, 2, 3, 'question/665f17a85b606.txt', 'D', 1, 13, 1, '2024-06-04 13:33:28', '2024-06-04 13:33:28', NULL);
INSERT INTO `questions` VALUES (44, 1, 2, 3, 'question/665f17cf731b2.txt', 'B', 1, 14, 1, '2024-06-04 13:34:07', '2024-06-04 13:34:07', NULL);
INSERT INTO `questions` VALUES (45, 1, 2, 3, 'question/665f17f724a5b.txt', 'A', 1, 15, 1, '2024-06-04 13:34:47', '2024-06-04 13:34:47', NULL);
INSERT INTO `questions` VALUES (46, 1, 2, 3, 'question/665f181d538bc.txt', 'A', 1, 16, 1, '2024-06-04 13:35:25', '2024-06-04 13:35:25', NULL);
INSERT INTO `questions` VALUES (47, 1, 2, 3, 'question/665f185015bfd.txt', 'D', 1, 17, 1, '2024-06-04 13:36:16', '2024-06-04 13:36:16', NULL);
INSERT INTO `questions` VALUES (48, 1, 2, 3, 'question/665f18803c9de.txt', 'A', 1, 18, 1, '2024-06-04 13:37:04', '2024-06-04 13:37:04', NULL);
INSERT INTO `questions` VALUES (49, 1, 2, 3, 'question/665f18aadc90a.txt', 'D', 1, 19, 1, '2024-06-04 13:37:46', '2024-06-04 13:37:46', NULL);
INSERT INTO `questions` VALUES (50, 1, 2, 3, 'question/665f18d8f2054.txt', 'B', 1, 20, 1, '2024-06-04 13:38:32', '2024-06-04 13:38:32', NULL);
INSERT INTO `questions` VALUES (51, 1, 2, 4, 'question/665f19a0ae8dd.txt', 'A', 1, 10, 1, '2024-06-04 13:41:52', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (52, 1, 2, 4, 'question/665f19c2987e4.txt', 'B', 1, 1, 1, '2024-06-04 13:42:26', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (53, 1, 2, 4, 'question/665f1ab5e09e2.txt', 'C', 1, 2, 1, '2024-06-04 13:46:29', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (54, 1, 2, 4, 'question/665f1b1ed9f64.txt', 'A', 1, 3, 1, '2024-06-04 13:48:14', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (55, 1, 2, 4, 'question/665f1b3422873.txt', 'A', 1, 4, 1, '2024-06-04 13:48:36', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (56, 1, 2, 4, 'question/665f1b6ec2381.txt', 'C', 1, 5, 1, '2024-06-04 13:49:34', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (57, 1, 2, 4, 'question/665f1b8c9060f.txt', 'B', 1, 6, 1, '2024-06-04 13:50:04', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (58, 1, 2, 4, 'question/665f1bb10b4bf.txt', 'B', 1, 7, 1, '2024-06-04 13:50:41', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (59, 1, 2, 4, 'question/665f1bdc093a4.txt', 'C', 1, 8, 1, '2024-06-04 13:51:24', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (60, 1, 2, 4, 'question/665f1bfe6a1ee.txt', 'A', 1, 9, 1, '2024-06-04 13:51:58', '2024-06-04 13:52:15', NULL);
INSERT INTO `questions` VALUES (61, 1, 2, 4, 'question/665f1c275224e.txt', 'A', 1, 11, 1, '2024-06-04 13:52:39', '2024-06-04 13:52:39', NULL);
INSERT INTO `questions` VALUES (62, 1, 2, 4, 'question/665f1c3d1f660.txt', 'A', 1, 12, 1, '2024-06-04 13:53:01', '2024-06-04 13:53:01', NULL);
INSERT INTO `questions` VALUES (63, 1, 2, 4, 'question/665f1c55bc1b6.txt', 'D', 1, 13, 1, '2024-06-04 13:53:25', '2024-06-04 13:53:25', NULL);
INSERT INTO `questions` VALUES (64, 1, 2, 4, 'question/665f1cc6ada9f.txt', 'C', 1, 14, 1, '2024-06-04 13:54:02', '2024-06-04 13:55:18', NULL);
INSERT INTO `questions` VALUES (65, 1, 2, 4, 'question/665f1c9a93d5c.txt', 'A', 1, 15, 1, '2024-06-04 13:54:34', '2024-06-04 13:54:34', NULL);

-- ----------------------------
-- Table structure for reviewers
-- ----------------------------
DROP TABLE IF EXISTS `reviewers`;
CREATE TABLE `reviewers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `reviewer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `reviewers to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `reviewers to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of reviewers
-- ----------------------------
INSERT INTO `reviewers` VALUES (1, 1, 'ENGLISH-FILIPINO GRAMMAR AND CORRECT USAGE', 'reviewer/665d461e673f9.txt', 1, '2024-06-03 04:27:10', '2024-06-03 04:27:10', NULL);
INSERT INTO `reviewers` VALUES (2, 1, 'FILIPINO-ENGLISH VOCABULARY', 'reviewer/6660147adc76f.txt', 1, '2024-06-03 04:30:32', '2024-06-05 07:32:10', NULL);
INSERT INTO `reviewers` VALUES (3, 1, 'CORRECT SPELLING AND IDIOMATIC EXPRESSIONS', 'reviewer/665d4810eb293.txt', 1, '2024-06-03 04:35:28', '2024-06-03 04:35:28', NULL);
INSERT INTO `reviewers` VALUES (4, 1, 'ANALOGY AND LOGIC', 'reviewer/665d48f5157bc.txt', 1, '2024-06-03 04:39:17', '2024-06-03 04:39:17', NULL);
INSERT INTO `reviewers` VALUES (5, 1, 'READING COMPREHENSION', 'reviewer/665d49843e06d.txt', 1, '2024-06-03 04:41:40', '2024-06-03 04:41:40', NULL);
INSERT INTO `reviewers` VALUES (6, 1, 'PARAGRAPH ORGANIZATION', 'reviewer/665d4ab0ac383.txt', 1, '2024-06-03 04:46:40', '2024-06-03 04:46:40', NULL);
INSERT INTO `reviewers` VALUES (7, 1, 'NUMERICAL REASONING', 'reviewer/665d4b730257a.txt', 1, '2024-06-03 04:49:55', '2024-06-03 04:49:55', NULL);
INSERT INTO `reviewers` VALUES (8, 1, 'CLERICAL OPERATIONS', 'reviewer/665d4c653f3cd.txt', 1, '2024-06-03 04:53:57', '2024-06-03 04:53:57', NULL);
INSERT INTO `reviewers` VALUES (9, 1, 'GENERAL INFORMATION', 'reviewer/665d5546d5c5a.txt', 1, '2024-06-03 05:31:50', '2024-06-03 05:31:50', NULL);

-- ----------------------------
-- Table structure for sections
-- ----------------------------
DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `exam_id` bigint UNSIGNED NOT NULL,
  `reviewer_id` bigint UNSIGNED NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sections to users`(`user_id` ASC) USING BTREE,
  INDEX `sections to exams`(`exam_id` ASC) USING BTREE,
  INDEX `sections to reviewers`(`reviewer_id` ASC) USING BTREE,
  CONSTRAINT `sections to exams` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sections to reviewers` FOREIGN KEY (`reviewer_id`) REFERENCES `reviewers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sections to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sections
-- ----------------------------
INSERT INTO `sections` VALUES (1, 1, 1, 4, 'section/665d5c220aaed.txt', 2, 1, '2024-06-03 06:01:06', '2024-06-03 06:41:26', NULL);
INSERT INTO `sections` VALUES (2, 1, 1, 8, 'section/665d65900c70f.txt', 1, 1, '2024-06-03 06:41:20', '2024-06-03 06:41:26', NULL);
INSERT INTO `sections` VALUES (3, 1, 2, 2, 'section/665f15852c745.txt', 1, 1, '2024-06-04 13:24:21', '2024-06-04 13:24:21', NULL);
INSERT INTO `sections` VALUES (4, 1, 2, 3, 'section/665f198c55dec.txt', 2, 1, '2024-06-04 13:41:32', '2024-06-04 13:41:32', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `contact_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_admin` tinyint NOT NULL DEFAULT 0,
  `is_examinee` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remember_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'RODS', 'Rods_2001', 'cabotaje.rodrigo@gmail.com', '09100575676', '$2y$10$NcMYNSMApIfVevs2gkscOO1fjvVA0RkbfyMbnk.qKPyWeqzIoGOqG', 1, 1, 1, '666025033ec000.25702700 1717576963', 'NfrbeGkZA0wQXvtV6OT6lzvzoruzVGC0cR8lHQvdRnBwtqzlXqwfsbXiG7g4', '2024-05-27 02:23:46', '2024-06-05 08:42:43', NULL);
INSERT INTO `users` VALUES (2, 'TRISHA ROBIEGO SERRANO', 'Shangqt_06', 'trisha@gmail.com', '09950546601', '$2y$10$sxN.C/2J2ytg2cJP5A6ETu3PnOzpHXfwwhfgVTkk66rH7qOt30rXW', 0, 1, 1, '666057e0599bb0.36704100 1717589984', NULL, '2024-06-05 08:00:29', '2024-06-05 12:19:44', NULL);

SET FOREIGN_KEY_CHECKS = 1;
