Turbolinks.start();

$('a[turbo-links]').click(function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
    Turbolinks.visit(href, {
        action:'replace'
    });
});

$('input.required, select.required').on('input focus', function (e) {
    var regex = /^(.){1,}$/;
    var value = $(this).val();

    if(!value.match(regex)){
        $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
        return true;
    }

    $(this).removeClass('is-invalid').next('small').empty();

});

$('input[type="number"]').on('keypress', function (e) {

    var key = e.key;
    var regex = /^([0-9\.]){1,}$/;
    var value = $(this).val();

    if(!key.match(regex)){
        e.preventDefault();
    }

}).on('blur', function (e) {
    var value = $(this).val();
    $(this).val(parseFloat(value).toFixed(2));

}).on('input', function (e) {

    var regex = /^(.){1,}$/;
    var value = $(this).val();

    if(!value.match(regex)){
        $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
        return true;
    }

    if(parseInt(value) < parseInt(1)){
        $(this).addClass('is-invalid').next('small').text('This Field Must Not Lower Than 1');
        return true;
    }

    $(this).removeClass('is-invalid').next('small').empty();

});

function swal(icon, title, text){
    Swal.fire({
        icon: icon,
        title: title,
        text: text,
        timer: 5000,
        timerProgressBar:true,
    });
}

function toast(icon, title, text){
    Swal.fire({
        icon: icon,
        title: title,
        text: text,
        timer: 5000,
        timerProgressBar:true,
        toast:true,
        position:'top-right',
    });
}

function loader(icon, title, text) {
    Swal.fire({
        icon: icon,
        title: title,
        text: text,
        allowOutsideClick: false,
        showConfirmButton: false,
        timerProgressBar: false,
        didOpen: function () {
            Swal.showLoading();
        }
    });
}
