'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'dashboards/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    $('#form').submit(function (e) {
        e.preventDefault();
        getExaminationsPassingRate();
        examination_chart.destroy();
        getExaminationsChart();
        examination_most_take_chart.destroy();
        getExaminationsMostTakeChart();
    });

    function counts() {
        $.ajax({
            url: baseurl+'counts',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#total-users, #total-exams, #total-reviewers').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-users').text(parseInt(data.users));
            $('#total-exams').text(parseInt(data.exams));
            $('#total-reviewers').text(parseInt(data.reviewers));
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getExaminationsCount() {
        $.ajax({
            url: baseurl+'getExaminationsCount',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#total-examinations-today, #total-examinations-week, #total-examinations-month, #total-examinations-year').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-examinations-today').text(parseInt(data.today));
            $('#total-examinations-week').text(parseInt(data.week));
            $('#total-examinations-month').text(parseInt(data.month));
            $('#total-examinations-year').text(parseInt(data.year));
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getExaminationsPassingRate() {
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        $.ajax({
            url: baseurl+'getExaminationsPassingRate?start_date='+(startDate)+'&end_date='+(endDate),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#examination-most-take').empty();
            },
        }).done(function (data, status, xhr) {
            $.map(data, function (data, key) {
                $('#examination-most-take').append('<div class="list-group-item">'+
                    '<div class="row align-items-center">'+
                    '<div class="col">'+
                    '<strong>'+((data.exam).toUpperCase())+'</strong>'+
                    '<div class="my-0 text-muted small">Passing Rate</div>'+
                    '</div>'+
                    '<div class="col-auto">'+
                    '<strong>'+(parseFloat(data.total).toFixed(2))+'%</strong>'+
                    '<div class="progress mt-2" style="height: 4px;">'+
                    '<div class="progress-bar" role="progressbar" style="width: '+(parseFloat(data.total).toFixed(2))+'%" aria-valuenow="'+(parseFloat(data.total).toFixed(2))+'" aria-valuemin="0" aria-valuemax="100"></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getLeaderboardsWeek() {
        $.ajax({
            url: baseurl+'getLeaderboardsWeek',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#week-leaderboards').empty();
            },
        }).done(function (data, status, xhr) {
            $.map(data, function (data, key) {
                $('#week-leaderboards').append('<div class="list-group-item">'+
                    '<div class="row align-items-center">' +
                    '  <div class="col-3 col-sm-2 col-md-3 col-lg-3">'+
                    '<img src="/civil-service-practice-exam/public/img/user-avatar/'+(parseInt(data.user_id))+'.png" alt="#" class="thumbnail-sm" loading="lazy">'+
                    '</div>'+
                    '<div class="col">'+
                    '<strong>'+((data.name).toUpperCase())+'</strong>'+
                    '<div class="my-0 text-muted small">Total Average</div>'+
                    '</div>'+
                    '<div class="col-auto">'+
                    '<strong>'+(parseFloat(data.total).toFixed(2))+'%</strong>'+
                    '<div class="progress mt-2" style="height: 4px;">'+
                    '<div class="progress-bar" role="progressbar" style="width: '+(parseFloat(data.total).toFixed(2))+'%" aria-valuenow="'+(parseFloat(data.total).toFixed(2))+'" aria-valuemin="0" aria-valuemax="100"></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getLeaderboardsMonth() {
        $.ajax({
            url: baseurl+'getLeaderboardsMonth',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#month-leaderboards').empty();
            },
        }).done(function (data, status, xhr) {
            $.map(data, function (data, key) {
                $('#month-leaderboards').append('<div class="list-group-item">'+
                    '<div class="row align-items-center">' +
                    '  <div class="col-3 col-sm-2 col-md-3 col-lg-3">'+
                    '<img src="/civil-service-practice-exam/public/img/user-avatar/'+(parseInt(data.user_id))+'.png" alt="#" class="thumbnail-sm" loading="lazy">'+
                    '</div>'+
                    '<div class="col">'+
                    '<strong>'+((data.name).toUpperCase())+'</strong>'+
                    '<div class="my-0 text-muted small">Total Average</div>'+
                    '</div>'+
                    '<div class="col-auto">'+
                    '<strong>'+(parseFloat(data.total).toFixed(2))+'%</strong>'+
                    '<div class="progress mt-2" style="height: 4px;">'+
                    '<div class="progress-bar" role="progressbar" style="width: '+(parseFloat(data.total).toFixed(2))+'%" aria-valuenow="'+(parseFloat(data.total).toFixed(2))+'" aria-valuemin="0" aria-valuemax="100"></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getLeaderboardsYear() {
        $.ajax({
            url: baseurl+'getLeaderboardsYear',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#year-leaderboards').empty();
            },
        }).done(function (data, status, xhr) {
            $.map(data, function (data, key) {
                $('#year-leaderboards').append('<div class="list-group-item">'+
                    '<div class="row align-items-center">' +
                    '  <div class="col-3 col-sm-2 col-md-3 col-lg-3">'+
                    '<img src="/civil-service-practice-exam/public/img/user-avatar/'+(parseInt(data.user_id))+'.png" alt="#" class="thumbnail-sm" loading="lazy">'+
                    '</div>'+
                    '<div class="col">'+
                    '<strong>'+((data.name).toUpperCase())+'</strong>'+
                    '<div class="my-0 text-muted small">Total Average</div>'+
                    '</div>'+
                    '<div class="col-auto">'+
                    '<strong>'+(parseFloat(data.total).toFixed(2))+'%</strong>'+
                    '<div class="progress mt-2" style="height: 4px;">'+
                    '<div class="progress-bar" role="progressbar" style="width: '+(parseFloat(data.total).toFixed(2))+'%" aria-valuenow="'+(parseFloat(data.total).toFixed(2))+'" aria-valuemin="0" aria-valuemax="100"></div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    var examination_canvas = document.querySelector('#examination-chart').getContext('2d');
    var examination_chart;

    function getExaminations() {
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getExaminations?start_date='+(startDate)+'&end_date='+(endDate),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.month).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getExaminationsChart() {
        examination_chart =  new Chart(examination_canvas, {
            type: 'bar',
            data: {
                labels: getExaminations().label,
                datasets: [
                    {
                        label:'Total',
                        data: getExaminations().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 1
                    },
                    title: {
                        display: false,
                        text: null,
                        font: {
                            size: 16,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var examination_most_take_canvas = document.querySelector('#examination-most-take-chart').getContext('2d');
    var examination_most_take_chart;

    function getExaminationsMostTake() {
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getExaminationsMostTake?start_date='+(startDate)+'&end_date='+(endDate),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.exam).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getExaminationsMostTakeChart() {
        examination_most_take_chart =  new Chart(examination_most_take_canvas, {
            type: 'polarArea',
            data: {
                labels: getExaminationsMostTake().label,
                datasets: [
                    {
                        label:'Total',
                        data: getExaminationsMostTake().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 2
                    },
                    title: {
                        display: false,
                        text: null,
                        font: {
                            size: 16,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    counts();
    getExaminationsCount();
    getExaminationsPassingRate();
    getLeaderboardsWeek();
    getLeaderboardsMonth();
    getLeaderboardsYear();
    getExaminationsChart();
    getExaminationsMostTakeChart();

    setInterval(function (e) {
        counts();
        getExaminationsCount();
        getExaminationsPassingRate();
        getLeaderboardsWeek();
        getLeaderboardsMonth();
        getLeaderboardsYear();
        examination_chart.destroy();
        getExaminationsChart();
        examination_most_take_chart.destroy();
        getExaminationsMostTakeChart();
    }, parseInt(900000));

});
