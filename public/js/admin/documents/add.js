'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                loader('info', null, 'Please Wait');
                $('small').empty();
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(parseInt(Number(checked)));
    });

    $('#file').change(function (e) {

        var filereader = new FileReader();
        var file = e.target.files[0];
        var mimes = ['application/pdf'];

        if(!mimes.includes(file.type)){
            $(this).addClass('is-invalid').next('small').text('Only PDF Is Allowed');
            return true;
        }

        filereader.addEventListener('load', function (e) {
            $('#embed').attr('src', e.target.result).attr('height', parseInt(1000)).attr('width', parseInt(800));
        });

        filereader.readAsDataURL(file);
        $(this).removeClass('is-invalid').next('small').empty();
    });

});