'use strict';
$(document).ready(function () {
    var baseurl = mainurl+'questions/';
    var url = '';
    var isActive = [
        '<span class="text-danger"><i class="fe fe-x f-32"></i></span>',
        '<span class="text-success"><i class="fe fe-check f-32"></i></span>',
    ];
    var startDate = moment($('#start-date').val()).format('Y-MM-DD');
    var endDate = moment($('#end-date').val()).format('Y-MM-DD');
    var records = $('#records').val();

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'Rlfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getQuestions',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(7)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [4],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.points)));
                }
            },
            {
                targets: [5],
                data: null,
                render: function(data,type,row,meta){
                    return isActive[parseInt(row.is_active)];
                }
            },
            {
                targets: [7],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.updated_at).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [8],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'exams.exam'},
            { data: 'sections.reviewers.reviewer'},
            { data: 'answer'},
            { data: 'points'},
            { data: 'is_active'},
            { data: 'users.name'},
            { data: 'updated_at'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'view/'+(dataId);
        Swal.fire({
            title: 'Open In New Window',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            denyButtonColor: '#808080',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            denyButtonText: 'Cancel',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }else if(result.isDenied){
                Swal.close();
            }else{
                Turbolinks.visit(href, {action: 'advance'});
            }
        });
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', null, data.responseJSON.message);
                });
            }
        });
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', true);
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var records = $('#records').val();
        table.ajax.url(baseurl+'getQuestions?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records)).load(function () {
            $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', false);
        }, true);
    });

});