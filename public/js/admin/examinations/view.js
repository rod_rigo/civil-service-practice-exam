'use strict';
$(document).ready(function (e) {

    var correction = [
        'Incorrect',
        'Correct'
    ];
    $('input').prop('disabled', true);

    $('#percircle').percircle({
        percent: parseFloat(average).toFixed(2),
        text: parseFloat(average).toFixed(2),
    });

    $('#timer-container').mouseenter(function (e) {
        $(this).addClass('opacity');
    }).mouseleave(function (e) {
        $(this).removeClass('opacity');
    }).draggable({
        scroll: true,
        scrollSensitivity: 100,
        cursor: 'move',
        containment: 'body',
        drag: function( event, ui ) {
            if(parseInt(ui.position.top) < parseInt(-300)){
                $('#timer-container').removeAttr('style');
            }
        },
    });

    $('.is-correct').click(function (e) {
        e.preventDefault();
        var element = $(this);
        var dataId = $(this).attr('data-id');
        var isCorrect = $(this).attr('data-is-correct');
        var href = mainurl+'answers/isCorrect/'+(parseInt(dataId))+'/'+(parseInt(isCorrect));
        Swal.fire({
            title: 'Mark As '+(correction[parseInt(isCorrect)]),
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'POST',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    element.removeClass('btn-danger').removeClass('btn-success');
                    if(parseInt(isCorrect)){
                        element.addClass('btn-success').html('<i class="fe fe-check" style="font-size: 20px !important;"></i>').attr('data-is-correct', parseInt(0));
                    }else{
                        element.addClass('btn-danger').html('<i class="fe fe-x" style="font-size: 20px !important;"></i>').attr('data-is-correct', parseInt(1));
                    }

                    $('#scores').html((parseInt(data.total))+'/'+(parseInt(total)));
                    $('#percircle').percircle({
                        text: parseFloat(data.average).toFixed(2),
                        percent: parseFloat(data.average).toFixed(2)
                    });
                    swal('success', null, data.message);
                }).fail(function (data, status, xhr) {
                    swal('error', null, data.responseJSON.message);
                });
            }
        });
    });

});