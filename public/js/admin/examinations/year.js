'use strict';
$(document).ready(function () {
    var baseurl = mainurl+'examinations/';
    var url = '';
    var isReleased = [
        '<span class="text-danger"><i class="fe fe-x f-32"></i></span>',
        '<span class="text-success"><i class="fe fe-check f-32"></i></span>',
    ];

    var title = function () {
        return 'Examinations Of '+(moment().startOf('year').format('Y-MM-DD'))+' - '+(moment().endOf('year').format('Y-MM-DD'));
    };

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'RlBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getExaminationsYear',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(9)').addClass('text-center');
        },
        buttons: [
            {
                extend: 'print',
                title: 'Print',
                text: 'Print',
                attr:  {
                    id: 'print',
                    class:'btn btn-secondary rounded-0',
                },
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10px' )
                        .prepend('');
                    $(win.document.body).find( 'table tbody' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'excelHtml5',
                attr:  {
                    id: 'excel',
                    class:'btn btn-success rounded-0',
                },
                title: title,
                text: 'Excel',
                tag: 'button',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'Excel',
                        text:'Export To Excel?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'pdfHtml5',
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                text: 'PDF',
                title: title,
                tag: 'button',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,9,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'PDF',
                        text:'Export To PDF?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                customize: function(doc) {
                    doc.pageMargins = [2, 2, 2, 2 ];
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.styles.tableHeader.fontSize = 7;
                    doc.styles.tableBodyEven.fontSize = 7;
                    doc.styles.tableBodyOdd.fontSize = 7;
                    doc.styles.tableFooter.fontSize = 7;
                },
                download: 'open',
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [3],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.minutes)));
                }
            },
            {
                targets: [6],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.total)));
                }
            },
            {
                targets: [7],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.average)));
                }
            },
            {
                targets: [8],
                data: null,
                render: function(data,type,row,meta){
                    return isReleased[parseInt(row.is_released)];
                }
            },
            {
                targets: [9],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.updated_at).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [10],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    var release = (row.is_released)? '':' | <a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-success rounded-0 text-white release" title="Release">Release</a>';
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a>'+
                        (release);
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'users.name'},
            { data: 'exams.exam'},
            { data: 'minutes'},
            { data: 'month'},
            { data: 'year'},
            { data: 'total'},
            { data: 'average'},
            { data: 'is_released'},
            { data: 'updated_at'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'view/'+(dataId);
        Swal.fire({
            title: 'Open In New Window',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            denyButtonColor: '#808080',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            denyButtonText: 'Cancel',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }else if(result.isDenied){
                Swal.close();
            }else{
                Turbolinks.visit(href, {action: 'advance'});
            }
        });
    });

    datatable.on('click','.release',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'release/'+(parseInt(dataId));
        Swal.fire({
            title: 'Release Data',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'POST',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', null, data.responseJSON.message);
                });
            }
        });
    });

    setInterval(function (e) {
        table.ajax.reload(null, false);
    }, parseInt(900000));

});