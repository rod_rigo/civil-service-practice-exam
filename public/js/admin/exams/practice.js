'use strict';
$(document).ready(function () {

    var baseurl = mainurl+'examinations/';
    var element = '';
    var url = '';

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl + 'add',
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                loader('info', null, 'Please Wait');
                $('small').empty();
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            element.attr('data-url', parseInt(data.id));
            $('#form')[0].reset();
            $('#modal').modal('toggle');
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect, {action: 'replace'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#toggle-modal').click(function (e) {
        url = 'add';
        $('#modal').modal('toggle');
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#form')[0].reset();
        $('small').empty();
        $('.form-control').removeClass('is-invalid');
        $('button[type="reset"]').fadeIn(100);
        $('button[type="reset"], button[type="submit"]').prop('disabled', false);
    });

    $('.exam').click(function (e) {
        var dataId = $(this).attr('data-id');
        var dataUrl = $(this).attr('data-url');
        if(parseInt(dataUrl)){
            Turbolinks.visit(mainurl+'examinations/exam/'+(parseInt(dataUrl))+'/'+(parseInt(dataId)));
        }else{
            element = $(this);
            getExam(dataId);
        }
    });

    function getExam(id) {
        $.ajax({
            url: mainurl+ 'exams/getExam/'+(id),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                loader('info', null, 'Please Wait');
            },
        }).done(function (data, status, xhr) {
            url = 'add';
            $('#minutes').val(data.minutes);
            $('#exam-id').val(data.id);
            $('#modal').modal('toggle');
            Swal.close();
        }).fail(function (data, status, xhr) {

        });
    }

});
