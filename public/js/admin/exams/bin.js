'use strict';
$(document).ready(function () {
    var baseurl = mainurl+'exams/';
    var url = '';
    var isActive = [
        '<span class="text-danger"><i class="fe fe-x f-32"></i></span>',
        '<span class="text-success"><i class="fe fe-check f-32"></i></span>',
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'Rlfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getExamsDeleted',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(7)').addClass('text-center');
        },
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [2],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.minutes)));
                }
            },
            {
                targets: [5],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.total)));
                }
            },
            {
                targets: [6],
                data: null,
                render: function(data,type,row,meta){
                    return (new Intl.NumberFormat().format(parseFloat(row.average)));
                }
            },
            {
                targets: [7],
                data: null,
                render: function(data,type,row,meta){
                    return isActive[parseInt(row.is_active)];
                }
            },
            {
                targets: [9],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.deleted_at).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [10],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white restore" title="Restore">Restore</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'exam'},
            { data: 'minutes'},
            { data: 'month'},
            { data: 'year'},
            { data: 'total'},
            { data: 'average'},
            { data: 'is_active'},
            { data: 'users.name'},
            { data: 'deleted_at'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.restore',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'restore/'+(parseInt(dataId));
        Swal.fire({
            title: 'Restore Data',
            text: 'Are You Sure?',
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'POST',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', null, data.responseJSON.message);
                });
            }
        });
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'forceDelete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', null, data.responseJSON.message);
                });
            }
        });
    });

});