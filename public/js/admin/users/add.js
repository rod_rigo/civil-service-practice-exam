'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                loader('info', null, 'Please Wait');
                $('small').empty();
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#username').on('input', function (e) {
        var regex = /^(?=.*[A-Z])(?=.*\d).{8,24}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Username Must Contain At Least (1 Capital Letter, Number, & Special Character(!@#$%^&*()_) Minimum Of 8 Characters');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#email').on('input', function (e) {
        var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Enter Valid Email');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#password').on('input', function (e) {
        var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).{8,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Password Must Contain At Least (1 Capital Letter, Number, & Special Character(!@#$%^&*()_) Minimum Of 8 Characters');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#confirm-password').on('input', function (e) {
        var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).{8,}$/;
        var value = $(this).val();
        var password = $('#password').val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Password (Confirm) Must Contain At Least (1 Capital Letter, Number, & Special Character(!@#$%^&*()_) Minimum Of 8 Characters');
            return true;
        }

        if(value !== password){
            $(this).addClass('is-invalid').next('small').text('Password Do Not Matched');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#contact-number').on('input', function (e) {
        var regex = /^(09|\+639)([0-9]{9})$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Contact Number Must Be Start At 0 & 9 Followed By 9 Digits');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();
    });

    $('#show-password').change(function (e) {
        var checked = $(this).prop('checked');
        var type = (checked)? 'text': 'password';
        $('#password, #confirm-password').attr('type', type);
    });

    $('#admin').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-admin').val(parseInt(Number(checked)));
    });

    $('#examinee').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-examinee').val(parseInt(Number(checked)));
    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(parseInt(Number(checked)));
    });

});