'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'users/edit/'+(id);

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                loader('info', null, 'Please Wait');
                $('small').empty();
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#username').on('input', function (e) {
        var regex = /^(?=.*[A-Z])(?=.*\d).{8,24}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Username Must Contain At Least (1 Capital Letter, Number, & Special Character(!@#$%^&*()_) Minimum Of 8 Characters');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#email').on('input', function (e) {
        var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Enter Valid Email');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#contact-number').on('input', function (e) {
        var regex = /^(09|\+639)([0-9]{9})$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Contact Number Must Be Start At 0 & 9 Followed By 9 Digits');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();
    });

    $('#admin').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-admin').val(parseInt(Number(checked)));
    });

    $('#examinee').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-examinee').val(parseInt(Number(checked)));
    });

    $('#active').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-active').val(parseInt(Number(checked)));
    });

});
$(document).ready(function (e) {

    var baseurl = mainurl+'examinations/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    $('#percircle').percircle({
        percent: parseFloat(0).toFixed(2),
        text: parseFloat(0).toFixed(2),
    });

    function counts() {
        $.ajax({
            url: baseurl+'counts/'+(id),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#total-examinations, #total-released-examinations, #total-not-released-examinations').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-examinations').text(parseInt(data.total));
            $('#total-released-examinations').text(parseInt(data.released));
            $('#total-not-released-examinations').text(parseInt(data.not_released));
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getAverage() {
        $.ajax({
            url: baseurl+'getAverage/'+(id),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#percircle').percircle({
                    percent: parseFloat(0).toFixed(2),
                    text: parseFloat(0).toFixed(2),
                });
            },
        }).done(function (data, status, xhr) {
            if(parseInt(Object.keys(data).length)){
                $('#percircle').percircle({
                    percent: parseFloat(data.total).toFixed(2),
                    text: parseFloat(data.total).toFixed(2),
                });
            }else{
                $('#percircle').percircle({
                    percent: parseFloat(0).toFixed(2),
                    text: parseFloat(0).toFixed(2),
                });
            }
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    var examination_canvas = document.querySelector('#examination-chart').getContext('2d');
    var examination_chart;

    function getAnswersStatistics() {
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAnswersStatistics/'+(id)+'?start_date='+(startDate)+'&end_date='+(endDate),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.reviewer).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getAnswersStatisticsChart() {
        examination_chart =  new Chart(examination_canvas, {
            type: 'radar',
            data: {
                labels: getAnswersStatistics().label,
                datasets: [
                    {
                        label:'Total',
                        data: getAnswersStatistics().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 1
                    },
                    title: {
                        display: false,
                        text: null,
                        font: {
                            size: 16,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    counts();
    getAverage();
    getAnswersStatisticsChart();

});