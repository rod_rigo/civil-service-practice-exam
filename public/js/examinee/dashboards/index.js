'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'dashboards/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    $('#percircle').percircle({
        percent: parseFloat(0).toFixed(2),
        text: parseFloat(0).toFixed(2),
    });

    function counts() {
        $.ajax({
            url: baseurl+'counts',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#total-examinations, #total-released-examinations, #total-not-released-examinations').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-examinations').text(parseInt(data.total));
            $('#total-released-examinations').text(parseInt(data.released));
            $('#total-not-released-examinations').text(parseInt(data.not_released));
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function getAverage() {
        $.ajax({
            url: baseurl+'getAverage',
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#percircle').percircle({
                    percent: parseFloat(0).toFixed(2),
                    text: parseFloat(0).toFixed(2),
                });
            },
        }).done(function (data, status, xhr) {
            if(parseInt(Object.keys(data).length)){
                $('#percircle').percircle({
                    percent: parseFloat(data.total).toFixed(2),
                    text: parseFloat(data.total).toFixed(2),
                });
            }else{
                $('#percircle').percircle({
                    percent: parseFloat(0).toFixed(2),
                    text: parseFloat(0).toFixed(2),
                });
            }
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    var examination_canvas = document.querySelector('#examination-chart').getContext('2d');
    var examination_chart;

    function getAnswersStatistics() {
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getAnswersStatistics?start_date='+(startDate)+'&end_date='+(endDate),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.reviewer).toUpperCase());
            });
        }).fail(function (data, status, xhr) {

        });
        return array;
    }

    function getAnswersStatisticsChart() {
        examination_chart =  new Chart(examination_canvas, {
            type: 'radar',
            data: {
                labels: getAnswersStatistics().label,
                datasets: [
                    {
                        label:'Total',
                        data: getAnswersStatistics().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 1
                    },
                    title: {
                        display: false,
                        text: null,
                        font: {
                            size: 16,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    counts();
    getAverage();
    getAnswersStatisticsChart();

});

