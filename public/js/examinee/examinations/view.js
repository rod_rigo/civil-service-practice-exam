'use strict';
$(document).ready(function (e) {

    $('input').prop('disabled', true);

    $('#percircle').percircle({
        percent: parseFloat(average).toFixed(2),
        text: parseFloat(average).toFixed(2),
    });

    $('#timer-container').mouseenter(function (e) {
        $(this).addClass('opacity');
    }).mouseleave(function (e) {
        $(this).removeClass('opacity');
    }).draggable({
        scroll: true,
        scrollSensitivity: 100,
        cursor: 'move',
        containment: 'body',
        drag: function( event, ui ) {
            if(parseInt(ui.position.top) < parseInt(-300)){
                $('#timer-container').removeAttr('style');
            }
        },
    });

});