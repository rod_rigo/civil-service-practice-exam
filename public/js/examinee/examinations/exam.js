'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'answers/add';
    var timeout = '';

    var interval = setInterval(function (e) {
        var now  = moment();
        var interval = moment.duration(now.diff(date));
        var minutes = parseInt(Math.abs(interval._milliseconds)) / parseInt(1000) / parseInt(60);
        var seconds = parseInt(Math.abs(interval._data.seconds));
        var message = parseInt(minutes)+':'+parseInt(seconds);
        if(date >= now){
            $('#timer-body').html('<i class="fe fe-clock"></i> '+message);
        }else{
            clearInterval(interval);
            $('input').prop('disabled', true);
            $('#timer-body').html('<i class="fe fe-clock"></i> --:--');
            Turbolinks.visit(mainurl+'exams/index', {action:'replace'});
        }
    }, 1000);

    $('#timer-container').mouseenter(function (e) {
        $(this).addClass('opacity');
    }).mouseleave(function (e) {
        $(this).removeClass('opacity');
    }).draggable({
        scroll: true,
        scrollSensitivity: 100,
        cursor: 'move',
        containment: 'body',
        drag: function( event, ui ) {
            if(parseInt(ui.position.top) < parseInt(-300)){
                $('#timer-container').removeAttr('style');
            }
        },
    });

    $('.answer').on('input', function (e) {
        clearTimeout(timeout);
        var value = $(this).val();
        var data = {
            'user_id' : parseInt(user_id),
            'examination_id' : parseInt(examination_id),
            'question_id' : parseInt($(this).attr('data-question-id')),
            'section_id' : parseInt($(this).attr('data-section-id')),
            'answer' : value,
            'points': parseInt(0),
            'is_correct' : parseInt(0),
        };
        timeout = setTimeout(function () {
            answer(data);
        }, 800);
    });

    function answer(data) {
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {

        }).fail(function (data, status, xhr) {

        });
    }

});
$(function (e) {

    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');

    var webcamMgr = new WebCamManager({
        webcamParams: {
            video: {
                mandatory: {
                    maxWidth: 400,
                    maxHeight: 400,
                    minWidth: 400,
                    minHeight: 400
                }
            }
        }, //Set params for web camera
        testVideoMode: false,//true:force use example video for test false:use web camera
        videoTag: video
    });

    var faceDetector = new FaceDetector({
        video: webcamMgr.getVideoTag(),
        flipLeftRight: false,
        flipUpsideDown: false
    });

    webcamMgr.setOnGetUserMediaCallback(function () {
        faceDetector.startDetecting();
    });

    faceDetector.setOnFaceAddedCallback(function (addedFaces, detectedFaces) {

        if(parseInt(addedFaces.length) == parseInt(0)){

        }else{
            $('#modal').modal('hide');
            $('input').prop('disabled', false);
            for (var i = 0; i < addedFaces.length; i++) {

            }
        }

    });

    faceDetector.setOnFaceLostCallback(function (lostFaces, detectedFaces) {

        var ctx = canvas.getContext('2d', { willReadFrequently: true });
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        if(parseInt(lostFaces.length) == parseInt(0)){

        }else{
            for (var i = 0; i < lostFaces.length; i++) {

            }
        }

    });

    faceDetector.setOnFaceUpdatedCallback(function (detectedFaces) {

        var ctx = canvas.getContext('2d', { willReadFrequently: true });
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        ctx.strokeStyle = 'blue';
        ctx.lineWidth = 3;
        ctx.fillStyle = 'red';
        ctx.font = 'italic small-caps bold 20px arial';

        for (var i = 0; i < detectedFaces.length; i++) {

            var face = detectedFaces[i];

            ctx.fillText(face.faceId, face.x * canvas.width, face.y * canvas.height);
            ctx.strokeRect(face.x * canvas.width, face.y * canvas.height + 10, face.width * canvas.width, face.height * canvas.height);

        }
    });

    $('#modal').modal('show');

    webcamMgr.startCamera();

});