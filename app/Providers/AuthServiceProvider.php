<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\Answer;
use App\Models\Document;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Lesson;
use App\Models\Question;
use App\Models\Reviewer;
use App\Models\Section;
use App\Models\User;
use App\Policies\AnswerPolicy;
use App\Policies\DocumentPolicy;
use App\Policies\ExaminationPolicy;
use App\Policies\ExamPolicy;
use App\Policies\LessonPolicy;
use App\Policies\QuestionPolicy;
use App\Policies\ReviewerPolicy;
use App\Policies\SectionPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Reviewer::class => ReviewerPolicy::class,
        Section::class => SectionPolicy::class,
        Question::class => QuestionPolicy::class,
        Lesson::class => LessonPolicy::class,
        Exam::class => ExamPolicy::class,
        Document::class => DocumentPolicy::class,
        Examination::class => ExaminationPolicy::class,
        Answer::class => AnswerPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
