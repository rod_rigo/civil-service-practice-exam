<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviewer extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'reviewers';

    protected $fillable = [
        'user_id',
        'reviewer',
        'content',
        'is_active',
    ];

    protected function setReviewerAttribute($value){
        return $this->attributes['reviewer'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Lessons(){
        return $this->hasMany(Lesson::class,'reviewer_id','id');
    }

    public function Sections(){
        return $this->hasMany(Section::class,'reviewer_id','id');
    }

}
