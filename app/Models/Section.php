<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'sections';

    protected $fillable = [
        'user_id',
        'exam_id',
        'reviewer_id',
        'content',
        'order_position',
        'is_active'
    ];

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Exams(){
        return $this->belongsTo(Exam::class,'exam_id','id');
    }

    public function Reviewers(){
        return $this->belongsTo(Reviewer::class,'reviewer_id','id');
    }

    public function Answers(){
        return $this->hasMany(Answer::class,'section_id','id');
    }

    public function Questions(){
        return $this->hasMany(Question::class,'section_id','id');
    }

}
