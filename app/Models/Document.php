<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'documents';

    protected $fillable = [
        'user_id',
        'document',
        'title',
        'is_active'
    ];

    protected function _setTitle($value){
        return $this->attributes['title'] = ucwords($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

}
