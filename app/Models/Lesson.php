<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'lessons';

    protected $fillable = [
        'user_id',
        'reviewer_id',
        'lesson',
        'content',
        'lesson_file',
        'is_active'
    ];

    protected function setLessonAttribute($value){
        return $this->attributes['lesson'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function Reviewers(){
        return $this->belongsTo(Reviewer::class, 'reviewer_id','id');
    }

}
