<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'exams';

    protected $fillable = [
        'user_id',
        'exam',
        'minutes',
        'month',
        'year',
        'content',
        'total',
        'average',
        'is_active'
    ];

    protected function setExamAttribute($value){
        return $this->attributes['exam'] = strtoupper($value);
    }

    protected function setMinutesAttribute($value){
        return $this->attributes['minutes'] = intval($value);
    }

    protected function setMonthAttribute($value){
        return $this->attributes['month'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Sections(){
        return $this->hasMany(Section::class,'exam_id','id');
    }

    public function Questions(){
        return $this->hasMany(Question::class,'exam_id','id');
    }

    public function Examinations(){
        return $this->hasMany(Examination::class,'exam_id','id');
    }

}
