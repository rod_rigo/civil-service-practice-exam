<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'questions';

    protected $fillable = [
        'user_id',
        'exam_id',
        'section_id',
        'content',
        'answer',
        'points',
        'order_position',
        'is_active'
    ];

    protected function setAnswerAttribute($value){
        return $this->attributes['answer'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Exams(){
        return $this->belongsTo(Exam::class,'exam_id','id');
    }

    public function Sections(){
        return $this->belongsTo(Section::class,'section_id','id');
    }

    public function Answers(){
        return $this->hasMany(Answer::class,'question_id','id');
    }

}
