<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use YoHang88\LetterAvatar\LetterAvatar;

class User extends Authenticatable
{
    use HasFactory, SoftDeletes;

    protected $table = 'users';

    protected static function booted(): void
    {
        static::saved(function (User $user) {

            try{

                $path = public_path('img/user-avatar');
                $folder = File::isDirectory($path);
                if(!$folder){
                    File::makeDirectory($path);
                }

                $filepath = public_path('img/user-avatar/'.(strval($user->attributes['id'])).'.png');

                $avatar = new LetterAvatar(strtoupper($user->attributes['name']), 'circle', 200);

                $avatar->setColor('#339598', '#ffffff');

                $avatar->saveAs($filepath, LetterAvatar::MIME_TYPE_PNG);

            }catch (\Exception $exception){

            }

        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'contact_number',
        'password',
        'is_admin',
        'is_examinee',
        'is_active',
        'token',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'token',
        'remember_token',
    ];

    protected function setNameAttribute($value){
        return $this->attributes['name'] = strtoupper($value);
    }

    protected function setPasswordAttribute($value){
        return $this->attributes['password'] = Hash::make($value);
    }

    public function Sections(){
        return $this->hasMany(Section::class,'user_id', 'id');
    }

    public function Reviewers(){
        return $this->hasMany(Reviewer::class,'user_id', 'id');
    }

    public function Questions(){
        return $this->hasMany(Question::class,'user_id', 'id');
    }

    public function Lessons(){
        return $this->hasMany(Lesson::class,'user_id', 'id');
    }

    public function Examinations(){
        return $this->hasMany(Examination::class,'user_id', 'id');
    }

    public function Exams(){
        return $this->hasMany(Exam::class,'user_id', 'id');
    }

    public function Documents(){
        return $this->hasMany(Document::class,'user_id', 'id');
    }

    public function Answers(){
        return $this->hasMany(Answer::class,'user_id', 'id');
    }

}
