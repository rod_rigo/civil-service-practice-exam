<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Examination extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'examinations';

    protected $fillable = [
        'user_id',
        'exam_id',
        'minutes',
        'month',
        'year',
        'start_date',
        'end_date',
        'total',
        'average',
        'is_released'
    ];

    protected function setMinutesAttribute($value){
        return $this->attributes['minutes'] = intval($value);
    }

    protected function setMonthAttribute($value){
        return $this->attributes['month'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Exams(){
        return $this->belongsTo(Exam::class,'exam_id','id');
    }

    public function Answers(){
        return $this->hasMany(Answer::class,'examination_id','id');
    }

}
