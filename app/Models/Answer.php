<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'answers';

    protected $fillable = [
        'user_id',
        'examination_id',
        'question_id',
        'section_id',
        'answer',
        'points',
        'is_correct',
    ];

    protected function setAnswerAttribute($value){
        return $this->attributes['answer'] = strtoupper($value);
    }

    public function Users(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Examinations(){
        return $this->belongsTo(Examination::class,'examination_id','id');
    }

    public function Questions(){
        return $this->belongsTo(Question::class,'question_id','id');
    }

    public function Sections(){
        return $this->belongsTo(Section::class,'section_id','id');
    }

}
