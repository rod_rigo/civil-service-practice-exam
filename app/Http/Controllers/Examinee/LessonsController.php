<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Lesson;
use App\Models\Reviewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class LessonsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $this->authorize('examinee', Lesson::class);
        $lessons = Lesson::withoutTrashed()
            ->with([
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                },
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['lessons.is_active', '=', intval(1)]
            ])
            ->paginate(intval(30))
            ->withQueryString()
            ->appends($request->query());
        return view('Examinee.Lessons.index', compact('lessons'));
    }

    public function view($id = null){
        $this->authorize('examinee', Lesson::class);
        $lesson = Lesson::withoutTrashed()
            ->where([
                ['lessons.is_active', '=', intval(1)]
            ])
            ->findOrFail($id);
        return view('Examinee.Lessons.view', compact('lesson'));
    }

    public function download($id = null){
        $this->authorize('examinee', Lesson::class);
        $lesson = Lesson::withoutTrashed()
            ->where([
                ['lessons.is_active', '=', intval(1)]
            ])
            ->findOrFail($id);
        $path = public_path($lesson->lesson_file);
        $folder = File::isFile($path);
        if(!$folder){
            abort(404);
        }
        $name = File::basename($path);
        return response()->download($path, $name);
    }

}
