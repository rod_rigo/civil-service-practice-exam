<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class DocumentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $this->authorize('examinee', Document::class);
        $documents = Document::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['documents.is_active', '=', intval(1)]
            ])
            ->paginate(intval(30))
            ->withQueryString()
            ->appends($request->query());

        return view('Examinee.Documents.index', compact('documents'));
    }

    public function download($id = null){
        $this->authorize('examinee', Document::class);
        $document = Document::withoutTrashed()
            ->where([
                ['documents.is_active', '=', intval(1)]
            ])
            ->findOrFail($id);
        $path = public_path($document->document);
        $folder = File::isFile($path);
        if(!$folder){
            abort(404);
        }
        $name = File::basename($path);
        return response()->download($path, $name);
    }

}
