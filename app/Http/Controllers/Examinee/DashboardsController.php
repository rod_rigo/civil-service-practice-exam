<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Reviewer;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Moment\Moment;

class DashboardsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('examinee', User::class);
        return view('Examinee.Dashboards.index');
    }

    public function counts(){
        $this->authorize('examinee', User::class);

        $query = Examination::withoutTrashed()
            ->where([
                ['examinations.user_id', '=', intval(auth()->user()->id)]
            ]);

        $total = $query->get()->count();

        $released = $query->where([
            ['examinations.is_released' , '=', intval(1)]
        ])->get()->count();

        $notReleased = Examination::withoutTrashed()
            ->where([
                ['examinations.user_id', '=', intval(auth()->user()->id)]
            ])
            ->where([
            ['examinations.is_released' , '=', intval(0)]
        ])->get()->count();

        $collection = (new Collection([
           [
               'total' => doubleval($total),
               'released' => doubleval($released),
               'not_released' => doubleval($notReleased)
           ]
        ]))->first();

        return response()->json($collection);

    }

    public function getAnswersStatistics(){
        $this->authorize('examinee', User::class);

        $data = Answer::withoutTrashed()
            ->leftJoin('sections', 'answers.section_id', '=', 'sections.id')
            ->leftJoin('reviewers', 'sections.reviewer_id', '=', 'reviewers.id')
            ->selectRaw(   'answers.user_id as user_id')
            ->selectRaw(   'reviewers.reviewer as reviewer')
            ->selectRaw(   'reviewers.id as reviewer_id')
            ->selectRaw(   'AVG(answers.points) as total')
            ->having(function($query){
                return $query->having('user_id', '=', intval(auth()->user()->id));
            })
            ->groupBy([
                'reviewer_id',
                'user_id'
            ])
            ->get();

        return response()->json($data);
    }

    public function getAverage(){
        $this->authorize('examinee', User::class);

        $data = Examination::withoutTrashed()
            ->where([
                ['examinations.user_id', '=', intval(auth()->user()->id)],
                ['examinations.is_released' , '=', intval(1)]
            ])
            ->selectRaw('AVG(examinations.average) as total')
            ->groupBy([
                'user_id'
            ])
            ->get()
            ->first();

        return response()->json($data);
    }

}
