<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Lesson;
use App\Models\Reviewer;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class ReviewersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $this->authorize('examinee', Reviewer::class);
        $reviewers = Reviewer::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
            ])
            ->where([
                ['reviewers.is_active', '=', intval(1)]
            ])
            ->paginate(intval(30))
            ->withQueryString()
            ->appends($request->query());
        return view('Examinee.Reviewers.index', compact('reviewers'));
    }

    public function view($id = null){
        $this->authorize('examinee', Reviewer::class);
        $reviewer = Reviewer::withoutTrashed()
            ->where([
                ['reviewers.is_active', '=', intval(1)]
            ])
            ->findOrFail($id);
        return view('Examinee.Reviewers.view', compact('reviewer'));
    }

}
