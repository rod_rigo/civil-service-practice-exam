<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Moment\Moment;

class ExaminationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('examinee',Examination::class);
        return view('Examinee.Examinations.index');
    }

    public function getExaminations(Request $request){
        $this->authorize('examinee',Examination::class);
        $startDate = ($request->query('start_date'))? (new Moment(strval($request->query('start_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($request->query('end_date'))? (new Moment(strval($request->query('end_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $records = ($request->query('records'))? intval($request->query('records')): intval(10000);
        $data = Examination::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withoutTrashed()->get();
                }
            ])
            ->where([
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate],
                ['examinations.user_id', '=', intval(auth()->user()->id)],
                ['examinations.is_released', '=', intval(1)],
            ])
            ->orderBy('examinations.created_at', 'DESC')
            ->limit(intval($records))
            ->get();

        return response()->json(['data' => $data]);
    }

    public function today(){
        $this->authorize('examinee',Examination::class);
        return view('Examinee.Examinations.today');
    }

    public function getExaminationsToday(Request $request){
        $this->authorize('examinee',Examination::class);
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withoutTrashed()->get();
                }
            ])
            ->where([
                ['examinations.created_at', '>=', $now],
                ['examinations.user_id', '=', intval(auth()->user()->id)],
                ['examinations.is_released', '=', intval(1)],
            ])
            ->orderBy('examinations.created_at', 'DESC')
            ->get();

        return response()->json(['data' => $data]);
    }

    public function week(){
        $this->authorize('examinee',Examination::class);
        return view('Examinee.Examinations.week');
    }

    public function getExaminationsWeek(Request $request){
        $this->authorize('examinee',Examination::class);
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('week')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withoutTrashed()->get();
                }
            ])
            ->where([
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate],
                ['examinations.user_id', '=', intval(auth()->user()->id)],
                ['examinations.is_released', '=', intval(1)],
            ])
            ->orderBy('examinations.created_at', 'DESC')
            ->get();

        return response()->json(['data' => $data]);
    }

    public function month(){
        $this->authorize('examinee',Examination::class);
        return view('Examinee.Examinations.month');
    }

    public function getExaminationsMonth(Request $request){
        $this->authorize('examinee',Examination::class);
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('month')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withoutTrashed()->get();
                }
            ])
            ->where([
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate],
                ['examinations.user_id', '=', intval(auth()->user()->id)],
                ['examinations.is_released', '=', intval(1)],
            ])
            ->orderBy('examinations.created_at', 'DESC')
            ->get();

        return response()->json(['data' => $data]);
    }

    public function year(){
        $this->authorize('examinee',Examination::class);
        return view('Examinee.Examinations.year');
    }

    public function getExaminationsYear(Request $request){
        $this->authorize('examinee',Examination::class);
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withoutTrashed()->get();
                }
            ])
            ->where([
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate],
                ['examinations.user_id', '=', intval(auth()->user()->id)],
                ['examinations.is_released', '=', intval(1)],
            ])
            ->orderBy('examinations.created_at', 'DESC')
            ->get();

        return response()->json(['data' => $data]);
    }

    public function view($id = null){
        $examination = Examination::withoutTrashed()
            ->where([
                ['examinations.is_released', '=', intval(1)]
            ])
            ->with([
                'Users' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Answers' => function($query){
                    return $query->withoutTrashed()->get();
                }
            ])
            ->findOrFail($id);
        $this->authorize('owned', $examination);
        $answers = (new Collection($examination->toArray()['answers']));
        $exam = Exam::withoutTrashed()
            ->with([
                'Sections' => function($query){
                    return $query->withoutTrashed()
                        ->orderBy('sections.order_position', 'ASC')
                        ->get();
                },
                'Sections.Reviewers' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Sections.Questions' => function($query){
                    return $query->withoutTrashed()
                        ->orderBy('questions.order_position', 'ASC')
                        ->get();
                }
            ])
            ->findOrFail($examination->exam_id);
        $exam = $exam->toArray();
        return view('Examinee.Examinations.view', compact('exam', 'examination', 'answers'));
    }

    public function add(Request $request){
        $this->authorize('examinee', Examination::class);
        $user = User::withoutTrashed()->findOrFail(auth()->user()->id);
        $password = $user->password;
        if($request->isMethod('post')){

            $validator = $request->validate([
                'password' => ['required', function (string $attribute, mixed $value, \Closure $fail) use($password){
                    if (!password_verify($value, $password)) {
                        $fail(ucwords('password do not matched'));
                    }

                    return true;
                }],
                'exam_id' => ['required', 'numeric', Rule::exists('exams','id')],
                'minutes' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {
                    if (intval($value) <= intval(14)) {
                        $fail(ucwords('minutes must be 15 or higher'));
                    }

                    return true;
                }],
            ],[
                'password.required' => ucwords('please fill out this field'),
                'exam_id.required' => ucwords('please fill out this field'),
                'exam_id.numeric' => ucwords('please fill out this field'),
                'exam_id.exists' => ucwords('the exam does not exists'),
                'minutes.required' => ucwords('please fill out this field'),
                'minutes.numeric' => ucwords('please fill out this field'),
            ]);

            $examination = Examination::query()->make([
                'user_id' => intval(auth()->user()->id),
                'exam_id' => intval($request->input('exam_id')),
                'minutes' => intval($request->input('minutes')),
                'month' => strtoupper(date('F')),
                'year' => intval(date('Y')),
                'start_date' => (new Moment(null,'Asia/Manila')),
                'end_date' => (new Moment(null,'Asia/Manila'))->addMinutes(intval($request->input('minutes'))),
                'total' => intval(0),
                'average' => intval(0),
                'is_released' => intval(0)
            ]);

            if($examination->save()){
                $result = ['message' => ucwords('examination has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('examinee.examinations.exam',['id' => intval($examination->id), 'examId' => intval($examination->exam_id)]),
                    'id' => intval($examination->id)];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('examination has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function exam($id = null, $examinationId = null){
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d H:i:s');
        $examination = Examination::withoutTrashed()
            ->with([
                'Answers' => function($query){
                    return $query->withoutTrashed()->get();
                }
            ])
            ->where([
                ['examinations.end_date', '>=', $now]
            ])
            ->findOrFail($id);
        $this->authorize('owned', $examination);
        $answers = (new Collection($examination->toArray()['answers']));
        $exam = Exam::withoutTrashed()
            ->with([
                'Sections' => function($query){
                    return $query->withoutTrashed()
                        ->orderBy('sections.order_position', 'ASC')
                        ->get();
                },
                'Sections.Reviewers' => function($query){
                    return $query->withoutTrashed()->get();
                },
                'Sections.Questions' => function($query){
                    return $query->withoutTrashed()
                        ->orderBy('questions.order_position', 'ASC')
                        ->get();
                }
            ])
            ->findOrFail($examinationId);
        $exam = $exam->toArray();
        return view('Examinee.Examinations.exam', compact('exam', 'examination', 'answers'));
    }

}
