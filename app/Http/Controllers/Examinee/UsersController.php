<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Document;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Lesson;
use App\Models\Question;
use App\Models\Reviewer;
use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function account($id = null, Request $request){
        $user = User::withoutTrashed()->findOrFail(auth()->user()->id);
        $this->authorize('examinee', $user);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'name' => ['required', 'max:255'],
                'username' => ['required', 'max:255', Rule::unique('users','username')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d).{8,24}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Username Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    },
                ],
                'email' => ['required', 'max:255', 'email', Rule::unique('users','email')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $fail(ucwords('Please Enter A Valid Email'));
                        }

                        return true;
                    },
                ],
                'contact_number' => ['required', 'max:255', Rule::unique('users','contact_number')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(09|\+639)([0-9]{9})$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Contact Number Must Be Start At 0 & 9'));
                        }

                        return true;
                    },
                ],
                'is_examinee' => ['required', 'numeric', Rule::in([intval(1)])],
                'is_admin' => ['required', 'numeric', Rule::in([intval(0)])],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'token' => ['required', 'max:255'],
            ],[
                'name.required' => ucwords('please fill out this field'),
                'name.max' => ucwords('this field must not exceed at 255 characters'),
                'username.required' => ucwords('please fill out this field'),
                'username.max' => ucwords('this field must not exceed at 255 characters'),
                'username.unique' => ucwords('this username is already exists'),
                'email.required' => ucwords('please fill out this field'),
                'email.max' => ucwords('this field must not exceed at 255 characters'),
                'email.email' => ucwords('this field is not valid email'),
                'email.unique' => ucwords('this email is already exists'),
                'contact_number.required' => ucwords('please fill out this field'),
                'contact_number.max' => ucwords('this field must not exceed at 255 characters'),
                'contact_number.unique' => ucwords('this contact number is already exists'),
                'is_examinee.required' => ucwords('please fill out examinee value'),
                'is_examinee.numeric' => ucwords('please fill out examinee value'),
                'is_examinee.in' => ucwords('please fill out examinee value'),
                'is_admin.required' => ucwords('please fill out examinee value'),
                'is_admin.numeric' => ucwords('please fill out examinee value'),
                'is_admin.in' => ucwords('please fill out examinee value'),
                'is_active.required' => ucwords('please fill out active value'),
                'is_active.numeric' => ucwords('please fill out active value'),
                'is_active.in' => ucwords('please fill out active value'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user->update($request->all());

            $user->is_admin = intval(0);
            $user->is_examinee = intval(1);
            $user->is_active = intval(1);
            $user->token = uniqid().microtime();

            if($user->save()){
                $result = ['message' => ucwords('the account has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('examinee.users.account',['id' => intval(auth()->user()->id)])];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the account has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Examinee.Users.account',  compact('user'));
    }

    public function changepassword($id = null, Request $request){
        $user = User::withoutTrashed()->findOrFail(auth()->user()->id);
        $password = $user->password;
        $this->authorize('examinee', $user);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'current_password' => ['required', 'max:255',
                    function (string $attribute, mixed $value, \Closure $fail) use ($password){
                        $regex = '/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};\':"\\|,.<>\/?]).{8,}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Password Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        if(!password_verify($value, $password)){
                            $fail(ucwords('current password did not matched'));
                        }

                        return true;
                    }
                ],
                'password' => ['required', 'max:255',
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};\':"\\|,.<>\/?]).{8,}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Password Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    }
                ],
                'confirm_password' => ['required', 'max:255', 'same:password'],
                'token' => ['required', 'max:255'],
            ],[
                'current_password.required' => ucwords('please fill out this field'),
                'current_password.max' => ucwords('this field must not exceed at 255 characters'),
                'password.required' => ucwords('please fill out this field'),
                'password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.required' => ucwords('please fill out this field'),
                'confirm_password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.same' => ucwords('password do not matched'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user->update($request->all());

            $user->is_admin = intval(0);
            $user->is_examinee = intval(1);
            $user->is_active = intval(1);
            $user->token = uniqid().microtime();

            if($user->save()){
                $result = ['message' => ucwords('the password has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('examinee.users.account',['id' => intval(auth()->user()->id)])];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the password has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

}
