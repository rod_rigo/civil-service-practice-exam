<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Question;
use App\Models\Reviewer;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Moment\Moment;

class ExamsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $this->authorize('examinee', Exam::class);
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d H:i:s');
        $exams = Exam::withoutTrashed()
            ->select('exams.*')
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections' => function($query){
                    return $query->withTrashed()
                        ->where([
                            ['sections.is_active', '=', intval(1)]
                        ])->get();
                },
                'Sections.Reviewers' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections.Questions' => function($query){
                    return $query->withTrashed()
                        ->where([
                            ['questions.is_active', '=', intval(1)]
                        ])->get();
                },
                'Questions' => function($query){
                    return $query->withoutTrashed()
                        ->where([
                            ['questions.is_active', '=', intval(1)]
                        ])->get();
                },
            ])
            ->leftJoin('examinations', function($query) use($now){
                return $query->on('exams.id', '=', 'examinations.exam_id')
                    ->where('examinations.user_id', '=', intval(auth()->user()->id))
                    ->where('examinations.start_date', '<=', $now)
                    ->where('examinations.end_date', '>=', $now)
                    ->where('examinations.is_released', '=', intval(0));
            })
            ->selectRaw('examinations.id as examination_id')
            ->where([
                ['exams.is_active', '=', intval(1)]
            ])
            ->orderBy('exams.created_at','ASC')
            ->paginate(intval(100))
            ->withQueryString()
            ->appends($request->query());
        return view('Examinee.Exams.index', compact('exams'));
    }

    public function getExam($id = null){
        $this->authorize('examinee', Exam::class);
        $exam = Exam::withoutTrashed()->findOrFail($id);
        return response()->json($exam);
    }
}
