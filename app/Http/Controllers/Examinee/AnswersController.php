<?php

namespace App\Http\Controllers\Examinee;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Moment\Moment;

class AnswersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Request $request){
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users', 'id')],
                'examination_id' => ['required', 'numeric', Rule::exists('examinations', 'id'),
                    function (string $attribute, mixed $value, \Closure $fail) {

                        $now = (new Moment(null,'Asia/Manila'));
                        $examination = Examination::withoutTrashed()
                            ->findOrFail($value);
                        $endDate =(new Moment($examination->end_date,'Asia/Manila'));

                        if ($now > $endDate) {
                            $fail(ucwords('the examination is already finished'));
                        }

                        return true;
                    },
                ],
                'question_id' => ['required', 'numeric', Rule::exists('questions', 'id')],
                'section_id' => ['required', 'numeric', Rule::exists('sections', 'id')],
                'answer' => ['required', 'max:255'],
                'points' => ['required', 'numeric', Rule::in([intval(0)])],
                'is_correct' => ['required', 'numeric', Rule::in([intval(0)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'examination_id.required' => ucwords('please fill out this field'),
                'examination_id.numeric' => ucwords('please fill out this field'),
                'examination_id.exists' => ucwords('the examination does not exists'),
                'question_id.required' => ucwords('please fill out this field'),
                'question_id.numeric' => ucwords('please fill out this field'),
                'question_id.exists' => ucwords('the question does not exists'),
                'section_id.required' => ucwords('please fill out this field'),
                'section_id.numeric' => ucwords('please fill out this field'),
                'section_id.exists' => ucwords('the section does not exists'),
                'answer.required' => ucwords('please fill out this field'),
                'answer.max' => ucwords('this field must not exceed at 255 characters'),
                'points.required' => ucwords('please fill out this field'),
                'points.numeric' => ucwords('please fill out this field'),
                'points.in' => ucwords('please fill out this field'),
                'is_correct.required' => ucwords('please fill out this field'),
                'is_correct.numeric' => ucwords('please fill out this field'),
                'is_correct.in' => ucwords('please fill out this field'),
            ]);

            DB::beginTransaction();

           try{
               $answer = Answer::withoutTrashed()
                   ->where([
                       ['answers.user_id', '=', intval(auth()->user()->id)],
                       ['answers.examination_id', '=', intval($request->input('examination_id'))],
                       ['answers.section_id', '=', intval($request->input('section_id'))],
                       ['answers.question_id', '=', intval($request->input('question_id'))],
                   ])
                   ->get()
                   ->first();

               $question = Question::withoutTrashed()->findOrFail($request->input('question_id'));

               if(empty($answer)){
                   $answer = Answer::query()->make($request->all());
                   $examination = Examination::withoutTrashed()->findOrFail($answer->examination_id);
                   $exam = Exam::withoutTrashed()->findOrFail($examination->exam_id);

                   $sum = Answer::withoutTrashed()
                        ->where([
                            ['answers.examination_id', '=', intval($examination->id)]
                        ])
                       ->sum('answers.points');

                   $answer->user_id = intval(auth()->user()->id);

                   $points = (strtoupper(preg_replace('/\s+/', '_', $question->answer)) == strtoupper(preg_replace('/\s+/', '_', $request->input('answer'))))? intval($question->points): intval(0);
                   $answer->points = doubleval($points);

                   $isCorrect = (strtoupper(preg_replace('/\s+/', '_', $question->answer)) == strtoupper(preg_replace('/\s+/', '_', $request->input('answer'))))? intval(1): intval(0);
                   $answer->is_correct = intval($isCorrect);

                   $total = doubleval($sum) + doubleval($points);
                   $average = (doubleval($total) / doubleval($exam->total)) * doubleval(100);

                   $examination->total = doubleval($total);
                   $examination->average = doubleval($average);

                   if($answer->save() && $examination->save()){
                       DB::commit();
                       $result = ['message' => ucwords('the answer has been saved'), 'result' => ucwords('success')];
                       return response()->json($result,200);
                   }else{
                       $result = ['message' => ucwords('the answer has not been saved'), 'result' => ucwords('error')];
                       return response()->json($result,422);
                   }

               }else{
                   $answer = Answer::withoutTrashed()->findOrFail($answer->id);
                   $this->authorize('owned', $answer);

                   $examination = Examination::withoutTrashed()->findOrFail($answer->examination_id);
                   $exam = Exam::withoutTrashed()->findOrFail($examination->exam_id);

                   $sum = Answer::withoutTrashed()
                       ->where([
                           ['answers.examination_id', '=', intval($examination->id)],
                           ['answers.id', '!=', intval($answer->id)]
                       ])
                       ->sum('answers.points');

                   $answer->update($request->all());

                   $answer->user_id = intval(auth()->user()->id);

                   $points = (strtoupper(preg_replace('/\s+/', '_', $question->answer)) == strtoupper(preg_replace('/\s+/', '_', $request->input('answer'))))? intval($question->points): intval(0);
                   $answer->points = doubleval($points);

                   $isCorrect = (strtoupper(preg_replace('/\s+/', '_', $question->answer)) == strtoupper(preg_replace('/\s+/', '_', $request->input('answer'))))? intval(1): intval(0);
                   $answer->is_correct = intval($isCorrect);

                   $total = doubleval($sum) + doubleval($points);
                   $average = (doubleval($total) / doubleval($exam->total)) * doubleval(100);

                   $examination->total = doubleval($total);
                   $examination->average = doubleval($average);

                   if($answer->save() && $examination->save()){
                       DB::commit();
                       $result = ['message' => ucwords('the answer has been saved'), 'result' => ucwords('success')];
                       return response()->json($result,200);
                   }else{
                       $result = ['message' => ucwords('the answer has not been saved'), 'result' => ucwords('error')];
                       return response()->json($result,422);
                   }

               }
           }catch (\Exception $exception){
               DB::rollBack();
               $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
               return response()->json($result,422);
           }

        }
    }

}
