<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lesson;
use App\Models\Reviewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class LessonsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', Lesson::class);
        return view('Admin.Lessons.index');
    }

    public function getLessons(){
        $this->authorize('admin', Lesson::class);
        $data = Lesson::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        $this->authorize('admin', Lesson::class);
        return view('Admin.Lessons.bin');
    }

    public function getLessonsDeleted(){
        $this->authorize('admin', Lesson::class);
        $data = Lesson::onlyTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        $this->authorize('admin', Lesson::class);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'reviewer_id' => ['required', 'numeric', Rule::exists('reviewers','id')],
                'lesson' => ['required', 'max:255'],
                'content' => ['required', 'max:4294967295'],
                'file' => ['required', function (string $attribute, mixed $value, \Closure $fail) {

                        $mimes = [strtolower('application/pdf')];

                        if(!in_array(strtolower($value->getClientMimeType()),$mimes)){
                            $fail(ucwords('only PDF file is allowed'));
                        }

                        return true;
                    },
                ],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'reviewer_id.required' => ucwords('please fill out this field'),
                'reviewer_id.numeric' => ucwords('please fill out this field'),
                'reviewer_id.exists' => ucwords('the reviewer does not exists'),
                'lesson.required' => ucwords('please fill out this field'),
                'lesson.max' => ucwords('this field must not exceed at 255 characters'),
                'content.required' => ucwords('please fill out this field'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'file.required' => ucwords('please fill out this field'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $lesson = Lesson::query()->make($request->all());

            $lesson->user_id = intval(auth()->user()->id);

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('lesson');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $filepath = public_path('lesson/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $lesson->content = 'lesson/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($request->hasFile('file')){

                $file = $request->file('file');

                $filename = uniqid().$file->getClientOriginalName();

                try{

                    $path = public_path('lesson-file');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $filepath = public_path('lesson-file');

                    if($file->move($filepath, $filename)){
                        $lesson->lesson_file = 'lesson-file/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($lesson->save()){
                $result = ['message' => ucwords('the lesson has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.lessons.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the lesson has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        $reviewers = Reviewer::withoutTrashed()
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->orderBy('reviewers.reviewer','ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->reviewer), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return view('Admin.Lessons.add', compact('reviewers'));
    }

    public function edit($id = null, Request $request){
        $this->authorize('admin', Lesson::class);
        $lesson = Lesson::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'reviewer_id' => ['required', 'numeric', Rule::exists('reviewers','id')],
                'lesson' => ['required', 'max:255'],
                'content' => ['required', 'max:4294967295'],
                'file' => ['nullable', function (string $attribute, mixed $value, \Closure $fail) {

                        $mimes = [strtolower('application/pdf')];

                        if(!in_array(strtolower($value->getClientMimeType()),$mimes)){
                            $fail(ucwords('only PDF file is allowed'));
                        }

                        return true;
                    },
                ],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'reviewer_id.required' => ucwords('please fill out this field'),
                'reviewer_id.numeric' => ucwords('please fill out this field'),
                'reviewer_id.exists' => ucwords('the reviewer does not exists'),
                'lesson.required' => ucwords('please fill out this field'),
                'lesson.max' => ucwords('this field must not exceed at 255 characters'),
                'content.required' => ucwords('please fill out this field'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $content = $lesson->content;
            $lesson->update($request->all());

            $lesson->user_id = intval(auth()->user()->id);

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('lesson');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $path = public_path($content);
                    $folder = File::isFile($path);
                    if($folder){
                        File::delete($path);
                    }

                    $filepath = public_path('lesson/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $lesson->content = 'lesson/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($request->hasFile('file')){

                $file = $request->file('file');

                $filename = uniqid().$file->getClientOriginalName();

                try{

                    $path = public_path('lesson-file');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $path = public_path($lesson->lesson_file);
                    $folder = File::isFile($path);
                    if($folder){
                        File::delete($path);
                    }

                    $filepath = public_path('lesson-file');

                    if($file->move($filepath, $filename)){
                        $lesson->lesson_file = 'lesson-file/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($lesson->save()){
                $result = ['message' => ucwords('the lesson has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.lessons.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the lesson has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

    }

    public function view($id = null){
        $this->authorize('admin', Lesson::class);
        $lesson = Lesson::withoutTrashed()->findOrFail($id);
        $reviewers = Reviewer::withoutTrashed()
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->orderBy('reviewers.reviewer','ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->reviewer), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return view('Admin.Lessons.view', compact('lesson', 'reviewers'));
    }

    public function download($id = null){
        $this->authorize('admin', Lesson::class);
        $lesson = Lesson::withoutTrashed()->findOrFail($id);
        $path = public_path($lesson->lesson_file);
        $folder = File::isFile($path);
        if(!$folder){
            abort(404);
        }
        $name = File::basename($path);
        return response()->download($path, $name);
    }

    public function delete($id = null){
        $this->authorize('admin', Lesson::class);
        $lesson = Lesson::withoutTrashed()->findOrFail($id);
        if($lesson->delete()){
            $result = ['message' => ucwords('the lesson has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the lesson has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $this->authorize('admin', Lesson::class);
        $lesson = Lesson::onlyTrashed()->findOrFail($id);
        if($lesson->restore()){
            $result = ['message' => ucwords('the lesson has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the lesson has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $this->authorize('admin', Lesson::class);
        $lesson = Lesson::onlyTrashed()->findOrFail($id);

        try{

            $path = public_path($lesson->content);
            $folder = File::isFile($path);
            if($folder){
                File::delete($path);
            }

            $path = public_path($lesson->lesson_file);
            $folder = File::isFile($path);
            if($folder){
                File::delete($path);
            }

        }catch (\Exception $exception){
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($lesson->forceDelete()){
            $result = ['message' => ucwords('the lesson has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the lesson has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
