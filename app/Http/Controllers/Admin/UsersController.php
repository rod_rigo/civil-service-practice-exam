<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Document;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Lesson;
use App\Models\Question;
use App\Models\Reviewer;
use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', User::class);
        return view('Admin.Users.index');
    }

    public function getUsers(){
        $this->authorize('admin', User::class);
        $data = User::withoutTrashed()
            ->where([
                ['users.id', '!=', intval(auth()->user()->id)]
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        $this->authorize('admin', User::class);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'name' => ['required', 'max:255'],
                'username' => ['required', 'max:255', Rule::unique('users','username'),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d).{8,24}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Username Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    },
                ],
                'email' => ['required', 'max:255', 'email', Rule::unique('users','email'),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $fail(ucwords('Please Enter A Valid Email'));
                        }

                        return true;
                    },
                ],
                'contact_number' => ['required', 'max:255', Rule::unique('users','contact_number'),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(09|\+639)([0-9]{9})$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Contact Number Must Be Start At 0 & 9'));
                        }

                        return true;
                    },
                ],
                'password' => ['required', 'max:255',
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};\':"\\|,.<>\/?]).{8,}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Password Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    }
                ],
                'confirm_password' => ['required', 'max:255', 'same:password'],
                'is_admin' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'is_examinee' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'token' => ['required', 'max:255'],
            ],[
                'name.required' => ucwords('please fill out this field'),
                'name.max' => ucwords('this field must not exceed at 255 characters'),
                'username.required' => ucwords('please fill out this field'),
                'username.max' => ucwords('this field must not exceed at 255 characters'),
                'username.unique' => ucwords('this username is already exists'),
                'email.required' => ucwords('please fill out this field'),
                'email.max' => ucwords('this field must not exceed at 255 characters'),
                'email.email' => ucwords('this field is not valid email'),
                'email.unique' => ucwords('this email is already exists'),
                'contact_number.required' => ucwords('please fill out this field'),
                'contact_number.max' => ucwords('this field must not exceed at 255 characters'),
                'contact_number.unique' => ucwords('this contact number is already exists'),
                'password.required' => ucwords('please fill out this field'),
                'password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.required' => ucwords('please fill out this field'),
                'confirm_password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.same' => ucwords('password do not matched'),
                'is_admin.required' => ucwords('please fill out admin value'),
                'is_admin.numeric' => ucwords('please fill out admin value'),
                'is_admin.in' => ucwords('please fill out admin value'),
                'is_examinee.required' => ucwords('please fill out examinee value'),
                'is_examinee.numeric' => ucwords('please fill out examinee value'),
                'is_examinee.in' => ucwords('please fill out examinee value'),
                'is_active.required' => ucwords('please fill out active value'),
                'is_active.numeric' => ucwords('please fill out active value'),
                'is_active.in' => ucwords('please fill out active value'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user = User::query()->make($request->all());

            $user->token = uniqid().microtime();

            if($user->save()){
                $result = ['message' => ucwords('the user has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.users.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the user has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Admin.Users.add');
    }

    public function edit($id = null, Request $request){
        $this->authorize('admin', User::class);
        $user = User::withoutTrashed()->findOrFail($id);

        if($request->isMethod('post')){

            $validator = $request->validate([
                'name' => ['required', 'max:255'],
                'username' => ['required', 'max:255', Rule::unique('users','username')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d).{8,24}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Username Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    },
                ],
                'email' => ['required', 'max:255', 'email', Rule::unique('users','email')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $fail(ucwords('Please Enter A Valid Email'));
                        }

                        return true;
                    },
                ],
                'contact_number' => ['required', 'max:255', Rule::unique('users','contact_number')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(09|\+639)([0-9]{9})$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Contact Number Must Be Start At 0 & 9'));
                        }

                        return true;
                    },
                ],
                'is_admin' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'is_examinee' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'token' => ['required', 'max:255'],
            ],[
                'name.required' => ucwords('please fill out this field'),
                'name.max' => ucwords('this field must not exceed at 255 characters'),
                'username.required' => ucwords('please fill out this field'),
                'username.max' => ucwords('this field must not exceed at 255 characters'),
                'username.unique' => ucwords('this username is already exists'),
                'email.required' => ucwords('please fill out this field'),
                'email.max' => ucwords('this field must not exceed at 255 characters'),
                'email.email' => ucwords('this field is not valid email'),
                'email.unique' => ucwords('this email is already exists'),
                'contact_number.required' => ucwords('please fill out this field'),
                'contact_number.max' => ucwords('this field must not exceed at 255 characters'),
                'contact_number.unique' => ucwords('this contact number is already exists'),
                'is_admin.required' => ucwords('please fill out admin value'),
                'is_admin.numeric' => ucwords('please fill out admin value'),
                'is_admin.in' => ucwords('please fill out admin value'),
                'is_examinee.required' => ucwords('please fill out examinee value'),
                'is_examinee.numeric' => ucwords('please fill out examinee value'),
                'is_examinee.in' => ucwords('please fill out examinee value'),
                'is_active.required' => ucwords('please fill out active value'),
                'is_active.numeric' => ucwords('please fill out active value'),
                'is_active.in' => ucwords('please fill out active value'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user->update($request->all());

            $user->token = uniqid().microtime();

            if($user->save()){
                $result = ['message' => ucwords('the user has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.users.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the user has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function view($id = null){
        $this->authorize('admin', User::class);
        $user = User::withoutTrashed()->findOrFail($id);
        return view('Admin.Users.view', compact('user'));
    }

    public function account($id = null, Request $request){
        $user = User::withoutTrashed()->findOrFail(auth()->user()->id);
        $this->authorize('admin', $user);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'name' => ['required', 'max:255'],
                'username' => ['required', 'max:255', Rule::unique('users','username')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d).{8,24}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Username Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    },
                ],
                'email' => ['required', 'max:255', 'email', Rule::unique('users','email')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $fail(ucwords('Please Enter A Valid Email'));
                        }

                        return true;
                    },
                ],
                'contact_number' => ['required', 'max:255', Rule::unique('users','contact_number')->ignore(intval($user->id)),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(09|\+639)([0-9]{9})$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Contact Number Must Be Start At 0 & 9'));
                        }

                        return true;
                    },
                ],
                'is_admin' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'is_examinee' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'token' => ['required', 'max:255'],
            ],[
                'name.required' => ucwords('please fill out this field'),
                'name.max' => ucwords('this field must not exceed at 255 characters'),
                'username.required' => ucwords('please fill out this field'),
                'username.max' => ucwords('this field must not exceed at 255 characters'),
                'username.unique' => ucwords('this username is already exists'),
                'email.required' => ucwords('please fill out this field'),
                'email.max' => ucwords('this field must not exceed at 255 characters'),
                'email.email' => ucwords('this field is not valid email'),
                'email.unique' => ucwords('this email is already exists'),
                'contact_number.required' => ucwords('please fill out this field'),
                'contact_number.max' => ucwords('this field must not exceed at 255 characters'),
                'contact_number.unique' => ucwords('this contact number is already exists'),
                'is_admin.required' => ucwords('please fill out admin value'),
                'is_admin.numeric' => ucwords('please fill out admin value'),
                'is_admin.in' => ucwords('please fill out admin value'),
                'is_examinee.required' => ucwords('please fill out examinee value'),
                'is_examinee.numeric' => ucwords('please fill out examinee value'),
                'is_examinee.in' => ucwords('please fill out examinee value'),
                'is_active.required' => ucwords('please fill out active value'),
                'is_active.numeric' => ucwords('please fill out active value'),
                'is_active.in' => ucwords('please fill out active value'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user->update($request->all());

            $user->token = uniqid().microtime();

            if($user->save()){
                $result = ['message' => ucwords('the account has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.users.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the account has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Admin.Users.account',  compact('user'));
    }

    public function changepassword($id = null, Request $request){
        $user = User::withoutTrashed()->findOrFail(auth()->user()->id);
        $password = $user->password;
        $this->authorize('admin', $user);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'current_password' => ['required', 'max:255',
                    function (string $attribute, mixed $value, \Closure $fail) use ($password){
                        $regex = '/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};\':"\\|,.<>\/?]).{8,}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Password Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        if(!password_verify($value, $password)){
                            $fail(ucwords('current password did not matched'));
                        }

                        return true;
                    }
                ],
                'password' => ['required', 'max:255',
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};\':"\\|,.<>\/?]).{8,}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Password Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    }
                ],
                'confirm_password' => ['required', 'max:255', 'same:password'],
                'token' => ['required', 'max:255'],
            ],[
                'current_password.required' => ucwords('please fill out this field'),
                'current_password.max' => ucwords('this field must not exceed at 255 characters'),
                'password.required' => ucwords('please fill out this field'),
                'password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.required' => ucwords('please fill out this field'),
                'confirm_password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.same' => ucwords('password do not matched'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user->update($request->all());

            $user->token = uniqid().microtime();

            if($user->save()){
                $result = ['message' => ucwords('the password has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.users.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the password has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function delete($id = null){
        $this->authorize('admin', User::class);
        $user = User::withoutTrashed()->findOrFail($id);

        $section = Section::withTrashed()->get()->last();
        if(!empty($section)){
            $result = ['message' => ucwords('the user has been constrained to sections'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $reviewer = Reviewer::withTrashed()->get()->last();
        if(!empty($reviewer)){
            $result = ['message' => ucwords('the user has been constrained to reviewers'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $question = Question::withTrashed()->get()->last();
        if(!empty($question)){
            $result = ['message' => ucwords('the user has been constrained to questions'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $lesson = Lesson::withTrashed()->get()->last();
        if(!empty($lesson)){
            $result = ['message' => ucwords('the user has been constrained to lessons'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $examinations = Examination::withTrashed()->get()->last();
        if(!empty($examinations)){
            $result = ['message' => ucwords('the user has been constrained to examinations'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $exam = Exam::withTrashed()->get()->last();
        if(!empty($exam)){
            $result = ['message' => ucwords('the user has been constrained to exams'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $document = Document::withTrashed()->get()->last();
        if(!empty($document)){
            $result = ['message' => ucwords('the user has been constrained to documents'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $answer = Answer::withTrashed()->get()->last();
        if(!empty($answer)){
            $result = ['message' => ucwords('the user has been constrained to answers'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($user->delete()){
            $result = ['message' => ucwords('the user has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the user has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
