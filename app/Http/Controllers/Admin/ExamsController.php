<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Question;
use App\Models\Reviewer;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Moment\Moment;

class ExamsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', Exam::class);
        return view('Admin.Exams.index');
    }

    public function getExams(Request $request){
        $this->authorize('admin', Exam::class);
        $startDate = ($request->query('start_date'))? (new Moment(strval($request->query('start_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($request->query('end_date'))? (new Moment(strval($request->query('end_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $records = ($request->query('records'))? intval($request->query('records')): intval(10000);
        $data = Exam::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['exams.created_at', '>=', $startDate],
                ['exams.created_at', '<=', $endDate],
            ])
            ->orderBy('exams.created_at','ASC')
            ->limit(intval($records))
            ->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        $this->authorize('admin', Exam::class);
        return view('Admin.Exams.bin');
    }

    public function getExamsDeleted(){
        $this->authorize('admin', Exam::class);
        $data = Exam::onlyTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function getExam($id = null){
        $this->authorize('admin', Exam::class);
        $exam = Exam::withoutTrashed()->findOrFail($id);
        return response()->json($exam);
    }

    public function practice(Request $request){
        $this->authorize('admin', Exam::class);
        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d H:i:s');
        $exams = Exam::withoutTrashed()
            ->select('exams.*')
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections' => function($query){
                    return $query->withTrashed()
                        ->where([
                            ['sections.is_active', '=', intval(1)]
                        ])->get();
                },
                'Sections.Reviewers' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections.Questions' => function($query){
                    return $query->withTrashed()
                        ->where([
                            ['questions.is_active', '=', intval(1)]
                        ])->get();
                },
                'Questions' => function($query){
                    return $query->withoutTrashed()
                        ->where([
                            ['questions.is_active', '=', intval(1)]
                        ])->get();
                },
            ])
            ->leftJoin('examinations', function($query) use($now){
                return $query->on('exams.id', '=', 'examinations.exam_id')
                    ->where('examinations.user_id', '=', intval(auth()->user()->id))
                    ->where('examinations.start_date', '<=', $now)
                    ->where('examinations.end_date', '>=', $now)
                    ->where('examinations.is_released', '=', intval(0));
            })
            ->selectRaw('examinations.id as examination_id')
            ->where([
                ['exams.is_active', '=', intval(1)]
            ])
            ->orderBy('exams.created_at','ASC')
            ->paginate(intval(100))
            ->withQueryString()
            ->appends($request->query());
        return view('Admin.Exams.practice', compact('exams'));
    }

    public function add(Request $request){
        $this->authorize('admin', Exam::class);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'exam' => ['required', 'max:255'],
                'minutes' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {
                    if (intval($value) <= intval(14)) {
                        $fail(ucwords('minutes must be 15 or higher'));
                    }

                    return true;
                }],
                'month' => ['required', 'max:255', function (string $attribute, mixed $value, \Closure $fail) {
                        $months = [
                            strtoupper('January'),
                            strtoupper('February'),
                            strtoupper('March'),
                            strtoupper('April'),
                            strtoupper('May'),
                            strtoupper('June'),
                            strtoupper('July'),
                            strtoupper('August'),
                            strtoupper('September'),
                            strtoupper('October'),
                            strtoupper('November'),
                            strtoupper('December'),
                        ];
                        if (!in_array(strtoupper($value), $months)) {
                            $fail(ucwords('invalid month value'));
                        }

                        return true;
                    },
                ],
                'year' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {
                    $year = (new Moment(null,'Asia/Manila'))->addYears(20)->format('Y');
                    if (intval($value) > intval($year)) {
                        $fail(ucwords('invalid year value'));
                    }

                    return true;
                }],
                'content' => ['required', 'max:4294967295'],
                'total' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {

                        if(intval($value) < intval(1)){
                            $fail(ucwords('this field must be higher than 1'));
                        }

                        return true;
                    }
                ],
                'average' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {

                        if(intval($value) < intval(1)){
                            $fail(ucwords('this field must be higher than 1'));
                        }

                        if(intval($value) > intval(100)){
                            $fail(ucwords('this field must be not higher than 100'));
                        }

                        return true;
                    }
                ],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'exam.required' => ucwords('please fill out this field'),
                'exam.max' => ucwords('this field must not exceed at 255 characters'),
                'minutes.required' => ucwords('please fill out this field'),
                'minutes.numeric' => ucwords('this field must be numeric'),
                'month.required' => ucwords('please fill out this field'),
                'month.max' => ucwords('this field must not exceed at 255 characters'),
                'year.required' => ucwords('please fill out this field'),
                'year.numeric' => ucwords('this field must be numeric'),
                'content.required' => ucwords('please fill out this field'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'total.required' => ucwords('please fill out this field'),
                'total.numeric' => ucwords('please fill out this field'),
                'average.required' => ucwords('please fill out this field'),
                'average.numeric' => ucwords('please fill out this field'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $exam = Exam::query()->make($request->all());

            $exam->user_id = intval(auth()->user()->id);

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('exam');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $filepath = public_path('exam/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $exam->content = 'exam/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($exam->save()){
                $result = ['message' => ucwords('the exam has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.exams.view',['id' => intval($exam->id)]), 'return' => route('admin.exams.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the exam has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Admin.Exams.add');
    }

    public function edit($id = null, Request $request){
        $this->authorize('admin', Exam::class);
        $exam = Exam::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'exam' => ['required', 'max:255'],
                'minutes' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {
                    if (intval($value) <= intval(14)) {
                        $fail(ucwords('minutes must be 15 or higher'));
                    }

                    return true;
                }],
                'month' => ['required', 'max:255', function (string $attribute, mixed $value, \Closure $fail) {
                    $months = [
                        strtoupper('January'),
                        strtoupper('February'),
                        strtoupper('March'),
                        strtoupper('April'),
                        strtoupper('May'),
                        strtoupper('June'),
                        strtoupper('July'),
                        strtoupper('August'),
                        strtoupper('September'),
                        strtoupper('October'),
                        strtoupper('November'),
                        strtoupper('December'),
                    ];
                    if (!in_array(strtoupper($value), $months)) {
                        $fail(ucwords('invalid month value'));
                    }

                    return true;
                },
                ],
                'year' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {
                    $year = (new Moment(null,'Asia/Manila'))->addYears(20)->format('Y');
                    if (intval($value) > intval($year)) {
                        $fail(ucwords('invalid year value'));
                    }

                    return true;
                }],
                'content' => ['required', 'max:4294967295'],
                'total' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {

                    if(intval($value) < intval(1)){
                        $fail(ucwords('this field must be higher than 1'));
                    }

                    return true;
                }
                ],
                'average' => ['required', 'numeric', function (string $attribute, mixed $value, \Closure $fail) {

                    if(intval($value) < intval(1)){
                        $fail(ucwords('this field must be higher than 1'));
                    }

                    if(intval($value) > intval(100)){
                        $fail(ucwords('this field must be not higher than 100'));
                    }

                    return true;
                }
                ],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'exam.required' => ucwords('please fill out this field'),
                'exam.max' => ucwords('this field must not exceed at 255 characters'),
                'minutes.required' => ucwords('please fill out this field'),
                'minutes.numeric' => ucwords('this field must be numeric'),
                'month.required' => ucwords('please fill out this field'),
                'month.max' => ucwords('this field must not exceed at 255 characters'),
                'year.required' => ucwords('please fill out this field'),
                'year.numeric' => ucwords('this field must be numeric'),
                'content.required' => ucwords('please fill out this field'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'total.required' => ucwords('please fill out this field'),
                'total.numeric' => ucwords('please fill out this field'),
                'average.required' => ucwords('please fill out this field'),
                'average.numeric' => ucwords('please fill out this field'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $content = $exam->content;
            $exam->update($request->all());

            $exam->user_id = intval(auth()->user()->id);

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('exam');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $path = public_path($content);
                    $folder = File::isFile($path);
                    if($folder){
                        File::delete($path);
                    }

                    $filepath = public_path('exam/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $exam->content = 'exam/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($exam->save()){
                $result = ['message' => ucwords('the exam has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.exams.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the exam has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function view($id = null){
        $this->authorize('admin', Exam::class);
        $exam = Exam::withoutTrashed()
            ->with([
                'Sections' => function($query){
                    return $query->withoutTrashed()
                        ->orderBy('sections.order_position', 'ASC')
                        ->get();
                },
                'Sections.Questions' => function($query){
                    return $query->withoutTrashed()
                        ->orderBy('questions.order_position', 'ASC')
                        ->get();
                },
                'Sections.Reviewers' => function($query){
                    return $query->withoutTrashed()
                        ->get();
                },
            ])
            ->findOrFail($id);
        $sections = $exam->toArray()['sections'];
        $reviewers = Reviewer::withoutTrashed()
            ->where([
                ['is_active', '=', intval(1)]
            ])
            ->orderBy('reviewers.reviewer','ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->reviewer), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return view('Admin.Exams.view', compact('exam', 'sections', 'reviewers'));
    }

    public function delete($id = null){
        $this->authorize('admin', Exam::class);
        $exam = Exam::withoutTrashed()->findOrFail($id);

        $section = Section::withTrashed()
            ->where([
                ['exam_id', '=', intval($exam->id)]
            ])
            ->get()
            ->last();
        if(!empty($section)){
            $result = ['message' => ucwords('the exam has been constrained to sections'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $question = Question::withTrashed()
            ->where([
                ['exam_id', '=', intval($exam->id)]
            ])
            ->get()
            ->last();
        if(!empty($question)){
            $result = ['message' => ucwords('the exam has been constrained to questions'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $examination = Examination::withTrashed()
            ->where([
                ['exam_id', '=', intval($exam->id)]
            ])
            ->get()
            ->last();
        if(!empty($examination)){
            $result = ['message' => ucwords('the exam has been constrained to examinations'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($exam->delete()){
            $result = ['message' => ucwords('the exam has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the exam has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $this->authorize('admin', Exam::class);
        $exam = Exam::onlyTrashed()->findOrFail($id);

        if($exam->restore()){
            $result = ['message' => ucwords('the exam has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the exam has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $this->authorize('admin', Exam::class);
        $exam = Exam::withoutTrashed()->findOrFail($id);

        $section = Section::withTrashed()
            ->where([
                ['exam_id', '=', intval($exam->id)]
            ])
            ->get()
            ->last();
        if(!empty($section)){
            $result = ['message' => ucwords('the exam has been constrained to sections'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $question = Question::withTrashed()
            ->where([
                ['exam_id', '=', intval($exam->id)]
            ])
            ->get()
            ->last();
        if(!empty($question)){
            $result = ['message' => ucwords('the exam has been constrained to questions'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $examination = Examination::withTrashed()
            ->where([
                ['exam_id', '=', intval($exam->id)]
            ])
            ->get()
            ->last();
        if(!empty($examination)){
            $result = ['message' => ucwords('the exam has been constrained to examinations'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        try{

            $path = public_path($exam->content);
            $folder = File::isFile($path);
            if($folder){
                File::delete($path);
            }

        }catch (\Exception $exception){
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
            return response()->json($result,422);
        }


        if($exam->forceDelete()){
            $result = ['message' => ucwords('the exam has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the exam has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
