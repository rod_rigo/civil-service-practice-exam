<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class DocumentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', Document::class);
        return view('Admin.Documents.index');
    }

    public function getDocuments(){
        $this->authorize('admin', Document::class);
        $data = Document::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        $this->authorize('admin', Document::class);
        return view('Admin.Documents.bin');
    }

    public function getDocumentsDeleted(){
        $this->authorize('admin', Document::class);
        $data = Document::onlyTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        $this->authorize('admin', Document::class);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'title' => ['required', 'max:255'],
                'document' => ['required', 'max:255'],
                'file' => ['required',   function (string $attribute, mixed $value, \Closure $fail) {

                        $mimes = [strtolower('application/pdf')];

                        if (!in_array(strtolower($value->getClientMimeType()), $mimes)) {
                            $fail(ucwords('only pdf is allowed'));
                        }

                        return true;
                    },
                ],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'title.required' => ucwords('please fill out this field'),
                'title.max' => ucwords('this field must not exceed at 255 characters'),
                'document.required' => ucwords('please fill out this field'),
                'document.max' => ucwords('this field must not exceed at 255 characters'),
                'file.required' => ucwords('please fill out this field'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $document = Document::query()->make($request->all());

            $document->user_id = intval(auth()->user()->id);

            if($request->hasFile('file')){

                $file = $request->file('file');

                $filename = uniqid().$file->getClientOriginalName();

                try{

                    $path = public_path('document');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $filepath = public_path('document');

                    if($file){
                        $file->move($filepath, $filename);
                    }

                    $document->document = 'document/'.($filename);

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($document->save()){
                $result = ['message' => ucwords('the document has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.documents.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the document has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Admin.Documents.add');
    }

    public function edit($id = null, Request $request){
        $this->authorize('admin', Document::class);
        $document = Document::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'title' => ['required', 'max:255'],
                'document' => ['required', 'max:255'],
                'file' => ['nullable',   function (string $attribute, mixed $value, \Closure $fail) {

                    $mimes = [strtolower('application/pdf')];

                    if (!in_array(strtolower($value->getClientMimeType()), $mimes)) {
                        $fail(ucwords('only pdf is allowed'));
                    }

                    return true;
                },
                ],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'title.required' => ucwords('please fill out this field'),
                'title.max' => ucwords('this field must not exceed at 255 characters'),
                'document.required' => ucwords('please fill out this field'),
                'document.max' => ucwords('this field must not exceed at 255 characters'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $document->update($request->all());

            $document->user_id = intval(auth()->user()->id);

            if($request->hasFile('file')){

                $file = $request->file('file');

                $filename = uniqid().$file->getClientOriginalName();

                try{

                    $path = public_path('document');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $path = public_path($document->document);
                    $folder = File::isFile($path);
                    if($folder){
                        File::delete($path);
                    }

                    $filepath = public_path('document');

                    if($file){
                        $file->move($filepath, $filename);
                    }

                    $document->document = 'document/'.($filename);

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($document->save()){
                $result = ['message' => ucwords('the document has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.documents.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the document has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function view($id = null){
        $this->authorize('admin', Document::class);
        $document = Document::withoutTrashed()->findOrFail($id);
        return view('Admin.Documents.view', compact('document'));
    }

    public function download($id = null){
        $this->authorize('admin', Document::class);
        $document = Document::withoutTrashed()->findOrFail($id);
        $path = public_path($document->document);
        $folder = File::isFile($path);
        if(!$folder){
            abort(404);
        }
        $name = File::basename($path);
        return response()->download($path, $name);
    }

    public function delete($id = null){
        $this->authorize('admin', Document::class);
        $document = Document::withoutTrashed()->findOrFail($id);

        if($document->delete()){
            $result = ['message' => ucwords('the document has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the document has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $this->authorize('admin', Document::class);
        $document = Document::onlyTrashed()->findOrFail($id);

        if($document->restore()){
            $result = ['message' => ucwords('the document has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the document has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $this->authorize('admin', Document::class);
        $document = Document::onlyTrashed()->findOrFail($id);

        try{

            $path = public_path($document->document);
            $folder = File::isFile($path);
            if($folder){
                File::delete($path);
            }

        }catch (\Exception $exception){
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($document->forceDelete()){
            $result = ['message' => ucwords('the document has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the document has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
