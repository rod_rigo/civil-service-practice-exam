<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Reviewer;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Moment\Moment;

class SectionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', Section::class);
        return view('Admin.Sections.index');
    }

    public function getSections(Request $request){
        $this->authorize('admin', Section::class);
        $startDate = ($request->query('start_date'))? (new Moment(strval($request->query('start_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($request->query('end_date'))? (new Moment(strval($request->query('end_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $records = ($request->query('records'))? intval($request->query('records')): intval(10000);
        $data = Section::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                },
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['sections.created_at', '>=', $startDate],
                ['sections.created_at', '<=', $endDate],
            ])
            ->orderBy('sections.order_position','ASC')
            ->limit(intval($records))
            ->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        $this->authorize('admin', Section::class);
        return view('Admin.Sections.bin');
    }

    public function getSectionsDeleted(){
        $this->authorize('admin', Section::class);
        $data = Section::onlyTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                },
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function getSectionsList($examId = null){
        $this->authorize('admin', Section::class);
        $data = Section::withoutTrashed()
            ->with([
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['sections.exam_id', '=', intval($examId)]
            ])
            ->orderBy('sections.order_position','ASC')
            ->get()
            ->map(function($query){
                return ['value' => strtoupper($query->reviewers->reviewer), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return response()->json($data);
    }

    public function getSectionsExamList($examId = null){
        $this->authorize('admin', Section::class);
        $data = Section::withoutTrashed()
            ->with([
                'Questions' => function($query){
                    return $query->withTrashed()->get();
                },
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['sections.exam_id', '=', intval($examId)]
            ])
            ->orderBy('sections.order_position','ASC')
            ->get();
        return response()->json($data);
    }

    public function add(Request $request){
        $this->authorize('admin', Section::class);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'exam_id' => ['required', 'numeric', Rule::exists('exams','id'),
                    Rule::unique('sections')->where(function($query) use($request){
                        return $query->where([
                            ['exam_id', '=', intval($request->input('exam_id'))],
                            ['reviewer_id', '=', intval($request->input('reviewer_id'))],
                        ]);
                    })
                ],
                'reviewer_id' => ['required', 'numeric', Rule::exists('reviewers','id'),
                    Rule::unique('sections')->where(function($query) use($request){
                        return $query->where([
                            ['exam_id', '=', intval($request->input('exam_id'))],
                            ['reviewer_id', '=', intval($request->input('reviewer_id'))],
                        ]);
                    })
                ],
                'content' => ['required', 'max:4294967295'],
                'order_position' => ['required', 'numeric'],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'exam_id.required' => ucwords('please fill out this field'),
                'exam_id.numeric' => ucwords('please fill out this field'),
                'exam_id.exists' => ucwords('the exam does not exists'),
                'exam_id.unique' => ucwords('this exam and reviewer is already exists in this section'),
                'reviewer_id.required' => ucwords('please fill out this field'),
                'reviewer_id.numeric' => ucwords('please fill out this field'),
                'reviewer_id.exists' => ucwords('the reviewer does not exists'),
                'reviewer_id.unique' => ucwords('this exam and reviewer is already exists in this section'),
                'content.required' => ucwords('please fill out this field'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'order_position.required' => ucwords('please fill out this field'),
                'order_position.numeric' => ucwords('please fill out this field'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $section = Section::query()->make($request->all());

            $section->user_id = intval(auth()->user()->id);

            $max = Section::withoutTrashed()
                ->where([
                    ['sections.exam_id', '=', intval($request->input('exam_id'))]
                ])
                ->max('sections.order_position');

            $section->order_position = (intval(@$max) + intval(1));

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('section');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $filepath = public_path('section/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $section->content = 'section/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($section->save()){
                $result = ['message' => ucwords('the section has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.sections.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the section has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
        $exams = Exam::withoutTrashed()
            ->where([
                ['exams.is_active', '=', intval(1)]
            ])
            ->orderBy('exams.exam', 'ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->exam), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        $reviewers = Reviewer::withoutTrashed()
            ->where([
                ['reviewers.is_active', '=', intval(1)]
            ])
            ->orderBy('reviewers.reviewer', 'ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->reviewer), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return view('Admin.Sections.add', compact('exams', 'reviewers'));
    }

    public function edit($id = null, Request $request){
        $this->authorize('admin', Section::class);
        $section = Section::withoutTrashed()->findOrFail($id);

        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'exam_id' => ['required', 'numeric', Rule::exists('exams','id'),
                    Rule::unique('sections')->where(function($query) use($request){
                        return $query->where([
                            ['exam_id', '=', intval($request->input('exam_id'))],
                            ['reviewer_id', '=', intval($request->input('reviewer_id'))],
                        ]);
                    })->ignore(intval($section->id))
                ],
                'reviewer_id' => ['required', 'numeric', Rule::exists('reviewers','id'),
                    Rule::unique('sections')->where(function($query) use($request){
                        return $query->where([
                            ['exam_id', '=', intval($request->input('exam_id'))],
                            ['reviewer_id', '=', intval($request->input('reviewer_id'))],
                        ]);
                    })->ignore(intval($section->id))
                ],
                'content' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'exam_id.required' => ucwords('please fill out this field'),
                'exam_id.numeric' => ucwords('please fill out this field'),
                'exam_id.exists' => ucwords('the exam does not exists'),
                'exam_id.unique' => ucwords('this exam and reviewer is already exists in this section'),
                'reviewer_id.required' => ucwords('please fill out this field'),
                'reviewer_id.numeric' => ucwords('please fill out this field'),
                'reviewer_id.exists' => ucwords('the reviewer does not exists'),
                'reviewer_id.unique' => ucwords('this exam and reviewer is already exists in this section'),
                'content.required' => ucwords('please fill out this field'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

           $content = $section->content;
           $section->update($request->all());

           $section->user_id = intval(auth()->user()->id);

           if($section->wasChanged(['exam_id'])){
               $max = Section::withoutTrashed()
                   ->where([
                       ['sections.exam_id', '=', intval($request->input('exam_id'))]
                   ])
                   ->max('sections.order_position');

               $section->order_position = (intval(@$max) + intval(1));
           }

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('section');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $path = public_path($content);
                    $folder = File::isFile($path);
                    if($folder){
                        File::delete($path);
                    }

                    $filepath = public_path('section/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $section->content = 'section/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($section->save()){
                $result = ['message' => ucwords('the section has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.sections.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the section has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function view($id = null){
        $this->authorize('admin', Section::class);
        $section = Section::withoutTrashed()
            ->with([
                'Questions' => function($query){
                    return $query->withTrashed()
                        ->orderBy('questions.order_position', 'ASC')
                        ->get();
                }
            ])
            ->findOrFail($id);
        $exams = Exam::withoutTrashed()
            ->where([
                ['exams.is_active', '=', intval(1)]
            ])
            ->orderBy('exams.exam', 'ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->exam), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        $reviewers = Reviewer::withoutTrashed()
            ->where([
                ['reviewers.is_active', '=', intval(1)]
            ])
            ->orderBy('reviewers.reviewer', 'ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->reviewer), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        $questions = $section->toArray()['questions'];
        return view('Admin.Sections.view', compact('section','exams', 'reviewers', 'questions'));
    }

    public function orderPosition(Request $request){
        $this->authorize('admin', Section::class);
        if($request->isMethod('post')){

            DB::beginTransaction();

            try{

                $i = intval(0);

                foreach ($request->all() as $data){
                    $i++;
                    $section = Section::query()
                        ->where([
                            ['id', '=', intval($data['id'])]
                        ])
                        ->update([
                            'order_position' => intval($i)
                        ]);
                }

                DB::commit();
                $result = ['message' => ucwords('the exam has been saved'), 'result' => ucwords('success')];
                return response()->json($result,200);

            }catch (\Exception $exception){
                DB::rollBack();
                $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                return response()->json($result,422);
            }

        }
    }

    public function delete($id = null){
        $this->authorize('admin', Section::class);
        $section = Section::withoutTrashed()->findOrFail($id);

        $answer = Answer::withTrashed()
            ->where([
                ['section_id', '=', intval($section->id)]
            ])
            ->get()
            ->last();
        if(!empty($answer)){
            $result = ['message' => ucwords('the section has been constrained to answers'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $question = Question::withTrashed()
            ->where([
                ['section_id', '=', intval($section->id)]
            ])
            ->get()
            ->last();
        if(!empty($question)){
            $result = ['message' => ucwords('the section has been constrained to questions'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($section->delete()){
            $result = ['message' => ucwords('the section has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the section has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $this->authorize('admin', Section::class);
        $section = Section::onlyTrashed()->findOrFail($id);
        if($section->restore()){
            $result = ['message' => ucwords('the section has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the section has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $this->authorize('admin', Section::class);
        $section = Section::onlyTrashed()->findOrFail($id);

        DB::beginTransaction();

        try{

            DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

            $answer = Answer::withTrashed()
                ->where([
                    ['section_id', '=', intval($section->id)]
                ])
                ->get()
                ->last();
            if(!empty($answer)){
                $result = ['message' => ucwords('the section has been constrained to answers'), 'result' => ucwords('info')];
                return response()->json($result,422);
            }

            $question = Question::withTrashed()
                ->where([
                    ['section_id', '=', intval($section->id)]
                ])
                ->get()
                ->last();
            if(!empty($question)){
                $result = ['message' => ucwords('the section has been constrained to questions'), 'result' => ucwords('info')];
                return response()->json($result,422);
            }

            try{

                $path = public_path($section->content);
                $folder = File::isFile($path);
                if($folder){
                    File::delete($path);
                }

            }catch (\Exception $exception){
                $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                return response()->json($result,422);
            }

            if($section->forceDelete()){
                DB::commit();
                $result = ['message' => ucwords('the section has been deleted'), 'result' => ucwords('success')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the section has not been deleted'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }
        }catch (\Exception $exception){
            DB::rollBack();
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
            return response()->json($result,422);
        }
    }

}
