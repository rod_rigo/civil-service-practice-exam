<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lesson;
use App\Models\Reviewer;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;

class ReviewersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', Reviewer::class);
        return view('Admin.Reviewers.index');
    }

    public function getReviewers(){
        $this->authorize('admin', Reviewer::class);
        $data = Reviewer::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        $this->authorize('admin', Reviewer::class);
        return view('Admin.Reviewers.bin');
    }

    public function getReviewersDeleted(){
        $this->authorize('admin', Reviewer::class);
        $data = Reviewer::onlyTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function add(Request $request){
        $this->authorize('admin', Reviewer::class);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'reviewer' => ['required', 'max:255', Rule::unique('reviewers','reviewer')],
                'content' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'reviewer.required' => ucwords('please fill out this field'),
                'reviewer.max' => ucwords('this field must not exceed at 255 characters'),
                'reviewer.unique' => ucwords('this reviewer is already exists'),
                'content.required' => ucwords('please fill out the content'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $reviewer = Reviewer::query()->make($request->all());

            $reviewer->user_id = intval(auth()->user()->id);

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('reviewer');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $filepath = public_path('reviewer/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $reviewer->content = 'reviewer/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($reviewer->save()){
                $result = ['message' => ucwords('the reviewer has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.reviewers.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the reviewer has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Admin.Reviewers.add');
    }

    public function edit($id = null, Request $request){
        $this->authorize('admin', Reviewer::class);
        $reviewer = Reviewer::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'reviewer' => ['required', 'max:255', Rule::unique('reviewers','reviewer')->ignore(intval($reviewer->id))],
                'content' => ['required', 'max:4294967295'],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'reviewer.required' => ucwords('please fill out this field'),
                'reviewer.max' => ucwords('this field must not exceed at 255 characters'),
                'reviewer.unique' => ucwords('this reviewer is already exists'),
                'content.required' => ucwords('please fill out the content'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $content = $reviewer->content;
            $reviewer->update($request->all());

            $reviewer->user_id = intval(auth()->user()->id);

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('reviewer');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $path = public_path($content);
                    $folder = File::isFile($path);
                    if($folder){
                        File::delete($path);
                    }

                    $filepath = public_path('reviewer/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $reviewer->content = 'reviewer/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($reviewer->save()){
                $result = ['message' => ucwords('the reviewer has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.reviewers.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the reviewer has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function view($id = null){
        $this->authorize('admin', Reviewer::class);
        $reviewer = Reviewer::withoutTrashed()->findOrFail($id);
        return view('Admin.Reviewers.view', compact('reviewer'));
    }

    public function delete($id = null){
        $this->authorize('admin', Reviewer::class);
        $reviewer = Reviewer::withoutTrashed()->findOrFail($id);

        $lesson = Lesson::withTrashed()
            ->where([
                ['reviewer_id', '=', intval($reviewer->id)]
            ])
            ->get()
            ->last();
        if(!empty($lesson)){
            $result = ['message' => ucwords('the reviewer has been constrained to lessons'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $section = Section::withTrashed()
            ->where([
                ['reviewer_id', '=', intval($reviewer->id)]
            ])
            ->get()
            ->last();
        if(!empty($section)){
            $result = ['message' => ucwords('the reviewer has been constrained to sections'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($reviewer->delete()){
            $result = ['message' => ucwords('the reviewer has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the reviewer has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $this->authorize('admin', Reviewer::class);
        $reviewer = Reviewer::onlyTrashed()->findOrFail($id);

        if($reviewer->restore()){
            $result = ['message' => ucwords('the reviewer has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the reviewer has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $this->authorize('admin', Reviewer::class);
        $reviewer = Reviewer::onlyTrashed()->findOrFail($id);

        $lesson = Lesson::withTrashed()
            ->where([
                ['reviewer_id', '=', intval($reviewer->id)]
            ])
            ->get()
            ->last();
        if(!empty($lesson)){
            $result = ['message' => ucwords('the reviewer has been constrained to lessons'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        $section = Section::withTrashed()
            ->where([
                ['reviewer_id', '=', intval($reviewer->id)]
            ])
            ->get()
            ->last();
        if(!empty($section)){
            $result = ['message' => ucwords('the reviewer has been constrained to sections'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        try{

            $path = public_path($reviewer->content);
            $folder = File::isFile($path);
            if($folder){
                File::delete($path);
            }

        }catch (\Exception $exception){
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($reviewer->forceDelete()){
            $result = ['message' => ucwords('the reviewer has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the reviewer has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
