<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Moment\Moment;

class QuestionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', Question::class);
        return view('Admin.Questions.index');
    }

    public function getQuestions(Request $request){
        $this->authorize('admin', Question::class);
        $startDate = ($request->query('start_date'))? (new Moment(strval($request->query('start_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($request->query('end_date'))? (new Moment(strval($request->query('end_date')),'Asia/Manila'))->format('Y-m-d'):
            (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $records = ($request->query('records'))? intval($request->query('records')): intval(10000);
        $data = Question::withoutTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections.Reviewers' => function($query){
                    return $query->withTrashed()->get();
                },
            ])
            ->where([
                ['questions.created_at', '>=', $startDate],
                ['questions.created_at', '<=', $endDate],
            ])
            ->limit(intval($records))
            ->get();
        return response()->json(['data' => $data]);
    }

    public function bin(){
        $this->authorize('admin', Question::class);
        return view('Admin.Questions.bin');
    }

    public function getQuestionsDeleted(){
        $this->authorize('admin', Question::class);
        $data = Question::onlyTrashed()
            ->with([
                'Users' => function($query){
                    return $query->withTrashed()->get();
                },
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections' => function($query){
                    return $query->withTrashed()->get();
                },
                'Sections.Reviewers' => function($query){
                    return $query->withTrashed()->get();
                },
            ])
            ->get();
        return response()->json(['data' => $data]);
    }

    public function getQuestionsSectionList($sectionId = null){
        $this->authorize('admin', Question::class);
        $data = Question::withoutTrashed()
            ->where([
                ['questions.section_id', '=', intval($sectionId)]
            ])
            ->orderBy('questions.order_position', 'ASC')
            ->get();
        return response()->json($data);
    }

    public function add(Request $request){
        $this->authorize('admin', Question::class);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'exam_id' => ['required', 'numeric', Rule::exists('exams','id')],
                'section_id' => ['required', 'numeric', Rule::exists('sections','id')],
                'content' => ['required', 'max:4294967295'],
                'answer' => ['required', 'max:255'],
                'points' => ['required', 'numeric',
                    function (string $attribute, mixed $value, \Closure $fail) {
                        if (intval($value) == intval(0)) {
                            $fail(ucwords('This Field Must Not Lower Than 1'));
                        }

                        return true;
                    },
                ],
                'order_position' => ['required', 'numeric'],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'exam_id.required' => ucwords('please fill out this field'),
                'exam_id.numeric' => ucwords('please fill out this field'),
                'exam_id.exists' => ucwords('the exam does not exists'),
                'section_id.required' => ucwords('please fill out this field'),
                'section_id.numeric' => ucwords('please fill out this field'),
                'section_id.exists' => ucwords('the section does not exists'),
                'content.required' => ucwords('please fill out the question'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'answer.required' => ucwords('please fill out this field'),
                'answer.max' => ucwords('this field must not exceed at 255 characters'),
                'points.required' => ucwords('please fill out this field'),
                'points.numeric' => ucwords('please fill out this field'),
                'order_position.required' => ucwords('please fill out this field'),
                'order_position.numeric' => ucwords('please fill out this field'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            DB::beginTransaction();

            try{

                DB::statement('SET FOREIGN_KEY_CHECKS=0;');

                $question = Question::query()->make($request->all());
                $exam = Exam::withoutTrashed()->findOrFail($request->input('exam_id'));

                $max = Question::withoutTrashed()
                    ->where([
                        ['questions.exam_id', '=', intval($request->input('exam_id'))],
                        ['questions.section_id', '=', intval($request->input('section_id'))]
                    ])
                    ->max('questions.order_position');

                $sum = Question::withoutTrashed()
                    ->where([
                        ['questions.exam_id', '=', intval($request->input('exam_id'))],
                    ])
                    ->sum('questions.points');

                $exam->total = doubleval(@$sum) + doubleval($request->input('points'));

                $question->user_id = intval(auth()->user()->id);

                $question->order_position = intval(@$max) + intval(1);

                if($request->has('content')){

                    $file = $request->input('content');

                    $filename = uniqid().'.txt';

                    try{

                        $path = public_path('question');
                        $folder = File::isDirectory($path);
                        if(!$folder){
                            File::makeDirectory($path);
                        }

                        $filepath = public_path('question/');

                        if(file_put_contents($filepath.($filename), $file)){
                            $question->content = 'question/'.($filename);
                        }

                    }catch (\Exception $exception){
                        $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                        return response()->json($result,422);
                    }

                }

                if($question->save() && $exam->save()){
                    DB::commit();
                    $result = ['message' => ucwords('the question has been saved'), 'result' => ucwords('success'),
                        'redirect' => route('admin.questions.index')];
                    return response()->json($result,200);
                }else{
                    $result = ['message' => ucwords('the question has not been saved'), 'result' => ucwords('error')];
                    return response()->json($result,422);
                }

            }catch (\Exception $exception){
                DB::rollBack();
                $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                return response()->json($result,422);
            }

        }

        $exams = Exam::withoutTrashed()
            ->where([
                ['exams.is_active', '=', intval(1)]
            ])
            ->orderBy('exams.created_at','ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->exam), 'key' => intval($query->id)];
            })
            ->pluck('value','key');

        return view('Admin.Questions.add', compact('exams'));
    }

    public function edit($id = null, Request $request){
        $this->authorize('admin', Question::class);
        $question = Question::withoutTrashed()->findOrFail($id);
        if($request->isMethod('post')){

            $validator = $request->validate([
                'user_id' => ['required', 'numeric', Rule::exists('users','id')],
                'exam_id' => ['required', 'numeric', Rule::exists('exams','id')],
                'section_id' => ['required', 'numeric', Rule::exists('sections','id')],
                'content' => ['required', 'max:4294967295'],
                'answer' => ['required', 'max:255'],
                'points' => ['required', 'numeric',
                    function (string $attribute, mixed $value, \Closure $fail) {
                        if (intval($value) == intval(0)) {
                            $fail(ucwords('This Field Must Not Lower Than 1'));
                        }

                        return true;
                    },
                ],
                'order_position' => ['required', 'numeric'],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
            ],[
                'user_id.required' => ucwords('please fill out this field'),
                'user_id.numeric' => ucwords('please fill out this field'),
                'user_id.exists' => ucwords('the user does not exists'),
                'exam_id.required' => ucwords('please fill out this field'),
                'exam_id.numeric' => ucwords('please fill out this field'),
                'exam_id.exists' => ucwords('the exam does not exists'),
                'section_id.required' => ucwords('please fill out this field'),
                'section_id.numeric' => ucwords('please fill out this field'),
                'section_id.exists' => ucwords('the section does not exists'),
                'content.required' => ucwords('please fill out the question'),
                'content.max' => ucwords('the content must not exceed at 4,294,967,295 characters'),
                'answer.required' => ucwords('please fill out this field'),
                'answer.max' => ucwords('this field must not exceed at 255 characters'),
                'points.required' => ucwords('please fill out this field'),
                'points.numeric' => ucwords('please fill out this field'),
                'order_position.required' => ucwords('please fill out this field'),
                'order_position.numeric' => ucwords('please fill out this field'),
                'is_active.required' => ucwords('please fill out this field'),
                'is_active.numeric' => ucwords('please fill out this field'),
                'is_active.in' => ucwords('please fill out this field'),
            ]);

            $content = $question->content;

            $question->update($request->all());
            $exam = Exam::withoutTrashed()->findOrFail($request->input('exam_id'));

            $question->user_id = intval(auth()->user()->id);

            $sum = Question::withoutTrashed()
                ->where([
                    ['questions.exam_id', '=', intval($request->input('exam_id'))],
                    ['questions.id', '!=', intval($question->id)],
                ])
                ->sum('questions.points');

            $exam->total = doubleval(@$sum) + doubleval($request->input('points'));

            if($request->has('content')){

                $file = $request->input('content');

                $filename = uniqid().'.txt';

                try{

                    $path = public_path('question');
                    $folder = File::isDirectory($path);
                    if(!$folder){
                        File::makeDirectory($path);
                    }

                    $path = public_path($content);
                    $folder = File::isFile($path);
                    if($folder){
                        File::delete($path);
                    }

                    $filepath = public_path('question/');

                    if(file_put_contents($filepath.($filename), $file)){
                        $question->content = 'question/'.($filename);
                    }

                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            }

            if($question->save() && $exam->save()){
                $result = ['message' => ucwords('the question has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('admin.questions.index')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the question has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }
    }

    public function view($id = null){
        $this->authorize('admin', Question::class);
        $question = Question::withoutTrashed()->findOrFail($id);

        $exams = Exam::withoutTrashed()
            ->where([
                ['exams.is_active', '=', intval(1)]
            ])
            ->orderBy('exams.created_at','ASC')
            ->get()
            ->map(function ($query){
                return ['value' => strtoupper($query->exam), 'key' => intval($query->id)];
            })
            ->pluck('value','key');

        $sections = Section::withoutTrashed()
            ->with([
                'Reviewers' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->where([
                ['sections.exam_id', '=', intval($question->exam_id)]
            ])
            ->orderBy('sections.order_position','ASC')
            ->get()
            ->map(function($query){
                return ['value' => strtoupper($query->reviewers->reviewer), 'key' => intval($query->id)];
            })
            ->pluck('value', 'key');
        return view('Admin.Questions.view', compact('question', 'exams', 'sections'));
    }

    public function orderPosition(Request $request){
        $this->authorize('admin', Question::class);
        if($request->isMethod('post')){

            DB::beginTransaction();

            try{

                $i = intval(0);

                foreach ($request->all() as $data){
                    $i++;
                    $question = Question::query()
                        ->where([
                            ['id', '=', intval($data['id'])]
                        ])
                        ->update([
                            'order_position' => intval($i)
                        ]);
                }

                DB::commit();
                $result = ['message' => ucwords('the question has been saved'), 'result' => ucwords('success')];
                return response()->json($result,200);

            }catch (\Exception $exception){
                DB::rollBack();
                $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
                return response()->json($result,422);
            }

        }
    }

    public function delete($id = null){
        $this->authorize('admin', Question::class);
        $question = Question::withoutTrashed()->findOrFail($id);

        $answer = Answer::withTrashed()
            ->where([
                ['question_id', '=', intval($question->id)]
            ])
            ->get()
            ->last();
        if(!empty($answer)){
            $result = ['message' => ucwords('the question has been constrained to answers'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($question->delete()){
            $result = ['message' => ucwords('the question has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the question has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function restore($id = null){
        $this->authorize('admin', Question::class);
        $question = Question::onlyTrashed()->findOrFail($id);
        if($question->restore()){
            $result = ['message' => ucwords('the question has been restored'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the question has not been restored'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

    public function forceDelete($id = null){
        $this->authorize('admin', Question::class);
        $question = Question::onlyTrashed()->findOrFail($id);

        $answer = Answer::withTrashed()
            ->where([
                ['question_id', '=', intval($question->id)]
            ])
            ->get()
            ->last();
        if(!empty($answer)){
            $result = ['message' => ucwords('the question has been constrained to answers'), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        try{

            $path = public_path($question->content);
            $folder = File::isFile($path);
            if($folder){
                File::delete($path);
            }

        }catch (\Exception $exception){
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('info')];
            return response()->json($result,422);
        }

        if($question->forceDelete()){
            $result = ['message' => ucwords('the question has been deleted'), 'result' => ucwords('success')];
            return response()->json($result,200);
        }else{
            $result = ['message' => ucwords('the question has not been deleted'), 'result' => ucwords('error')];
            return response()->json($result,422);
        }
    }

}
