<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\Examination;
use App\Models\Reviewer;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Moment\Moment;

class DashboardsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('admin', User::class);
        return view('Admin.Dashboards.index');
    }

    public function counts(){
        $this->authorize('admin', User::class);
        $users = User::withoutTrashed()->get()->count();
        $exams = Exam::withoutTrashed()->get()->count();
        $reviewers = Reviewer::withoutTrashed()->get()->count();

        $collection = (new Collection([
            [
                'users' => doubleval($users),
                'exams' => doubleval($exams),
                'reviewers' => doubleval($reviewers)
            ]
        ]))->first();

        return response()->json($collection);
    }

    public function getExaminations(Request $request){
        $this->authorize('admin', User::class);
        $startDate = ($request->query('start_date'))?(new Moment($request->query('start_date'),'Asia/Manila'))->format('Y-m-d'):(new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($request->query('end_date'))?(new Moment($request->query('end_date'),'Asia/Manila'))->format('Y-m-d'):(new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->selectRaw('MONTHNAME(examinations.created_at) as month')
            ->selectRaw('COUNT(examinations.id) as total')
            ->where([
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate]
            ])
            ->orderBy('examinations.created_at','ASC')
            ->groupBy([
                'month'
            ])
            ->get();

        return response()->json($data);
    }

    public function getExaminationsCount(){
        $this->authorize('admin', User::class);

        $now = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $today = Examination::withoutTrashed()
            ->where([
                ['examinations.created_at', '>=', $now]
            ])
            ->get()
            ->count();

        $startWeek = (new Moment(null,'Asia/Manila'))->startOf('week')->subtractDays(intval(1))->format('Y-m-d');
        $endWeek = (new Moment(null,'Asia/Manila'))->endOf('week')->addDays(intval(1))->format('Y-m-d');
        $week = Examination::withoutTrashed()
            ->where([
                ['examinations.created_at', '>', $startWeek],
                ['examinations.created_at', '<', $endWeek]
            ])
            ->get()
            ->count();

        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('month')->subtractDays(intval(1))->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('month')->addDays(intval(1))->format('Y-m-d');
        $month = Examination::withoutTrashed()
            ->where([
                ['examinations.created_at', '>', $startMonth],
                ['examinations.created_at', '<', $endMonth]
            ])
            ->get()
            ->count();

        $startYear = (new Moment(null,'Asia/Manila'))->startOf('year')->subtractDays(intval(1))->format('Y-m-d');
        $endYear = (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(intval(1))->format('Y-m-d');
        $year = Examination::withoutTrashed()
            ->where([
                ['examinations.created_at', '>', $startYear],
                ['examinations.created_at', '<', $endYear]
            ])
            ->get()
            ->count();

        $collection = (new Collection([
            [
                'today' => intval($today),
                'week' => intval($week),
                'month' => intval($month),
                'year' => intval($year)
            ]
        ]))->first();
        return response()->json($collection);
    }

    public function getExaminationsPassingRate(Request $request){
        $this->authorize('admin', User::class);
        $startDate = ($request->query('start_date'))?(new Moment($request->query('start_date'),'Asia/Manila'))->format('Y-m-d'):(new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($request->query('end_date'))?(new Moment($request->query('end_date'),'Asia/Manila'))->format('Y-m-d'):(new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->select([
                'exam' => Exam::withoutTrashed()->selectRaw('UPPER(exams.exam)')
                    ->whereColumn('examinations.exam_id', '=', 'exams.id')
            ])
            ->selectRaw('AVG(examinations.average) as total')
            ->groupBy([
                'exam_id'
            ])
            ->where([
                ['examinations.is_released', '=', intval(1)],
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate]
            ])
            ->orderBy('total','DESC')
            ->limit(intval(5))
            ->get();

        return response()->json($data);
    }

    public function getExaminationsMostTake(Request $request){
        $this->authorize('admin', User::class);
        $startDate = ($request->query('start_date'))?(new Moment($request->query('start_date'),'Asia/Manila'))->format('Y-m-d'):(new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = ($request->query('end_date'))?(new Moment($request->query('end_date'),'Asia/Manila'))->format('Y-m-d'):(new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->select([
                'exam' => Exam::withoutTrashed()->selectRaw('UPPER(exams.exam)')
                    ->whereColumn('examinations.exam_id', '=', 'exams.id')
            ])
            ->selectRaw('COUNT(examinations.exam_id) as total')
            ->groupBy([
                'exam_id'
            ])
            ->where([
                ['examinations.is_released', '=', intval(1)],
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate]
            ])
            ->orderBy('total','DESC')
            ->get();

        return response()->json($data);
    }

    public function getLeaderboardsWeek(Request $request){
        $this->authorize('admin', User::class);
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('week')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                },
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->select([
                'name' => User::withoutTrashed()->selectRaw('UPPER(users.name)')
                    ->whereColumn('examinations.user_id', '=', 'users.id'),
                'examinations.user_id',
            ])
            ->selectRaw('AVG(examinations.average) as total')
            ->groupBy([
                'user_id'
            ])
            ->where([
                ['examinations.is_released', '=', intval(1)],
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate]
            ])
            ->orderBy('total','DESC')
            ->get();

        return response()->json($data);
    }

    public function getLeaderboardsMonth(Request $request){
        $this->authorize('admin', User::class);
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('month')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                },
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->select([
                'name' => User::withoutTrashed()->selectRaw('UPPER(users.name)')
                    ->whereColumn('examinations.user_id', '=', 'users.id'),
                'examinations.user_id',
            ])
            ->selectRaw('AVG(examinations.average) as total')
            ->groupBy([
                'user_id'
            ])
            ->where([
                ['examinations.is_released', '=', intval(1)],
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate]
            ])
            ->orderBy('total','DESC')
            ->get();

        return response()->json($data);
    }

    public function getLeaderboardsYear(Request $request){
        $this->authorize('admin', User::class);
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $data = Examination::withoutTrashed()
            ->with([
                'Exams' => function($query){
                    return $query->withTrashed()->get();
                },
                'Users' => function($query){
                    return $query->withTrashed()->get();
                }
            ])
            ->select([
                'name' => User::withoutTrashed()->selectRaw('UPPER(users.name)')
                    ->whereColumn('examinations.user_id', '=', 'users.id'),
                'examinations.user_id',
            ])
            ->selectRaw('AVG(examinations.average) as total')
            ->groupBy([
                'user_id'
            ])
            ->where([
                ['examinations.is_released', '=', intval(1)],
                ['examinations.created_at', '>=', $startDate],
                ['examinations.created_at', '<=', $endDate]
            ])
            ->orderBy('total','DESC')
            ->get();

        return response()->json($data);
    }

}
