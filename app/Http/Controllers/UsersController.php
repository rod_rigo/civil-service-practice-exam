<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')
            ->except([
                'login',
                'register',
                'logout'
            ]);
    }

    public function login(Request $request){
        if (auth()->check()) {
            return redirect()->route('users.logout');
        }

        if ($request->isMethod('post')) {

            $validator = $request->validate([
                'username' => ['required'],
                'password' => ['required']
            ],[
                'username' => ucwords('please fill out this field'),
                'password' => ucwords('please fill out this field'),
            ]);

            $auth = User::withoutTrashed()->where(function ($query) use ($request){
                return $query->where('users.username','like','%'.$request->input('username').'%')
                    ->orWhere('users.email','like','%'.$request->input('username').'%');
            })->count();

            if (intval($auth) > intval(0)) {

                $field = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
                if (auth()->attempt([$field => $request->input('username'), 'password' => $request->input('password'), 'is_active' => intval(1)], $request->boolean('remember_me'))) {
                    $request->session()->regenerate();
                    $user = User::withoutTrashed()->findOrFail(auth()->id());
                    $user->password = $request->input('password');
                    $user->token = uniqid().microtime();

                    if ($user->save()) {
                        if(boolval(auth()->user()->is_admin)){
                            $result = ['message' => ucwords('Redirecting Please Wait'), 'result' => ucwords('success'),
                                 'redirect' =>  route('admin.dashboards.index')];
                            return response()->json($result,200);
                        }

                        if(boolval(auth()->user()->is_examinee)){
                            $result = ['message' => ucwords('Redirecting Please Wait'), 'result' => ucwords('success'),
                                'redirect' =>  route('examinee.dashboards.index')];
                            return response()->json($result,200);
                        }
                    }else{
                        if(boolval(auth()->user()->is_admin)){
                            $result = ['message' => ucwords('Redirecting Please Wait'), 'result' => ucwords('success'),
                                'redirect' =>  route('admin.dashboards.index')];
                            return response()->json($result,200);
                        }

                        if(boolval(auth()->user()->is_examinee)){
                            $result = ['message' => ucwords('Redirecting Please Wait'), 'result' => ucwords('success'),
                                'redirect' =>  route('examinee.dashboards.index')];
                            return response()->json($result,200);
                        }
                    }

                } else {
                    $result = ['message' => ucwords('please check your username or email & password, please try again!'),
                        'result' => ucwords('info')];
                    return response()->json($result,422);
                }

            } else {
                $result = ['message' => ucwords('please check your username or email & password, please try again!'),
                    'result' => ucwords('info')];
                return response()->json($result,422);
            }

        }

        return view('Users.login');
    }

    public function register(Request $request){
        if($request->isMethod('post')){

            $validator = $request->validate([
                'name' => ['required', 'max:255'],
                'username' => ['required', 'max:255', Rule::unique('users','username'),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d).{8,24}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Username Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    },
                ],
                'email' => ['required', 'max:255', 'email', Rule::unique('users','email'),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $fail(ucwords('Please Enter A Valid Email'));
                        }

                        return true;
                    },
                ],
                'contact_number' => ['required', 'max:255', Rule::unique('users','contact_number'),
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(09|\+639)([0-9]{9})$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Contact Number Must Be Start At 0 & 9'));
                        }

                        return true;
                    },
                ],
                'password' => ['required', 'max:255',
                    function (string $attribute, mixed $value, \Closure $fail) {
                        $regex = '/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};\':"\\|,.<>\/?]).{8,}$/';
                        if (!preg_match_all($regex, $value)) {
                            $fail(ucwords('Password Must Contain At Least (1 Capital Letter & Number, Minimum Of 8 Characters)'));
                        }

                        return true;
                    }
                ],
                'confirm_password' => ['required', 'max:255', 'same:password'],
                'is_admin' => ['required', 'numeric', Rule::in([intval(0)])],
                'is_examinee' => ['required', 'numeric', Rule::in([intval(1)])],
                'is_active' => ['required', 'numeric', Rule::in([intval(0), intval(1)])],
                'token' => ['required', 'max:255'],
            ],[
                'name.required' => ucwords('please fill out this field'),
                'name.max' => ucwords('this field must not exceed at 255 characters'),
                'username.required' => ucwords('please fill out this field'),
                'username.max' => ucwords('this field must not exceed at 255 characters'),
                'username.unique' => ucwords('this username is already exists'),
                'email.required' => ucwords('please fill out this field'),
                'email.max' => ucwords('this field must not exceed at 255 characters'),
                'email.email' => ucwords('this field is not valid email'),
                'email.unique' => ucwords('this email is already exists'),
                'contact_number.required' => ucwords('please fill out this field'),
                'contact_number.max' => ucwords('this field must not exceed at 255 characters'),
                'contact_number.unique' => ucwords('this contact number is already exists'),
                'password.required' => ucwords('please fill out this field'),
                'password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.required' => ucwords('please fill out this field'),
                'confirm_password.max' => ucwords('this field must not exceed at 255 characters'),
                'confirm_password.same' => ucwords('password do not matched'),
                'is_admin.required' => ucwords('please fill out admin value'),
                'is_admin.numeric' => ucwords('please fill out admin value'),
                'is_admin.in' => ucwords('please fill out admin value'),
                'is_examinee.required' => ucwords('please fill out examinee value'),
                'is_examinee.numeric' => ucwords('please fill out examinee value'),
                'is_examinee.in' => ucwords('please fill out examinee value'),
                'is_active.required' => ucwords('please fill out active value'),
                'is_active.numeric' => ucwords('please fill out active value'),
                'is_active.in' => ucwords('please fill out active value'),
                'token.required' => ucwords('please fill out this field'),
                'token.max' => ucwords('this field must not exceed at 255 characters'),
            ]);

            $user = User::query()->make($request->all());

            $user->is_admin = intval(0);
            $user->is_examinee = intval(1);
            $user->is_active = intval(1);
            $user->token = uniqid().microtime();

            if($user->save()){
                $result = ['message' => ucwords('the registration has been saved'), 'result' => ucwords('success'),
                    'redirect' => route('users.login')];
                return response()->json($result,200);
            }else{
                $result = ['message' => ucwords('the registration has not been saved'), 'result' => ucwords('error')];
                return response()->json($result,422);
            }

        }

        return view('Users.register');
    }

    public function logout(Request $request)
    {
        if(auth()->logout()){
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return redirect()->route('users.login');
        }
        return redirect()->route('users.login');
    }

}
