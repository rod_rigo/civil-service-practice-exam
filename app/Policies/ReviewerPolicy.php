<?php

namespace App\Policies;

use App\Models\Reviewer;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ReviewerPolicy
{
    use HandlesAuthorization;

    public function admin(User $user){
        return (!empty(auth()->user()) && boolval($user->is_admin))? Response::allow(): Response::deny();
    }

    public function examinee(User $user){
        return (!empty(auth()->user()) && boolval($user->is_examinee))? Response::allow(): Response::deny();
    }

    public function owned(User $user, Reviewer $reviewer){
        return (!empty(auth()->user()) && (intval($user->id) == intval($reviewer->user_id)))? Response::allow(): Response::deny();
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Reviewer  $reviewer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Reviewer $reviewer)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Reviewer  $reviewer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Reviewer $reviewer)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Reviewer  $reviewer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Reviewer $reviewer)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Reviewer  $reviewer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Reviewer $reviewer)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Reviewer  $reviewer
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Reviewer $reviewer)
    {
        //
    }
}
